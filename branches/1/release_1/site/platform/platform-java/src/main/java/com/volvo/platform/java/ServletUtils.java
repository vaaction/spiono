package com.volvo.platform.java;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.util.UUID.randomUUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ServletUtils {
    public static void writeExeToResponseAndFlush(HttpServletResponse response, byte[] exe, String downloadFileName) throws IOException {
        ServletOutputStream outputStream = response.getOutputStream ();
        response.setContentType ("application/exe");
        response.setHeader ("Content-Disposition", "attachment; filename=\"" + downloadFileName + "\"");

        outputStream.write(exe);
        outputStream.flush();
    }

    public static String randomNoCache() {
        return "?nocache=" + randomUUID();
    }
}
