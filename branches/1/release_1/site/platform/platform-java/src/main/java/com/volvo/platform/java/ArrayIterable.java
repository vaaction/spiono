package com.volvo.platform.java;

import org.apache.commons.collections.iterators.ArrayIterator;

import java.util.Iterator;

import static com.google.common.base.Preconditions.checkNotNull;

public class ArrayIterable<T> implements Iterable<T> {

    public final T[] array;

    public static<T> ArrayIterable<T> newArrayIterable(T[] array) {
        return new ArrayIterable<T>(array);
    }

    public ArrayIterable(T[] array) {
        this.array = checkNotNull(array);
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayIterator(array);
    }
}
