package com.volvo.platform.java;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PropertiesUtils {
    public static String decorateAsProperty(String name) {
        return "${" + name + "}";
    }

    public static String undecorateProperty(String property) {
        return StringUtils.strip(property, "{$}");
    }
}
