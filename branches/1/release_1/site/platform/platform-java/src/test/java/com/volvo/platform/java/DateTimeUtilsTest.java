package com.volvo.platform.java;

import org.joda.time.DateTime;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class DateTimeUtilsTest {

    @Test(expectedExceptions = NullPointerException.class)
    public void shitToTheEndOfDayNPETest() {
        DateTimeUtils.shiftToTheEndOfDay(null);
    }

    @Test
    public void shitToTheEndOfDayTest() {
        DateTime srcDateTime = new DateTime(2013, 2, 25, 11, 15, 12);
        DateTime dstDateTime = new DateTime(DateTimeUtils.shiftToTheEndOfDay(srcDateTime.toDate()));

        assertEquals(dstDateTime.getYear(), 2013);
        assertEquals(dstDateTime.getMonthOfYear(), 2);
        assertEquals(dstDateTime.getDayOfMonth(), 25);
        assertEquals(dstDateTime.getHourOfDay(), 23);
        assertEquals(dstDateTime.getMinuteOfHour(), 59);
        assertEquals(dstDateTime.getSecondOfMinute(), 59);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shitToTheBeginOfDayNPETest() {
        DateTimeUtils.shiftToBeginOfDay(null);
    }

    @Test
    public void shitToTheBeginOfDayTest() {
        DateTime srcDateTime = new DateTime(2013, 2, 25, 11, 15, 12);
        DateTime dstDateTime = new DateTime(DateTimeUtils.shiftToBeginOfDay(srcDateTime.toDate()));

        assertEquals(dstDateTime.getYear(), 2013);
        assertEquals(dstDateTime.getMonthOfYear(), 2);
        assertEquals(dstDateTime.getDayOfMonth(), 25);
        assertEquals(dstDateTime.getHourOfDay(), 0);
        assertEquals(dstDateTime.getMinuteOfHour(), 0);
        assertEquals(dstDateTime.getSecondOfMinute(), 0);
    }
}
