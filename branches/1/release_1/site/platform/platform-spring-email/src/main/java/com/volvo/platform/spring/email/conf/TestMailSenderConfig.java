package com.volvo.platform.spring.email.conf;

import freemarker.template.TemplateModelException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import javax.annotation.PostConstruct;

@Configuration
@Slf4j
public class TestMailSenderConfig {

    @Value("${sitename}")
    private String siteName;

    @Value("${hostname}")
    private String hostName;

    @Autowired
    private freemarker.template.Configuration freemarkerConfiguration;

    @Bean
    public MailSender mailSender() {
        return new MailSender() {
            @Override
            public void send(SimpleMailMessage simpleMessage) throws MailException {
                TestMailSenderConfig.log.info("Sending message: " + simpleMessage);
            }

            @Override
            public void send(SimpleMailMessage[] simpleMessages) throws MailException {
                TestMailSenderConfig.log.info("Sending messags: " + simpleMessages);
            }
        };
    }

    @PostConstruct
    private void postConstruct() throws TemplateModelException {
        freemarkerConfiguration.setSharedVariable("sitename", siteName);
        freemarkerConfiguration.setSharedVariable("hostname", hostName);
    }
}