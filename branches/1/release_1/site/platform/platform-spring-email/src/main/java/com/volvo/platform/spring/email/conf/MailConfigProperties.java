package com.volvo.platform.spring.email.conf;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class MailConfigProperties {

    public static final String HOST = "${mail.host}";

    public static final String PORT = "${mail.port}";

    public static final String USERNAME = "${mail.username}";

    public static final String PASSWORD = "${mail.password}";

    public static final String SMTP_AUTH = "${mail.smtp.auth}";

    public static final String SMTP_SMARTTTLS_ENABLE = "${mail.smtp.starttls.enable}";
}
