package com.volvo.platform.spring.email;

import freemarker.template.Template;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.mail.MailMessage;

import static com.volvo.platform.spring.email.FreemarkerUtils.processToStringQuietly;


@AllArgsConstructor
public class FreemarkerEmailTemplate {

    @Getter
    private final Template subjectTemplate;

    @Getter
    private final Template bodyTemplate;

    public<T extends MailMessage> T process(Object rootMap, T mailMessage) {
        mailMessage.setSubject(processToStringQuietly(rootMap, subjectTemplate));
        mailMessage.setText(processToStringQuietly(rootMap, bodyTemplate));
        return mailMessage;
    }
}
