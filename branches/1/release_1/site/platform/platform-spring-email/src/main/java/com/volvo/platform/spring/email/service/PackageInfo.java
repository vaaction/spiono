package com.volvo.platform.spring.email.service;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PackageInfo {
    public static final String PACKAGE_NAME = "com.volvo.platform.spring.email.service";
}
