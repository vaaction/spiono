package com.volvo.platform.spring.email;

public interface EmailLetter {

    String getBodyPath();

    String getSubjectPath();
}
