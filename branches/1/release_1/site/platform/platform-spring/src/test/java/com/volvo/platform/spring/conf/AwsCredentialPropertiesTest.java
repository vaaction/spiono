package com.volvo.platform.spring.conf;

import org.testng.annotations.Test;

import static com.volvo.platform.java.testing.TestUtils.getStaticFinalStringValues;
import static com.volvo.platform.java.testing.TestUtils.isStringsAreUnique;
import static org.testng.Assert.assertTrue;

public class AwsCredentialPropertiesTest {
    @Test
    public void testEnumValieAreUnique() {
        assertTrue(isStringsAreUnique(getStaticFinalStringValues(AwsCredentialProperties.class)));
    }
}
