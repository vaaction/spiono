package com.volvo.platform.spring.captcha;

import lombok.Getter;

import java.awt.image.BufferedImage;

public class Captcha {
    @Getter
    private final String captchaString;

    @Getter
    private final BufferedImage bufferedImage;

    public static final String SESSION_CAPTCHA_ATTR = "captcha";

    private Captcha(CaptchaBuilder captchaBuilder) {
        this.captchaString = captchaBuilder.getCaptchaText();
        this.bufferedImage = captchaBuilder.getBufferedImage();
    }

    public static Captcha newCaptcha(int width, int height) {
        CaptchaBuilder captchaBuilder = new CaptchaBuilder(width, height);
        return new Captcha(captchaBuilder.build());
    }
}
