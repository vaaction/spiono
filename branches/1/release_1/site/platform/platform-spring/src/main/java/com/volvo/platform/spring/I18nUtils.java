package com.volvo.platform.spring;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.context.MessageSource;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class I18nUtils {

    public static String getMessage(MessageSource source, String key) {
        return source.getMessage(key, null, "Message not found", null);
    }
}
