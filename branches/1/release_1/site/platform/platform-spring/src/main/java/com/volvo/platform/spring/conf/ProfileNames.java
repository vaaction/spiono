package com.volvo.platform.spring.conf;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class ProfileNames {

    public static final String DEVELOPMENT = "development";

    public static final String PRODUCTION = "production";

    public static final String TESTING = "testing";
}
