package com.volvo.platform.spring;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import java.util.Map;

import static com.volvo.platform.java.JavaUtils.invokeConstructorQuietly;
import static org.apache.commons.lang3.StringUtils.uncapitalize;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class MvcUtil {

    public static Cookie newCookie(String name, String value) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        return cookie;
    }

    public static final ModelAndView redirectToView(String viewName) {
        return new ModelAndView(redirectTo(viewName));
    }

    public static String redirectTo(String viewName) {
        return "redirect:/" + viewName + "/";
    }

    public static ModelAndView modelAndViewWithForm(String viewName, Class<?> formClazz) {
        return new ModelAndView(viewName, getUncapitalizedSimpleClassName(formClazz),
                invokeConstructorQuietly(formClazz));
    }

    public static ModelAndView modelAndViewWithForm(String viewName, Object formObject, ModelMap map) {
        return modelAndViewWithForm(viewName, formObject).addAllObjects(map);
    }

    public static ModelAndView modelAndViewWithForm(String viewName, Object formObject) {
        return new ModelAndView(viewName, getUncapitalizedSimpleClassName(formObject.getClass()), formObject);
    }

    public static String getUncapitalizedSimpleClassName(Class<?> clazz) {
        return uncapitalize(clazz.getSimpleName());
    }

    public static ModelAndView modelAndView(String viewName, BindingResult bindingResult) {
        return modelAndView(viewName, bindingResult.getModel());
    }

    public static ModelAndView modelAndView(String viewName, Map<String, Object> model) {
        return new ModelAndView(viewName, model);
    }
}
