package com.volvo.platform.spring.captcha;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.ui.ModelMap;

import java.awt.image.BufferedImage;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class CaptchaMvcUtils {

    public static boolean isCaptchaValid(String captcha, ModelMap modelMap) {
        return isCaptchaValid(captcha, (String) modelMap.get(Captcha.SESSION_CAPTCHA_ATTR));
    }

    public static boolean isCaptchaValid(String captcha, String valueInSession) {
        return equalsIgnoreCase(captcha, valueInSession);
    }

    public static BufferedImage generateBufferedImageCaptcha(ModelMap modelMap) {
        return generateBufferedImageCaptcha(200, 50, modelMap);
    }

    public static BufferedImage generateBufferedImageCaptcha(int width, int height, ModelMap modelMap) {
        Captcha captcha = Captcha.newCaptcha(width, height);
        modelMap.addAttribute(Captcha.SESSION_CAPTCHA_ATTR, captcha.getCaptchaString());
        return captcha.getBufferedImage();
    }
}
