package com.volvo.platform.spring.captcha;

import lombok.Getter;
import org.apache.commons.lang3.RandomStringUtils;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Random;

public class CaptchaBuilder {
    // captcha constants
    private static final Color textColor = Color.white;
    private static final Color circleColor = new Color(160, 160, 160);
    private static final Font textFont = new Font("Arial", Font.PLAIN, 24);
    private static final int charsToPrint = 4;

    private static final int circlesToDraw = 4;
    private static final float horizMargin = 10.0f;
    // this is radians
    private static final double rotationRange = 0.7;
    public static final String CHARS = "qwertyupasdfghjkzxcvbnm23456789";

    private final int width;
    private final int height;
    private final Random random;

    @Getter
    private BufferedImage bufferedImage;

    @Getter
    private String captchaText = "";

    private Graphics2D graphic;

    public CaptchaBuilder(int width, int height) {
        this.width = width;
        this.height = height;

        this.random = new Random();
    }

    private void init() {
        this.bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        this.graphic = (Graphics2D) bufferedImage.getGraphics();
    }

    public CaptchaBuilder build() {
        init();

        generateCaptchaText();

        drawNoise();
        drawText();

        graphic.dispose();

        return this;
    }

    private void generateCaptchaText() {
        captchaText = RandomStringUtils.random(charsToPrint, CHARS);
    }

    private void drawNoise() {
        // Draw an oval
        graphic.setColor(Color.DARK_GRAY);
        graphic.fillRect(0, 0, width, height);

        // lets make some noisy circles
        graphic.setColor(circleColor);
        for (int i = 0; i < circlesToDraw; i++) {
            int circleRadius = random.nextInt(height / 2);
            int circleX = random.nextInt(width) - circleRadius;
            int circleY = random.nextInt(height) - circleRadius;
            graphic.drawOval(circleX, circleY, circleRadius * 2, circleRadius * 2);
        }
    }

    private void drawText() {
        graphic.setColor(textColor);
        graphic.setFont(textFont);

        FontMetrics fontMetrics = graphic.getFontMetrics();
        int maxAdvance = fontMetrics.getMaxAdvance();
        int fontHeight = fontMetrics.getHeight();

        float spaceForLetters = -horizMargin * 2 + width;
        float spacePerChar = spaceForLetters / (charsToPrint - 1.0f);

        for (int i = 0; i < charsToPrint; i++) {
            char characterToShow = captchaText.charAt(i);

            int charWidth = fontMetrics.charWidth(characterToShow);
            int charDim = Math.max(maxAdvance, fontHeight);
            int halfCharDim = (charDim / 2);

            BufferedImage charImage = new BufferedImage(charDim, charDim, BufferedImage.TYPE_INT_ARGB);
            Graphics2D charGraphics = charImage.createGraphics();
            charGraphics.translate(halfCharDim, halfCharDim);
            double angle = (Math.random() - 0.5) * rotationRange;
            charGraphics.transform(AffineTransform.getRotateInstance(angle));
            charGraphics.translate(-halfCharDim, -halfCharDim);
            charGraphics.setColor(textColor);
            charGraphics.setFont(textFont);

            int charX = (int) (0.5 * charDim - 0.5 * charWidth);
            charGraphics.drawString("" + characterToShow, charX,
                    ((charDim - fontMetrics.getAscent()) / 2 + fontMetrics.getAscent()));

            float x = horizMargin + spacePerChar * (i) - charDim / 2.0f;
            int y = ((height - charDim) / 2);
            graphic.drawImage(charImage, (int) x, y, charDim, charDim, null, null);

            charGraphics.dispose();
        }
    }
}
