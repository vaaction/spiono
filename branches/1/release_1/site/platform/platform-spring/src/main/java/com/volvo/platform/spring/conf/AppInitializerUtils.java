package com.volvo.platform.spring.conf;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.core.env.ConfigurableEnvironment;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class AppInitializerUtils {

    public static String loadProfile(ConfigurableEnvironment env, String propertyName, String defaultProfile) {
        String systemProfile = readSystemProperty(env, propertyName);
        String[] knownProfileNames =  { ProfileDev.name, ProfileProduction.name, ProfileTest.name };
        for(String knownProfile: knownProfileNames) {
            if (knownProfile.equals(systemProfile)) {
                return knownProfile;
            }
        }
        return defaultProfile;
    }


    public static String readSystemProperty(ConfigurableEnvironment env, String propertyName) {
        return (String) env.getSystemProperties().get(propertyName);
    }
}
