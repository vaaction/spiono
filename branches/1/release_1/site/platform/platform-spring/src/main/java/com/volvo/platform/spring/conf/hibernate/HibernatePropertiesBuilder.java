package com.volvo.platform.spring.conf.hibernate;

import lombok.Getter;

import javax.annotation.concurrent.NotThreadSafe;
import java.util.Properties;

@NotThreadSafe
public class HibernatePropertiesBuilder {

    @Getter
    private final Properties properties = new Properties();

    public HibernatePropertiesBuilder withDialect(String dialect) {
        return setProperty(HibernateProperty.Dialect, dialect);
    }

    public HibernatePropertiesBuilder withSqlFormatting(boolean withSqlFormatting) {
        return setProperty(HibernateProperty.FormatSQL, withSqlFormatting);
    }

    public HibernatePropertiesBuilder withNamingStrategy(String namingStrategy) {
        return setProperty(HibernateProperty.NamingStrategy, namingStrategy);
    }

    public HibernatePropertiesBuilder withShowingSql(boolean withShowingSql) {
        return setProperty(HibernateProperty.ShowSQL, withShowingSql);
    }

    public HibernatePropertiesBuilder withSearchProvider(String searchProvider) {
        return setProperty(HibernateProperty.SearchProvider, searchProvider);
    }

    public HibernatePropertiesBuilder withSearchDir(String searchDir) {
        return setProperty(HibernateProperty.SearchDir, searchDir);
    }

    public HibernatePropertiesBuilder withUnicodeSuppport() {
        setProperty("useUnicode", "true");
        return setProperty("characterEncoding","UTF-8");
    }

    private HibernatePropertiesBuilder setProperty(HibernateProperty property, boolean value) {
        return setProperty(property, String.valueOf(value));
    }

    private HibernatePropertiesBuilder setProperty(HibernateProperty property, String value) {
        return setProperty(property.toString(), value);
    }

    private HibernatePropertiesBuilder setProperty(String key, String value) {
        properties.setProperty(key, value);
        return this;
    }
}
