package com.volvo.platform.gwt.shared.command;

import com.volvo.platform.gwt.shared.dto.GwtDTO;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public interface CommandDTO extends GwtDTO {


    @NotNull
    @NotEmpty
    String getName();

    void setName(String name);

    @NotNull
    @NotEmpty
    String getPayload();

    void setPayload(String payload);
}
