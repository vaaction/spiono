package com.volvo.platform.gwt.client.util;


import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.Paragraph;
import com.github.gwtbootstrap.client.ui.Tooltip;
import com.github.gwtbootstrap.client.ui.TooltipCellDecorator;
import com.github.gwtbootstrap.client.ui.base.HasType;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.constants.Placement;
import com.github.gwtbootstrap.client.ui.constants.Trigger;
import com.github.gwtbootstrap.client.ui.event.ShownEvent;
import com.github.gwtbootstrap.client.ui.event.ShownHandler;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.client.JavaScriptException;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class UiUtil {

    private static Map<String, IconType> classToIconTypeMap = new HashMap<String, IconType>();

    static {
        for (IconType icon: IconType.values()) {
            classToIconTypeMap.put(icon.get(), icon);
        }
    }

	private UiUtil() {
	}

    public static void hidePopupOnEscape(final PopupPanel popup) {
        popup.sinkEvents(Event.ONKEYDOWN);
        popup.addHandler(new KeyDownHandler(){
            @Override
            public void onKeyDown(KeyDownEvent event) {
                if (event.getNativeKeyCode() ==  KeyCodes.KEY_ESCAPE) {
                    popup.hide();
                }
            }
        }, KeyDownEvent.getType());
    }

    public static IconType getIconTypeByClass(String cssClass) {
        return classToIconTypeMap.get(cssClass);
    }

    public static void hideAllTooltips() {
        try {
            hideAllTooltips("*");
        }
        catch (JavaScriptException jsException) {
            // something went wrong in js. Were not tooltips to hide, so, jQuery selector didn't find anything
        }
    }

    public static native void hideAllTooltips(String selector) /*-{
        $wnd.jQuery(selector).tooltip('hide');
    }-*/;

    public static void hideTooltip(final com.google.gwt.dom.client.Element element) {
        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                Tooltip.changeVisibility(element, "hide");
            };
        });
    }

    public static void makeToolTip(final String selector, final String text) {
        scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                Tooltip.configure(selector, text, true, Placement.RIGHT.get(), Trigger.HOVER.get(), 0, 0);
            }
        });
    }

	public static void makeAgentStatusTooltip(String status, String text){
		makeToolTip("[statusTooltipSelector='"+status+"']", text);
	}

    public static<T> TooltipCellDecorator<T> tooltipCellDecorator(Cell<T> cell, String text, Placement placement) {
        TooltipCellDecorator<T> decorator = new TooltipCellDecorator<T>(cell);
        decorator.setText(text);
        decorator.setPlacement(placement);
        return decorator;
    }

    public static HasLoading hasLoadingForButtons(final com.github.gwtbootstrap.client.ui.Button ... buttons) {
        return new HasLoading() {
            @Override
            public void loading() {
                for (com.github.gwtbootstrap.client.ui.Button button: buttons) {
                    button.state().loading();
                }
            }

            @Override
            public void completed() {
                for (com.github.gwtbootstrap.client.ui.Button button: buttons) {
                    button.state().complete();
                }
            }

            @Override
            public void reset() {
                for (com.github.gwtbootstrap.client.ui.Button button: buttons) {
                    button.state().reset();
                }
            }
        };
    }

    public static<T extends HasText> T[] setTextToEmptyString(T ... textBoxes) {
        for (T textBox: textBoxes) {
            textBox.setText("");
        }
        return textBoxes;
    }

    public static<T extends HasType<ControlGroupType>> T[] setControlGroupType(ControlGroupType type,
                                                                               T ... controlGroups) {
        for (T controlGroup: controlGroups) {
            controlGroup.setType(type);
        }
        return controlGroups;
    }

    public static<T extends Paragraph> T[] setTextToEmptyStringForParagraph(T ... paragraphs) {
        for (T paragraph: paragraphs) {
            paragraph.setText("");
        }
        return paragraphs;
    }

    public static String getHiddenFieldValue(String id) {
        Element element = DOM.getElementById(id);
        return element == null ? null : element.getAttribute("value");
    }

	public static AcceptsOneWidget wrapDiv(String divId) {
		SimplePanel panel = new SimplePanel();
		panel.setSize("100%", "100%");
		RootPanel.get(divId).add(panel);
		return panel;
	}

    public static void setFocusAfterModalIsShown(Modal modal, final Focusable toFocus) {
        modal.addShownHandler(new ShownHandler() {
            @Override
            public void onShown(ShownEvent shownEvent) {
                UiUtil.setFocus(toFocus);
            }
        });
    }

	public static void setFocus(final Focusable uiElement) {
		scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				uiElement.setFocus(true);
			}
		});
	}

	public static void scheduleDeferred(Scheduler.ScheduledCommand command) {
		Scheduler.get().scheduleDeferred(command);
	}

	public static HandlerRegistration removeHandlerAndRetNull(HandlerRegistration handler) {
		if (handler != null) {
			handler.removeHandler();
		}
		return null;
	}

	public static HandlerRegistration ensureKeyUpHandler(HandlerRegistration old, HasKeyUpHandlers widget,
			KeyUpHandler handler) {
		if (old == null) {
			old = widget.addKeyUpHandler(handler);
		}
		return old;
	}

    public static String formatDateMedium(Date date) {
        return DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_MEDIUM).format(date);
    }

    public static String formatTimeShort(Date date) {
        return DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.TIME_SHORT).format(date);
    }
}
