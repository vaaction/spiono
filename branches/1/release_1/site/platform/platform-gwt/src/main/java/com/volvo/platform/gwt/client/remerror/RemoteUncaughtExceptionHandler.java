package com.volvo.platform.gwt.client.remerror;

import com.google.gwt.core.client.GWT;
import com.volvo.platform.gwt.client.command.CommandDispatcher;

import static com.volvo.platform.gwt.client.remerror.RemoteErrorUtils.sendRemoteErrorCommand;

public class RemoteUncaughtExceptionHandler implements GWT.UncaughtExceptionHandler {

    private final CommandDispatcher commandDispatcher;

    public RemoteUncaughtExceptionHandler(CommandDispatcher commandDispatcher) {
        this.commandDispatcher = commandDispatcher;
    }

    @Override
    public void onUncaughtException(Throwable e) {
        sendRemoteErrorCommand(commandDispatcher, e);
    }

    public static void setUp(final CommandDispatcher dispatcher){
        RemoteUncaughtExceptionHandler handler = new RemoteUncaughtExceptionHandler(dispatcher);
        GWT.setUncaughtExceptionHandler(handler);
    }
}
