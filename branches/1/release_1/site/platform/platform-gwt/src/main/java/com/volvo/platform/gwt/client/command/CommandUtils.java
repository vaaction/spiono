package com.volvo.platform.gwt.client.command;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.volvo.platform.gwt.shared.command.CommandAutoBeanFactory;
import com.volvo.platform.gwt.shared.command.CommandDTO;

public final class CommandUtils {
    private CommandUtils() {}

    private static final CommandAutoBeanFactory commandAutoBeanFactory = GWT.create(CommandAutoBeanFactory.class);

    public static AutoBean<CommandDTO> createCommandBean(AutoBean<?> dtoToWrap) {
        return createCommandBean(dtoToWrap.getType().getName(), dtoToWrap);
    }

    public static String getAutoBeanPayload(AutoBean<?> autoBean) {
        return AutoBeanCodex.encode(autoBean).getPayload();
    }

    private static AutoBean<CommandDTO> createCommandBean(String commandName, AutoBean<?> payload) {
        return createCommandBean(commandName, getAutoBeanPayload(payload));
    }

    private static AutoBean<CommandDTO> createCommandBean(String commandName, String payload) {
        AutoBean<CommandDTO> commandBean = commandAutoBeanFactory.command();
        CommandDTO command = commandBean.as();
        command.setName(commandName);
        command.setPayload(payload);
        return commandBean;
    }
}
