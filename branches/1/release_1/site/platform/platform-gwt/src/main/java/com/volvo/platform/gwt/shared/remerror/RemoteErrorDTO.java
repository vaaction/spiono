package com.volvo.platform.gwt.shared.remerror;

import com.volvo.platform.gwt.shared.dto.GwtDTO;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public interface RemoteErrorDTO extends GwtDTO {

    @NotNull
    @NotEmpty
    String getMessage();

    void setMessage(String message);
}
