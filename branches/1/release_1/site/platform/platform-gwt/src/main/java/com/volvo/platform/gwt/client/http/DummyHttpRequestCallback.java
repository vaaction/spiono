package com.volvo.platform.gwt.client.http;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.Response;

public class DummyHttpRequestCallback implements HttpRequestCallback {
    @Override
    public void onSuccess(Request request, Response response) {
    }

    @Override
    public void onFailure(RequestFailure failure) {
    }
}
