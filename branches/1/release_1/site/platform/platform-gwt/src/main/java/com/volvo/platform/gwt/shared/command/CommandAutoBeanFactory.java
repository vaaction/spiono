package com.volvo.platform.gwt.shared.command;


import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;

public interface CommandAutoBeanFactory extends AutoBeanFactory {

    AutoBean<CommandDTO> command();
}
