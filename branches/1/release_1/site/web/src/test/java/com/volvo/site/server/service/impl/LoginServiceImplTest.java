package com.volvo.site.server.service.impl;

import com.volvo.site.server.dto.KeyLogDTO;
import com.volvo.site.server.model.ModelConstants;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.KeyLogJpaRepository;
import com.volvo.site.server.util.SpyKeyLogDataDTOMocker;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.volvo.platform.java.testing.TestUtils.randomLong;
import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class LoginServiceImplTest {

    private LogsServiceImpl logsService;

    @Mock
    private KeyLogJpaRepository keyLogJpaRepository;

    @Mock
    private AgentJpaRepository agentJpaRepository;

    @BeforeMethod
    public void beforeTest() {
        initMocks(this);

        logsService = spy(new LogsServiceImpl(agentJpaRepository, keyLogJpaRepository));
    }


    @Test(expectedExceptions = NullPointerException.class)
    public void shouldLogAgentDataThrowNPEWhenDTOIsNull() {
        logsService.writeLogs(null);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldLogAgentDataThrowNPEWhenCertificateIsNull() {
        KeyLogDTO dto = mock(KeyLogDTO.class);
        when(dto.getCertificate()).thenReturn(null);
        logsService.writeLogs(dto);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldLogAgentDataThrowIllegalArgWhenCertificateIsBad() {
        KeyLogDTO dto = mock(KeyLogDTO.class);
        when(dto.getCertificate()).thenReturn(wrongAgentCertificate());
        logsService.writeLogs(dto);
    }

    @Test
    public void shouldLogAgentDataReturnIfAgentNotFound() {
        KeyLogDTO dto = mock(KeyLogDTO.class);
        when(dto.getCertificate()).thenReturn(randomAgentCertificate());
        when(agentJpaRepository.findByCertificate(anyString())).thenReturn(null);

        logsService.writeLogs(dto);

        verify(dto, never()).getKeyLogs();
    }

    @Test
    public void shouldLogAgentDataReturnIfAgentOnPause() {
        KeyLogDTO dto = mock(KeyLogDTO.class);
        Agent agent = mock(Agent.class);

        when(dto.getCertificate()).thenReturn(randomAgentCertificate());
        when(agentJpaRepository.findByCertificate(anyString())).thenReturn(agent);
        when(agent.isOnPause()).thenReturn(true);

        logsService.writeLogs(dto);

        verify(dto, never()).getKeyLogs();
    }

    @Test
    public void shouldLogAgentDataReturnIfAgentIsNotRegistered() {
        KeyLogDTO dto = mock(KeyLogDTO.class);
        Agent agent = mock(Agent.class);

        when(dto.getCertificate()).thenReturn(randomAgentCertificate());
        when(agentJpaRepository.findByCertificate(anyString())).thenReturn(agent);
        when(agent.isOnPause()).thenReturn(false);
        when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_CREATED);

        logsService.writeLogs(dto);

        verify(dto, never()).getKeyLogs();
    }

    @Test
    public void shouldNotLogAgentDataIfTestIsNotPrintable() {
        String certificate = randomAgentCertificate();
        Agent agent = mockRegisteredOnlineAgent();
        KeyLogDTO dto = new SpyKeyLogDataDTOMocker().spyLogDTO(certificate);
        dto.getKeyLogs()[0].setText("\r\n");

        when(agentJpaRepository.findByCertificate(certificate)).thenReturn(agent);

        logsService.writeLogs(dto);

        verify(keyLogJpaRepository, times(0)).save(any(Iterable.class));
    }

    @Test
    public void logAgentDataTest() {
        SpyKeyLogDataDTOMocker mocker = new SpyKeyLogDataDTOMocker();
        String certificate = randomAgentCertificate();
        Agent agent = mockRegisteredOnlineAgent();
        KeyLogDTO dto = spy(mocker.spyLogDTO(certificate));

        when(agentJpaRepository.findByCertificate(certificate)).thenReturn(agent);
        logsService.writeLogs(dto);

        verify(dto).getKeyLogs();
        verify(keyLogJpaRepository).save(any(Iterable.class));
    }

    private static String randomAgentCertificate() {
        return randomAlphanumeric(ModelConstants.agentCertificateLen);
    }

    private Agent mockRegisteredOnlineAgent() {
        Agent agent = mock(Agent.class);
        when(agent.getId()).thenReturn(randomLong());
        when(agent.isOnPause()).thenReturn(false);
        when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_REGISTERED);
        return agent;
    }

    private static String wrongAgentCertificate() {
        int noise = System.currentTimeMillis() % 2 == 0 ? 1 : -1;
        return randomAlphanumeric(ModelConstants.agentCertificateLen + noise);
    }
}
