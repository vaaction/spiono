package com.volvo.site.server.util.converter;

import com.google.web.bindery.autobean.vm.AutoBeanFactorySource;
import com.volvo.platform.gwt.shared.dto.PageResultDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.BeanFactory;
import com.volvo.site.server.converter.PagingToDTOConverter;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class PagingToDTOConverterTest {

    @Mock
    private Page page;

    private final BeanFactory beanFactoryHolder = AutoBeanFactorySource.create(BeanFactory.class);

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    @Test
    public void pageToPageResultDTOConvertTest(){
        //Given
        returnFakePage(page);
        PageResultDTO pageResultDTO = beanFactoryHolder.create(PageResultDTO.class).as();

        //When
        new PagingToDTOConverter(page).apply(pageResultDTO);

        //Then
        checkPagination(pageResultDTO);
    }

    public static void returnFakePage(Page page){
        when(page.getSize()).thenReturn(10);
        when(page.getNumber()).thenReturn(1);
        when(page.getNumberOfElements()).thenReturn(10);
        when(page.getTotalElements()).thenReturn(100L);
        when(page.getTotalPages()).thenReturn(10);
        when(page.hasNextPage()).thenReturn(true);
        when(page.hasPreviousPage()).thenReturn(false);
        when(page.isFirstPage()).thenReturn(true);
        when(page.isLastPage()).thenReturn(false);
    }


    public static void checkPagination(PageResultDTO page){
        assertEquals(page.getPageSize(), 10);
        assertEquals(page.getPageNumber(), 1);
        assertEquals(page.getNumberOfElements(), 10);
        assertEquals(page.getTotalElements(), 100);
        assertEquals(page.getTotalPages(), 10);
        assertEquals(page.isHasNextPage(), true);
        assertEquals(page.isHasPreviousPage(), false);
        assertEquals(page.getIsFirstPage(), true);
        assertEquals(page.getIsLastPage(), false);
    }

}
