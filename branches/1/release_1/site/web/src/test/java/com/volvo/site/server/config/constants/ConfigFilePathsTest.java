package com.volvo.site.server.config.constants;

import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static com.volvo.platform.java.testing.TestUtils.isFileExistsInClassPath;
import static org.testng.Assert.assertTrue;

public class ConfigFilePathsTest {

    @Test
    public void testAppContextExists() throws FileNotFoundException {
        assertTrue(isFileExistsInClassPath(ConfigFilePaths.APP_CONTEXT_CLASSPATH));
    }

    @Test
    public void testDBDevConfigFileExists() throws FileNotFoundException {
        assertTrue(isFileExistsInClassPath(ConfigFilePaths.DB_DEV_CONFIG_CLASSPATH));
        assertTrue(isFileExistsInClassPath(ConfigFilePaths.DB_TEST_CONFIG_CLASSPATH));
        assertTrue(isFileExistsInClassPath(ConfigFilePaths.DB_PROD_CONFIG_CLASSPATH));
    }

    @Test
    public void testWinAppConfigFileExists() throws FileNotFoundException {
        assertTrue(isFileExistsInClassPath(ConfigFilePaths.APP_TEST_CONFIG_CLASSPATH));
        assertTrue(isFileExistsInClassPath(ConfigFilePaths.APP_DEV_CONFIG_CLASSPATH));
        assertTrue(isFileExistsInClassPath(ConfigFilePaths.APP_PROD_CONFIG_CLASSPATH));
    }
}
