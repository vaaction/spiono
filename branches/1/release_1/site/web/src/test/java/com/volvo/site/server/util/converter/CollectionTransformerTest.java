package com.volvo.site.server.util.converter;

import com.google.common.base.Function;
import org.mockito.Mock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.volvo.platform.java.CollectionsTransformer.transformToList;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class CollectionTransformerTest {

    @Mock
    private Function<Integer, String> converter;

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    @Test
    public void collectionTransformerTest(){
        //Given
        when(converter.apply(anyInt())).thenReturn("ConvertedString");

        List<Integer> integerList = new ArrayList<Integer>();
        for(int i = 0; i < 10; i++){
            integerList.add(i);
        }

        //When
        List<String> stringList = transformToList(integerList, converter);

        //Then
        assertEquals(stringList.size(), integerList.size());
        verify(converter, times(integerList.size())).apply(anyInt());
    }
}
