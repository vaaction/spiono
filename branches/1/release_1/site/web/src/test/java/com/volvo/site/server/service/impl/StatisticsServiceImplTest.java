package com.volvo.site.server.service.impl;

import com.volvo.platform.java.ArrayIterable;
import com.volvo.site.server.dto.StatisticsLogDTO;
import com.volvo.site.server.model.ModelConstants;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.StatisticsLog;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.StatisticsJpaRepository;
import com.volvo.site.server.repository.UserSettingsJpaRepository;
import com.volvo.site.server.service.StatisticsService;
import com.volvo.site.server.util.ProcessBlackList;
import com.volvo.site.server.util.SpyStatisticsDataDTOMocker;
import org.apache.commons.lang3.RandomStringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;


public class StatisticsServiceImplTest{

	private StatisticsService statisticsService;
	@Mock
	public UserSettingsJpaRepository userSettingsJpaRepository;
	@Mock
	public StatisticsJpaRepository statisticsJpaRepository;
	@Mock
	public AgentJpaRepository agentJpaRepository;

	@BeforeMethod
	public void tearUp(){
		initMocks(this);
		statisticsService = new StatisticsServiceImpl(statisticsJpaRepository, agentJpaRepository);
	}

	@Test
	public void listStatisticForAgentTest(){
		List<StatisticsLog> statistics = new ArrayList<StatisticsLog>();
		when(statisticsJpaRepository.findByAgentId(1L, false, null, null, ProcessBlackList.getList())).thenReturn(statistics);

		statisticsService.listStatisticForAgent(1L, null, null);
		verify(statisticsJpaRepository, times(1)).findByAgentId(1L, false, null, null, ProcessBlackList.getList());

		Date searchDate = new Date();
		statisticsService.listStatisticForAgent(1L, searchDate , searchDate);
		verify(statisticsJpaRepository, times(1)).findByAgentId(1L, true, searchDate, searchDate, ProcessBlackList.getList());
	}

	@Test(expectedExceptions = NullPointerException.class)
	public void logAgentDataBadCertificateIsNullTest(){
		StatisticsLogDTO spyLog = new SpyStatisticsDataDTOMocker().spyStatisticsDTO(null);
		statisticsService.logAgentData(spyLog);
	}

	@Test(expectedExceptions = IllegalArgumentException.class)
	 public void logAgentDataBadCertificateIsLowerTest(){
		StatisticsLogDTO spyLog = new SpyStatisticsDataDTOMocker().spyStatisticsDTO(RandomStringUtils
				.random(ModelConstants.agentCertificateLen - 1));
		statisticsService.logAgentData(spyLog);
	}

	@Test(expectedExceptions = IllegalArgumentException.class)
	public void logAgentDataBadCertificateIsHigherTest(){
		StatisticsLogDTO spyLog = new SpyStatisticsDataDTOMocker().spyStatisticsDTO(RandomStringUtils
				.random(ModelConstants.agentCertificateLen + 1));
		statisticsService.logAgentData(spyLog);
	}

	@Test
	public void logAgentDataSuccessfulTest(){
		//Given
		Agent agent = mockAgent();
		String certificate = createCertificate();
		when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_REGISTERED);
		when(agentJpaRepository.findByCertificate(certificate)).thenReturn(agent);

		//When
		statisticsService.logAgentData(createLogData(certificate));

		//Then
		verify(agentJpaRepository, times(1)).findByCertificate(certificate);
		verify(statisticsJpaRepository, times(1)).save(any(ArrayIterable.class));
	}

	@Test
	public void logAgentDataCannotLogIfAgentStatusDeletedTest(){
		//Given
		Agent agent = mockAgent();
		String certificate = createCertificate();
		when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_DELETED);
		when(agentJpaRepository.findByCertificate(certificate)).thenReturn(agent);

		//When
		statisticsService.logAgentData(createLogData(certificate));
		//Then
		verifyLogNotSaved(certificate);
	}

	@Test
	public void logAgentDataCannotLogIfAgentStatusCreatedTest(){
		//Given
		Agent agent = mockAgent();
		String certificate = createCertificate();
		when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_CREATED);
		when(agentJpaRepository.findByCertificate(certificate)).thenReturn(agent);

		//When
		statisticsService.logAgentData(createLogData(certificate));
		//Then
		verifyLogNotSaved(certificate);
	}

	@Test
	public void logAgentDataCannotLogIfAgentStatusDownloadedTest(){
		//Given
		Agent agent = mockAgent();
		String certificate = createCertificate();
		when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_DOWNLOADED);
		when(agentJpaRepository.findByCertificate(certificate)).thenReturn(agent);
		//When
		statisticsService.logAgentData(createLogData(certificate));
		//Then
		verifyLogNotSaved(certificate);
	}

	@Test
	public void logAgentDataCannotLogIfAgentOnPauseTest(){
		//Given
		Agent agent = mockAgent();
		String certificate = createCertificate();
		when(agent.isOnPause()).thenReturn(true);
		when(agentJpaRepository.findByCertificate(certificate)).thenReturn(agent);
		//When
		statisticsService.logAgentData(createLogData(certificate));
		//Then
		verifyLogNotSaved(certificate);
	}

	@Test
	public void logAgentDataCannotLogIfAgentNullTest(){
		//Given
		String certificate = createCertificate();
		when(agentJpaRepository.findByCertificate(certificate)).thenReturn(null);
		//When
		statisticsService.logAgentData(createLogData(certificate));
		//Then
		verifyLogNotSaved(certificate);
	}

	private Agent mockAgent(){
		return mock(Agent.class);
	}

	private String createCertificate(){
		return RandomStringUtils.random(ModelConstants.agentCertificateLen);
	}

	private StatisticsLogDTO createLogData(String certificate){
		return new SpyStatisticsDataDTOMocker().spyStatisticsDTO(certificate);
	}

	private void verifyLogNotSaved(String certificate){
		verify(agentJpaRepository, times(1)).findByCertificate(certificate);
		verify(statisticsJpaRepository, times(0)).save(any(ArrayIterable.class));
	}
}
