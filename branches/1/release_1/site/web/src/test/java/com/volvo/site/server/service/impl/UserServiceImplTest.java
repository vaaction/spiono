package com.volvo.site.server.service.impl;

import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.model.entity.UserSettings;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.KeyLogJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.repository.UserSettingsJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.impl.util.AgentUtilService;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;


public class UserServiceImplTest {

    private UserServiceImpl userService;

    @Mock
    private UserJpaRepository userJpaRepository;

    @Mock
    private User user;

    @Mock
    private AgentJpaRepository agentJpaRepository;

    @Mock
    private ClientInstaller clientInstaller;

    @Mock
    private AgentInstaller agentInstaller;

    @Mock
    private KeyLogJpaRepository keyLogJpaRepository;

	@Mock
	private UserSettingsJpaRepository userSettingsJpaRepository;

	@Mock
    private AgentUtilService agentUtilService;

    @Mock
    private UserSettings userSettings;

    @BeforeMethod
    public void beforeTest() {
        initMocks(this);

        userService = spy(new UserServiceImpl(userJpaRepository, userSettingsJpaRepository
        ));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldBeNPEInChangePasswordWhenOldPasswordIsNull() throws Exception {
        userService.changePassword(0, null, "password");
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldBeNPEInChangePasswordWhenNewPasswordIsNull() throws Exception {
        userService.changePassword(0, "password", null);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldBeEntityNotFoundExceptionInChangePasswordWhenUserNotFound() {
        mockReturningNullFakeUser();
        userService.changePassword(-1, "password", "password");
    }

    @Test
    public void shouldReturnFalseInChangePasswordWhenOldPasswordIsWrong() {
        mockReturningFakeUserFromJpaRepository();
        mockTryingUpdatePasswordInUserRetutns(false);
        assertFalse(userService.changePassword(0, "oldPwd", "newPwd"));
    }

    @Test
    public void changePasswordPositiveTest() {
        mockReturningFakeUserFromJpaRepository();
        mockTryingUpdatePasswordInUserRetutns(true);

        assertTrue(userService.changePassword(0, "oldPwd", "newPwd"));
        verify(userJpaRepository).save(user);
    }

    @Test
    public void updateLastLogsSearchSettingsTest() {
        // Given
        long userId = 5;
        mockGettingAndReturningUserSettings(userId);

        // When
        UserSettings result = userService.updateLastLogsSearchSettings(userId, "phrase", "status");

        // Than
        assertSame(result, userSettings);
        verifyUserSettingsAndRepositoryIteractions(userId);
        verify(userSettings).setLastAgentsSearchPhrase("phrase");
        verify(userSettings).setLastAgentsSearchStatus("status");
    }

    @Test
    public void updateLastLogsSearchAgentIdTest() {
        // Given
        long userId = 5;
        long selectedAgentId = 10;
        mockGettingAndReturningUserSettings(userId);

        // When
        UserSettings result = userService.updateLastLogsSearchAgentId(userId, selectedAgentId);

        // Than
        assertSame(result, userSettings);
        verifyUserSettingsAndRepositoryIteractions(userId);
        verify(userSettings).setLastLogsSearchAgentId(selectedAgentId);
    }

    @Test
    public void updateLastStatAgentIdTest() {
        // Given
        long userId = 5;
        long selectedAgentId = 10;
        mockGettingAndReturningUserSettings(userId);

        // When
        UserSettings result = userService.updateLastStatAgentId(userId, selectedAgentId);

        // Than
        assertSame(result, userSettings);
        verifyUserSettingsAndRepositoryIteractions(userId);
        verify(userSettings).setLastStatAgentId(selectedAgentId);
    }

    @Test
    public void getUserSettingsReturnIfExists() {
        // Given
        long userId = 5;
        when(userSettingsJpaRepository.findByUserId(userId)).thenReturn(userSettings);

        // When
        UserSettings result = userService.getUserSettings(userId);

        // Than
        assertSame(result, userSettings);
    }

    @Test
    public void getUserSettingsCreateNewAndReturnIfNotExists() {
        // Given
        long userId = 5;
        when(userSettingsJpaRepository.findByUserId(userId)).thenReturn(null);
        doAnswer(returnsFirstArg()).when(userSettingsJpaRepository).save(any(UserSettings.class));

        // When
        UserSettings result = userService.getUserSettings(userId);

        // Than
        assertNotSame(result, userSettings);
        assertEquals(result.getUserId(), userId);
        verify(userSettingsJpaRepository).save(result);
    }

    private void mockGettingAndReturningUserSettings(long userId) {
        doReturn(userSettings).when(userService).getUserSettings(userId);
        when(userSettingsJpaRepository.save(userSettings)).thenReturn(userSettings);
    }

    private void verifyUserSettingsAndRepositoryIteractions(long userId) {
        verify(userService).getUserSettings(userId);
        verify(userSettingsJpaRepository).save(userSettings);
    }

    private void mockReturningNullFakeUser() {
        when(userJpaRepository.findActiveUserById(anyLong())).thenReturn(null);
    }

    private void mockReturningFakeUserFromJpaRepository() {
        when(userJpaRepository.findActiveUserById(anyLong())).thenReturn(user);
    }

    private void mockTryingUpdatePasswordInUserRetutns(boolean returns) {
        when(user.tryUpdatePassword(anyString(), anyString())).thenReturn(returns);
    }
}
