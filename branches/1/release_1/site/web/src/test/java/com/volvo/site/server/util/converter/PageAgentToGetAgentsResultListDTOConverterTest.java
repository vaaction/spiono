package com.volvo.site.server.util.converter;

import com.google.web.bindery.autobean.vm.AutoBeanFactorySource;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.BeanFactory;
import com.volvo.site.gwt.usercabinet.shared.dto.PagedAgentsListDTO;
import com.volvo.site.gwt.usercabinet.shared.enums.AgentStatus;
import com.volvo.site.server.converter.PageAgentToGetAgentsResultListDTOConverter;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.service.UserService;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.volvo.site.server.util.converter.PagingToDTOConverterTest.checkPagination;
import static com.volvo.site.server.util.converter.PagingToDTOConverterTest.returnFakePage;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class PageAgentToGetAgentsResultListDTOConverterTest {

    @Mock
    UserService userService;

    @Mock
    Agent agent;

    @Mock
    Page<Agent> agentPage;

    private final BeanFactory beanFactoryHolder = AutoBeanFactorySource.create(BeanFactory.class);

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    @Test
    public void pageAgentToResultListDTOConvertTest(){
        //Given
        returnFakePage(agentPage);
        returnFakeAgentPage();

        //When
        PagedAgentsListDTO dtoList = new PageAgentToGetAgentsResultListDTOConverter
                (beanFactoryHolder).apply(agentPage);
        List<AgentDTO> agentsList = dtoList.getUserAgents();

        //Then
        checkPagination(dtoList);
        checkFakeAgentPage(agentsList);
    }

    private void returnFakeAgentPage(){
       List<Agent> agentList = new ArrayList<Agent>();
       agentList.add(agent);

       when(agent.getId()).thenReturn(1000L);
       when(agent.getName()).thenReturn("FakeAgent");
       when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_DELETED);
       when(agentPage.getContent()).thenReturn(agentList);
    }

    private void checkFakeAgentPage(List<AgentDTO> agentsList){
        assertEquals(agentsList.size(), 1);
        assertEquals(agentsList.get(0).getStatus(), AgentStatus.DELETED);
        assertEquals(agentsList.get(0).getAgentId(), 1000L);
        assertEquals(agentsList.get(0).getName(), "FakeAgent");
    }

}
