package com.volvo.site.server.service.facade;


import com.google.common.collect.Lists;
import com.volvo.site.gwt.usercabinet.shared.dto.GetKeyLogDataDTO;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.KeyLog;
import com.volvo.site.server.service.AgentService;
import com.volvo.site.server.service.LogsService;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@PrepareForTest({LogsSearchFacade.class, Lists.class })
public class LogsSearchFacadeTest extends PowerMockTestCase {

    @Mock
    LogsService logsService;

    @Mock
    AgentService agentService;

    LogsSearchFacade logsSearchFacade;

    @BeforeMethod
    public void beforeTest() {
        initMocks(this);
        logsSearchFacade = new LogsSearchFacade(logsService, agentService);
    }

    @Test
    public void performLogsSearchTest() {
        // Given
        PowerMockito.mockStatic(Lists.class);
        List<KeyLog> keylogs = mock(List.class);
        List<Agent> agents = mock(List.class);
        GetKeyLogDataDTO searchParams = mock(GetKeyLogDataDTO.class);
        ArrayList<Agent.AgentStatus> agentStatuses = mock(ArrayList.class);

        // When
        PowerMockito.when(Lists.newArrayList(LogsSearchFacade.STATUSES_WITH_LOGS)).thenReturn(agentStatuses);
        when(logsService.search(searchParams)).thenReturn(keylogs);
        when(agentService.getAgents(5, agentStatuses)).thenReturn(agents);

        // Then
        LogsSearchFacade.PerformLogsSearchResult result = logsSearchFacade.performLogsSearch(5, searchParams);
        Assert.assertEquals(result.agents, agents);
        Assert.assertEquals(result.keylogs, keylogs);
    }
}
