package com.volvo.site.server.repository.testing;


import com.volvo.site.server.model.entity.HasCrDate;
import com.volvo.site.server.model.entity.HasLongId;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.testng.annotations.Test;

import static com.volvo.platform.java.JavaUtils.invokeConstructorQuietly;
import static org.testng.Assert.assertNotNull;


public abstract class HasCrDateTestBase<
        TModel extends HasCrDate & HasLongId,
        TRepository extends JpaRepository<TModel, Long>>
        extends AbstractFullSpringTest {

    private final Class<TModel> modelClass;

    protected HasCrDateTestBase(Class<TModel> modelClass) {
        this.modelClass = modelClass;
    }

    protected abstract TRepository getRepository();

    @Test
    public void createEntityTest() {
        Long createdId = getRepository().save(createModel()).getId();

        try {
            TModel installer = getRepository().findOne(createdId);
            assertNotNull(installer);
            assertNotNull(installer.getCrDate());
        }finally {
            getRepository().delete(createdId);
        }
    }

    protected TModel createModel() {
        return invokeConstructorQuietly(modelClass);
    }
}
