package com.volvo.site.server.service;

import com.volvo.site.server.model.entity.StatisticsLog;
import com.volvo.site.server.repository.StatisticsJpaRepository;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import static org.testng.Assert.*;

public class StatisticsSearchTest extends AbstractFullSpringTest{

	@Autowired
	private StatisticsJpaRepository statisticsJpaRepository;

	@Autowired
	private StatisticsService statisticsService;

	private HashSet<StatisticsLog> generatedStatistic = new HashSet<StatisticsLog>();

	private final long AGENT_ID_FOR_TEST  = 11;
	private final int NUMBER_OF_GEN_LOGS_FOR_TEST  = 10;
	private final String FIRST_PROCESS_NAME  = "process1";
	private final String SECOND_PROCESS_NAME  = "process2";
	private final String THIRD_PROCESS_NAME  = "process3";
	private final String FIRST_PROGRAM_NAME  = "program1";
	private final String SECOND_PROGRAM_NAME  = "program2";
	private final String THIRD_PROGRAM_NAME  = null;
	private final String FOURTH_PROCESS_NAME  = "process4";
	private final String FOURTH_PROGRAM_NAME  = "program4";
	private final Date BENCHMARK_DATE = new Date();
	private final Date START_DATE_FOR_FIRST_PROGRAM = new DateTime(BENCHMARK_DATE).minusDays(1).toDate();
	private final Date END_DATE_FOR_FIRST_PROGRAM = new DateTime(BENCHMARK_DATE).minusDays(1).plusMinutes(20).toDate();
	private final Date START_DATE_FOR_SECOND_PROGRAM = new DateTime(BENCHMARK_DATE).minusDays(2).toDate();
	private final Date END_DATE_FOR_SECOND_PROGRAM = new DateTime(BENCHMARK_DATE).minusDays(2).plusMinutes(80).toDate();
	private final Date START_DATE_FOR_THIRD_PROGRAM = new DateTime(BENCHMARK_DATE).plusDays(1).toDate();
	private final Date END_DATE_FOR_THIRD_PROGRAM = new DateTime(BENCHMARK_DATE).plusDays(1).plusMillis(1).toDate();
	private final Date START_DATE_FOR_FOURTH_PROGRAM = new DateTime(BENCHMARK_DATE).plusDays(1).toDate();
	private final Date END_DATE_FOR_FOURTH_PROGRAM = new DateTime(BENCHMARK_DATE).plusDays(10).plusMinutes(1).toDate();

	@BeforeClass
	@Rollback(value = false)
	public void setupUp(){
		for(int i = 0; i < NUMBER_OF_GEN_LOGS_FOR_TEST; i++){
			generatedStatistic.add(statisticsJpaRepository.saveAndFlush(generateStatisticLog(AGENT_ID_FOR_TEST,
					FIRST_PROCESS_NAME, FIRST_PROGRAM_NAME,START_DATE_FOR_FIRST_PROGRAM, END_DATE_FOR_FIRST_PROGRAM)));
		}

		for(int i = 0; i < NUMBER_OF_GEN_LOGS_FOR_TEST; i++){
			generatedStatistic.add(statisticsJpaRepository.saveAndFlush(generateStatisticLog(AGENT_ID_FOR_TEST,
					SECOND_PROCESS_NAME, SECOND_PROGRAM_NAME,START_DATE_FOR_SECOND_PROGRAM, END_DATE_FOR_SECOND_PROGRAM)));
		}

		generatedStatistic.add(statisticsJpaRepository.saveAndFlush(generateStatisticLog(AGENT_ID_FOR_TEST,
				THIRD_PROCESS_NAME, THIRD_PROGRAM_NAME, START_DATE_FOR_THIRD_PROGRAM , END_DATE_FOR_THIRD_PROGRAM)));

		generatedStatistic.add(statisticsJpaRepository.saveAndFlush(generateStatisticLog(AGENT_ID_FOR_TEST,
				FOURTH_PROCESS_NAME, FOURTH_PROGRAM_NAME, START_DATE_FOR_FOURTH_PROGRAM, END_DATE_FOR_FOURTH_PROGRAM)));
	}

	@Test
	public void shouldReturnProcessNameIfProgramNameIsNullTest(){
		Map<String, Integer> result = statisticsService.listStatisticForAgent(AGENT_ID_FOR_TEST,
				START_DATE_FOR_THIRD_PROGRAM, END_DATE_FOR_THIRD_PROGRAM);

		assertTrue(result.containsKey(THIRD_PROCESS_NAME));
		assertFalse(result.containsKey(THIRD_PROGRAM_NAME));
	}

	@Test
	public void shouldReturnOnePercentIfLessThanOneTest(){
		Map<String, Integer> result = statisticsService.listStatisticForAgent(AGENT_ID_FOR_TEST,
				BENCHMARK_DATE, new DateTime(BENCHMARK_DATE).plusDays(15).toDate());
		assertEquals(2, result.size());
		assertEquals(99, result.get(FOURTH_PROGRAM_NAME).intValue());
		assertEquals(1, result.get(THIRD_PROCESS_NAME).intValue());
	}

	 @Test
	  public void findAllStatisticForAgentWithoutDateRangeTest(){
		Map<String, Integer> result = statisticsService.listStatisticForAgent(AGENT_ID_FOR_TEST, null, null);
		assertEquals(result.size(), 4);
	}

	@Test
	public void findAllStatisticForAgentWithDateRangeTest(){
		Map<String, Integer> result = statisticsService.listStatisticForAgent(AGENT_ID_FOR_TEST, new DateTime(BENCHMARK_DATE)
				.minusDays(2).toDate(), BENCHMARK_DATE);
		assertEquals(2, result.size());
		assertEquals(20, result.get(FIRST_PROGRAM_NAME).intValue());
		assertEquals(80, result.get(SECOND_PROGRAM_NAME).intValue());
	}

	@Test
	public void findAllForFirstDateTest(){
		Map<String, Integer> result = statisticsService.listStatisticForAgent(AGENT_ID_FOR_TEST, new DateTime(BENCHMARK_DATE)
				.minusDays(1).toDate(), BENCHMARK_DATE);
		assertEquals(1, result.size());
		assertEquals(100, result.get(FIRST_PROGRAM_NAME).intValue());
	}

	@Test
	public void findAllForSecondDateTest(){
		Map<String, Integer> result = statisticsService.listStatisticForAgent(AGENT_ID_FOR_TEST, new DateTime(BENCHMARK_DATE)
				.minusDays(2).toDate(), new DateTime(BENCHMARK_DATE).minusDays(2).plusMinutes(80).toDate());
		assertEquals(1, result.size());
		assertEquals(100, result.get(SECOND_PROGRAM_NAME).intValue());
	}

	@AfterClass
	@Rollback(value = false)
	public void clearUp(){
		statisticsJpaRepository.delete(generatedStatistic);
	}

	private StatisticsLog generateStatisticLog(long agentId, String processName, String programName, Date startDate, Date endDate){
		StatisticsLog statisticsLog = new StatisticsLog();
		statisticsLog.setAgentId(agentId);
		statisticsLog.setProcessName(processName);
		statisticsLog.setProcessDescription(programName);
		statisticsLog.setStartDate(startDate);
		statisticsLog.setEndDate(endDate);
		statisticsLog.setTimeOfUse(endDate.getTime() - startDate.getTime());
		return statisticsLog;
	}
}
