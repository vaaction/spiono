package com.volvo.site.server.testutil.spring;

import com.volvo.site.server.config.constants.ConfigFilePaths;
import com.volvo.site.server.config.db.AbstractDbConfig;
import org.apache.commons.lang3.concurrent.ConcurrentException;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ImportResource(ConfigFilePaths.APP_CONTEXT_CLASSPATH) // todo: remove this in the next spring release
@EnableTransactionManagement
@PropertySource(ConfigFilePaths.DB_TEST_CONFIG_CLASSPATH)
@EnableJpaRepositories(com.volvo.site.server.repository.PackageInfo.PACKAGE_NAME)
@ComponentScan(basePackages = {
        com.volvo.site.server.repository.PackageInfo.PACKAGE_NAME,
        com.volvo.site.server.service.PackageInfo.PACKAGE_NAME,
        "com.volvo.site.server.testutil.components",
})
public class TestDBConfig extends AbstractDbConfig {
    @Override
    protected void doDatabaseMigrations() throws ConcurrentException {
        databaseMigrator().clean();
        super.doDatabaseMigrations();
    }
}
