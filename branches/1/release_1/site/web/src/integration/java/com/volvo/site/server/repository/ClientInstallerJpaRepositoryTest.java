package com.volvo.site.server.repository;


import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.repository.testing.InstallerJpaTestBase;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

public class ClientInstallerJpaRepositoryTest extends InstallerJpaTestBase<ClientInstaller, ClientInstallerJpaRepository> {

    @Autowired
    @Getter
    ClientInstallerJpaRepository repository;

    public ClientInstallerJpaRepositoryTest() {
        super(ClientInstaller.class);
    }
}
