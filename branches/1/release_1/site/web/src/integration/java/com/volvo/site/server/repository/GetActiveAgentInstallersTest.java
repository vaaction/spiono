package com.volvo.site.server.repository;


import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.volvo.site.server.testutil.RepositoryTestUtils.random255Bytes;
import static org.testng.Assert.*;

public class GetActiveAgentInstallersTest extends AbstractFullSpringTest {

    @Autowired
    AgentInstallerJpaRepository repository;

    @Autowired
    AgentInstallerRepository advRepository;

    @Autowired
    ClientInstallerJpaRepository clientJpaRepository;

    List<ClientInstaller> clientInstallers = newArrayList();

    @Test
    public void noActiveInstallersTest() {
        assertEquals(repository.getActiveCount(0L), 0L);
        assertFalse(advRepository.hasActiveAgentInstaller(0L));
    }

    @Test
    @Rollback(false)
    public void addTestDataTest() {
        for (int j = 0; j < 3; ++j) {
            ClientInstaller client = clientJpaRepository.save(ClientInstaller.of(random255Bytes));
            clientInstallers.add(client);
            for (int i = 0; i < 4; ++i) {
                repository.save(AgentInstaller.of(random255Bytes, client, i < 2));
            }
        }
    }

    @Test(dependsOnMethods = "addTestDataTest")
    @Rollback(false)
    public void checkTestDataTest() {
        long id = clientInstallers.get(0).getId();
        assertEquals(repository.getActiveCount(id), 2L);
        assertTrue(advRepository.hasActiveAgentInstaller(id));
    }

    @AfterClass
    @Rollback(false)
    public void afterClass() {
        clientJpaRepository.delete(clientInstallers);
    }
}
