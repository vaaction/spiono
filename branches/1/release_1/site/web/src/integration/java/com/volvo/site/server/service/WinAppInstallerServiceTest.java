package com.volvo.site.server.service;


import com.volvo.site.server.dto.UpdateAgentResult;
import com.volvo.site.server.model.ModelConstants;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolationException;

import static com.volvo.platform.java.IOUtils.bytesToBase64String;
import static com.volvo.site.server.testutil.RepositoryTestUtils.*;
import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.ArrayUtils.EMPTY_BYTE_ARRAY;
import static org.testng.Assert.*;
import static org.testng.AssertJUnit.assertTrue;

public class WinAppInstallerServiceTest extends AbstractWinappServiceTest {

    @Autowired
    UserJpaRepository userJpaRepository;

    @Autowired
    AgentJpaRepository agentJpaRepository;

    @Autowired
    AgentService agentService;

    @Test
    public void addClientInstallerTest() {
        ClientInstaller clientInstaller = newClientInstaller();
        assertNotNull(clientInstaller);
        assertNotNull(clientInstaller.getId());
        assertNotNull(clientInstaller.getData());
        assertNotNull(clientInstaller.getCrDate());
        assertFalse(clientInstaller.isActive());
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void addEmptyClientInstallerTest() {
        newClientInstaller(EMPTY_BYTE_ARRAY);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void addNullClientInstallerTest() {
        newClientInstaller(null);
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void addTooLargeClientInstallerTest() {
        newClientInstaller(mysqlMediumBlobDataPlusOneByte());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void addAgentInstallerWithNullDataTest() {
        newInactiveAgentInstaller(0L, null);
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void addAgentInstallerWithEmptyDataTest() {
        newInactiveAgentInstaller(newClientInstaller().getId(), EMPTY_BYTE_ARRAY);
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void addAgentInstallerWithTooLargeDataTest() {
        newInactiveAgentInstaller(newClientInstaller().getId(), mysqlMediumBlobDataPlusOneByte());
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void addAgentInstallerWithNotExistingClientInstallerTest() {
        service.clearAll();
        newInactiveAgentInstaller(1L, random255Bytes);
    }

    @Test
    public void addAgentInstallerTest() {
        AgentInstaller installer = addValidAgentInstaller() ;
        assertNotNull(installer);
        assertNotNull(installer.getId());
        assertNotNull(installer.getData());
        assertNotNull(installer.getCrDate());
        assertFalse(installer.isActive());
        assertNotNull(installer.getClientInstaller());
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void activateAgentInstallerIfNotExistsTest() {
        service.activateAgentInstaller(-1L);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldUpdateAgentLibThrowEntityNotFoundIfAgentNotFound() {
        service.updateAgentLib(randomAgentCertificate());
    }

    @Test
    public void shouldNotUpdateAgentLib() {
        newActiveClientAndAgentInstallers();
        UpdateAgentResult result = service.updateAgentLib(createUserAndAgent().getCertificate());
        assertFalse(result.isHasUpdate());
    }

    @Test
    public void shouldUpdateAgentLib() {
        Pair<ClientInstaller, AgentInstaller> installers = newActiveClientAndAgentInstallers();
        Agent agent = createUserAndAgent();

        AgentInstaller newAgentInstaller = newActiveAgentInstaller(installers.getLeft().getId());

        UpdateAgentResult result = service.updateAgentLib(agent.getCertificate());

        assertTrue(result.isHasUpdate());
        assertEquals(result.getAgent(), bytesToBase64String(newAgentInstaller.getData()));
    }

    private Agent createUserAndAgent() {
        User user = userJpaRepository.save(createRandomActiveUser());
        Agent agent = agentService.createAgent(user.getId());
        agent.setStatus(Agent.AgentStatus.AGENT_DOWNLOADED);
        agent = agentJpaRepository.save(agent);

        agentService.registerAgent(agent.getClientKey());
        return agent;
    }

    private AgentInstaller addValidAgentInstaller() {
        return newInactiveAgentInstaller(newClientInstaller().getId());
    }

    private static String randomAgentCertificate() {
        return randomAlphanumeric(ModelConstants.agentCertificateLen);
    }
}
