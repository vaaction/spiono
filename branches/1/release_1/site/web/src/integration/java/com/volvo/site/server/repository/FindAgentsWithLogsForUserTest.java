package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.KeyLog;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.testutil.components.AgentTestUtils;
import com.volvo.site.server.testutil.components.KeyLogTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class FindAgentsWithLogsForUserTest extends AbstractFullSpringTest {
    private AgentTestUtils.TestAgentStuff agentWithLogs;
    private AgentTestUtils.TestAgentStuff agentWithoutLogs;
    private KeyLog keyLog;

    @Autowired
    AgentTestUtils agentTestUtils;

    @Autowired
    AgentJpaRepository agentJpaRepository;

    @Autowired
    KeyLogJpaRepository keyLogJpaRepository;

    @Autowired
    KeyLogTestUtils keyLogTestUtils;

    @BeforeClass
    @Rollback(false)
    public void setup() {
        agentWithLogs = agentTestUtils.createNormalAgent("agent-with-logs");
        agentWithoutLogs = agentTestUtils.createNormalAgent("agent-without-logs");
        keyLog = keyLogJpaRepository.save(keyLogTestUtils.generateKeyLog(agentWithLogs.agent.getId()));
    }

    @Test
    @Rollback(false)
    public void test() {
        List<Agent> agents = agentJpaRepository.findAgentsWithLogsForUser(agentWithLogs.user.getId());
        assertEquals(agents.size(), 1);

        Agent agent = agents.get(0);
        assertEquals(agent.getId(), agentWithLogs.agent.getId());
        assertEquals(agent.getStatus(), Agent.AgentStatus.AGENT_CREATED);
        assertEquals(agent.getName(), "agent-with-logs");
    }

    @AfterClass
    @Rollback(false)
    public void tearDown() {
        keyLogJpaRepository.delete(keyLog.getId());
        agentTestUtils.deleteTestAgentStuffFromDb(agentWithLogs, agentWithoutLogs);
    }
}
