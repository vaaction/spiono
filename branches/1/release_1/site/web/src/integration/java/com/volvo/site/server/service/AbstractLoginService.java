package com.volvo.site.server.service;

import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class AbstractLoginService extends AbstractFullSpringTest {

    @Autowired
    protected LoginService loginService;

    @Test
    public void autowiringTest() {
        assertNotNull(loginService);
    }
}
