package com.volvo.site.server.service;

import com.volvo.site.server.dto.HeartbeatResult;
import com.volvo.site.server.model.ModelConstants;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.impl.util.AgentUtilService;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.testutil.components.AgentTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.testng.annotations.Test;

import static com.volvo.site.server.service.UserService.FILTER_AGENT_STATUS_OFF;
import static com.volvo.site.server.service.UserService.SEARCHING_NAME_ANY;
import static com.volvo.site.server.testutil.RepositoryTestUtils.createRandomActiveUser;
import static com.volvo.site.server.testutil.RepositoryTestUtils.random255Bytes;
import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.testng.Assert.*;

@Test(groups = "SingleClientInstaller")
public class AgentServiceTest extends AbstractFullSpringTest {

    @Autowired
    AgentTestUtils agentTestUtils;

    @Autowired
    AgentJpaRepository agentJpaRepository;

    @Autowired
    AgentService agentService;

    @Autowired
    UserJpaRepository userJpaRepository;

    @Test
    public void deleteAgentAndCreateNewOneWhenAgentIsCreatedTest() {
        AgentTestUtils.TestAgentStuff agentStuff = agentTestUtils.createNormalAgent();
        Agent oldAgent = agentStuff.agent;
        User user = agentStuff.user;

        Agent newAgent = agentService.deleteAgentAndCreateNewOne(user.getId(), oldAgent.getId());

        assertTrue(agentJpaRepository.exists(newAgent.getId()));
        assertFalse(agentJpaRepository.exists(oldAgent.getId()));
    }

    @Test
    public void deleteAgentAndCreateNewOneWhenAgentIsDownloadedTest() {
        AgentTestUtils.TestAgentStuff agentStuff = agentTestUtils.createNormalAgent();
        Agent oldAgent = agentStuff.agent;
        User user = agentStuff.user;

        agentService.downloadAgent(oldAgent.getId(), oldAgent.getClientKey());
        Agent newAgent = agentService.deleteAgentAndCreateNewOne(user.getId(), oldAgent.getId());

        assertTrue(agentJpaRepository.exists(newAgent.getId()));
        assertFalse(agentJpaRepository.exists(oldAgent.getId()));
    }

    @Test
    public void deleteAgentAndCreateNewOneWhenAgentIsRegisteredTest() {
        AgentTestUtils.TestAgentStuff agentStuff = agentTestUtils.createNormalAgent();
        Agent oldAgent = agentStuff.agent;
        User user = agentStuff.user;

        agentService.downloadAgent(oldAgent.getId(), oldAgent.getClientKey());
        agentService.registerAgent(oldAgent.getClientKey());
        Agent newAgent = agentService.deleteAgentAndCreateNewOne(user.getId(), oldAgent.getId());

        assertTrue(agentJpaRepository.exists(newAgent.getId()));
        assertFalse(agentJpaRepository.exists(oldAgent.getId()));
    }

    @Test
    public void deleteAgentTest(){
        //Given
        Agent agent = agentTestUtils.createNormalAgent().agent;
        agent.setOnPause(true);
        agentJpaRepository.save(agent);

        //When
        agentService.deleteAgent(agent.getId());
        agent = agentJpaRepository.findOne(agent.getId());

        //Then
        assertEquals(agent.getStatus(), Agent.AgentStatus.AGENT_DELETED);
        assertEquals(agent.isOnPause(), false);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldCreateAgentThrowEntityNotFoundWhenUserNotFoundTest() {
        agentService.createAgent(0);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldCreateAgentThrowEntityNotFoundClientInstallerNotFoundTest() {
        User user = userJpaRepository.save(createRandomActiveUser());
        agentService.createAgent(user.getId());
    }

    @Test
    public void downloadAgentPositiveTest() {
        AgentTestUtils.TestAgentStuff agentStuff = agentTestUtils.createNormalAgent();
        Agent agent = agentStuff.agent;

        assertFalse(agentStuff.clientInstaller.isDownloaded());
        byte[] actualData = agentService.downloadAgent(agent.getId(), agent.getClientKey());

        assertEquals(actualData, random255Bytes);
        assertEquals(agent.getStatus(), Agent.AgentStatus.AGENT_DOWNLOADED);
        assertTrue(agentStuff.clientInstaller.isDownloaded());
    }

    @Test
    public void registerAgentTest() {
        AgentTestUtils.TestAgentStuff agentStuff = agentTestUtils.createNormalAgent();
        Agent agent = agentStuff.agent;

        agentService.downloadAgent(agent.getId(), agent.getClientKey());
        agentService.registerAgent(agent.getClientKey());

        assertEquals(agent.getCertificate().length(), ModelConstants.agentCertificateLen);
        assertEquals(agent.getAgentInstaller(), agentStuff.agentInstaller);
        assertEquals(agent.getStatus(), Agent.AgentStatus.AGENT_REGISTERED);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldDownloadAgentThrowEntityNotFoundWhenAgentNotFound() {
        agentService.downloadAgent(0, "someKey");
    }

    @Test
    public void shouldIsAgentRegisteredReturnTrueIfAgentFound() {
        // Given
        Agent agent = agentTestUtils.createNormalAgent().agent;

        // When
        agent.setStatus(Agent.AgentStatus.AGENT_REGISTERED);
        agentJpaRepository.save(agent);

        // Then
        assertTrue(agentService.isAgentRegistered(agent.getId()));
    }

    @Test
    public void shouldIsAgentRegisteredReturnFalseIfAgentNotFound() {
        Agent agent = agentTestUtils.createNormalAgent().agent;
        assertFalse(agentService.isAgentRegistered(agent.getId()));
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldRegisterAgentThrowEntityNotFoundWhenAgentNotFound() {
        agentService.registerAgent(randomAgentClientKey());
    }

    @Test
    public void canAgentLogPositiveTest() {
        Agent agent = agentTestUtils.createNormalAgentWithCertificate().agent;
        agent.setStatus(Agent.AgentStatus.AGENT_REGISTERED);
        agentJpaRepository.save(agent);

        assertTrue(AgentUtilService.canAgentLog(agent));
    }

    @Test
    public void shouldCanAgentLogReturnFalseIfAgentIsOnPause() {
        Agent agent = agentTestUtils.createNormalAgentWithCertificate().agent;
        agent.setOnPause(true);
        agentJpaRepository.save(agent);

        assertFalse(AgentUtilService.canAgentLog(agent));
    }

    @Test
    public void shouldCanAgentLogReturnFalseIfAgentStatusIsNotRegistered() {
        Agent agent = agentTestUtils.createNormalAgentWithCertificate().agent;
        agent.setStatus(Agent.AgentStatus.AGENT_DOWNLOADED);
        agentJpaRepository.save(agent);

        assertFalse(AgentUtilService.canAgentLog(agent));
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldThrowEntityNotFoundSetAgentNameIfAgentNotFound() {
        agentService.setAgentName(0, "agent-name");
    }

    @Test
    public void setAgentNameTest() {
        Agent agent = agentTestUtils.createNormalAgent().agent;
        assertNull(agent.getName());

        agentService.setAgentName(agent.getId(), "new-name");
        assertEquals(agentJpaRepository.findOne(agent.getId()).getName(), "new-name");
    }

    @Test
    public void shouldMakeHeartBeatReturnUninstallWhenAgentNotFound() {
        assertEquals(agentService.makeHeartBeat(randomAgentCertificate()), HeartbeatResult.UNINSTALL);
    }

    @Test
    public void shouldMakeHeartBeatReturnUninstallWhenAgentIsDeleted() {
        Agent agent = agentTestUtils.createNormalAgentWithCertificate().agent;
        agent.setStatus(Agent.AgentStatus.AGENT_DELETED);
        agentJpaRepository.save(agent);

        assertEquals(agentService.makeHeartBeat(agent.getCertificate()), HeartbeatResult.UNINSTALL);
    }

    @Test
    public void shouldMakeHeartBeatReturnPauseWhenAgentOnPause() {
        Agent agent = agentTestUtils.createNormalAgentWithCertificate().agent;
        agent.setOnPause(true);
        agentJpaRepository.save(agent);

        assertEquals(agentService.makeHeartBeat(agent.getCertificate()), HeartbeatResult.PAUSE);
    }

    @Test
    public void makeHeartBeatTest() {
        Agent agent = agentTestUtils.createNormalAgentWithCertificate().agent;
        agentJpaRepository.save(agent);

        assertNull(agent.getHearbeatTime());
        assertFalse(agent.hasHeartBeatsAtTheLastMinute());

        assertEquals(agentService.makeHeartBeat(agent.getCertificate()), HeartbeatResult.OK);

        assertNotNull(agent.getHearbeatTime());
        assertTrue(agent.hasHeartBeatsAtTheLastMinute());
    }

    @Test
    public void getAgentsEmptyListTest() {
        Page<Agent> result = agentService.getAgents(1, SEARCHING_NAME_ANY,  FILTER_AGENT_STATUS_OFF, 100, 10);
        assertNotNull(result);
        assertFalse(result.hasContent());
        assertFalse(result.iterator().hasNext());
        assertTrue(result.getContent().isEmpty());
        assertEquals(result.getNumberOfElements(), 0);
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void setAgentOnPauseTestWhenAgentIsNotRegistered() {
        Agent agent = agentTestUtils.createNormalAgent().agent;
        assertFalse(agent.isOnPause());

        agentService.setAgentOnPause(agent.getId(), true);

        assertTrue(agent.isOnPause());
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void setAgentOnPauseTestWhenAgentIsNotFound() {
        agentService.setAgentOnPause(0L, true);
    }

    @Test
    public void setAgentOnPauseTest() {
        Agent agent = agentTestUtils.createNormalAgentWithCertificate().agent;
        agent.setStatus(Agent.AgentStatus.AGENT_REGISTERED);

        agentService.setAgentOnPause(agent.getId(), true);
        assertTrue(agent.isOnPause());

        agentService.setAgentOnPause(agent.getId(), false);
        assertFalse(agent.isOnPause());
    }

    @Test
    public void createAgentWithNameTest() {
        Agent agent = agentTestUtils.createNormalAgent("Hello").agent;
        assertEquals(agent.getName(), "Hello");
    }

    @Test
    public void createAgentPositiveTest() {
        // When
        AgentTestUtils.TestAgentStuff agentStuff = agentTestUtils.createNormalAgent();
        Agent agent = agentStuff.agent;
        User user = agentStuff.user;
        ClientInstaller clientInstaller = agentStuff.clientInstaller;

        // Then
        assertNotNull(agent.getId());
        assertEquals(agent.getUser(), user);
        assertEquals(agent.getClientInstaller(), clientInstaller);
        assertNull(agent.getAgentInstaller());
        assertEquals(ModelConstants.clientInstallerKeyLen, agent.getClientKey().length());
        assertEquals(agent.getStatus(), Agent.AgentStatus.AGENT_CREATED);
        assertNull(agent.getName());
    }

    private static String randomAgentClientKey() {
        return randomAlphanumeric(ModelConstants.clientInstallerKeyLen);
    }

    private static String randomAgentCertificate() {
        return randomAlphanumeric(ModelConstants.agentCertificateLen);
    }
}
