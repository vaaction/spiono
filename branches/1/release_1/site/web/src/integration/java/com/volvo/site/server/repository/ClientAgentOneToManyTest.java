package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.Test;

import static com.volvo.site.server.testutil.RepositoryTestUtils.random255Bytes;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

@Test(groups = "SaveAgentLogsTestConflict")
public class ClientAgentOneToManyTest extends AbstractFullSpringTest {
    @Autowired
    AgentInstallerJpaRepository agentRepository;

    @Autowired
    ClientInstallerJpaRepository clientRepository;

    private ClientInstaller clientInstaller;

    @Test
    @Rollback(false)
    public void cascadeCreateTest() {
        clientInstaller = clientRepository.save(ClientInstaller.of(random255Bytes));
        clientInstaller.addAgentInstaller(agentRepository.save(AgentInstaller.of(random255Bytes, clientInstaller)));
        clientInstaller.addAgentInstaller(agentRepository.save(AgentInstaller.of(random255Bytes, clientInstaller)));
    }

    @Test(dependsOnMethods = "cascadeCreateTest")
    @Rollback(false)
    public void cascadeReadTest() {
        checkRead(random255Bytes);
    }

    @Test(dependsOnMethods = "cascadeReadTest")
    @Rollback(false)
    public void cascadeUpdateTest() {
        byte[] newData = new byte[] {1, 2, 3};

        ClientInstaller clientInstallerNew = clientRepository.findOne(clientInstaller.getId());
        clientInstallerNew.setData(newData);
        clientRepository.save(clientInstallerNew);

        assertFalse(clientInstallerNew.getAgentInstallers().isEmpty());
        for (AgentInstaller agent: clientInstallerNew.getAgentInstallers()) {
            AgentInstaller agentNew = agentRepository.findOne(agent.getId());
            agentNew.setData(newData);
            agentRepository.save(agentNew);
        }
    }

    @Test(dependsOnMethods = "cascadeUpdateTest")
    @Rollback(false)
    public void cascadeReadAfterUpdateTest() {
        checkRead(new byte[] {1, 2, 3});
    }

    @Test(dependsOnMethods = "cascadeReadAfterUpdateTest")
    @Rollback(false)
    public void cascadeDeleteTest() {
        clientRepository.delete(clientInstaller.getId());
    }

    @Test(dependsOnMethods = "cascadeDeleteTest")
    @Rollback(false)
    public void checkCascadeDeletedTest() {
        assertFalse(clientRepository.exists(clientInstaller.getId()));
        for (AgentInstaller agent: clientInstaller.getAgentInstallers()) {
            assertFalse(agentRepository.exists(agent.getId()));
        }
    }

    private void checkRead(byte[] data) {
        ClientInstaller loadedClientInstaller = clientRepository.findOne(clientInstaller.getId());
        assertFalse(loadedClientInstaller.getAgentInstallers().isEmpty());
        assertEquals(loadedClientInstaller.getData(), data);

        for (AgentInstaller agent: loadedClientInstaller.getAgentInstallers()) {
            assertEquals(agentRepository.findOne(agent.getId()).getData(), data);
        }
    }
}
