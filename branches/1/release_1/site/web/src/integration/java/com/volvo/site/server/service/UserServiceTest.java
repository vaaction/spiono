package com.volvo.site.server.service;

import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.model.entity.UserSettings;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.repository.UserSettingsJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.testutil.components.AgentTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.testng.annotations.Test;

import static com.volvo.platform.java.testing.TestUtils.randomPwd;
import static com.volvo.site.server.testutil.RepositoryTestUtils.createActiveUserWithPassword;
import static com.volvo.site.server.testutil.RepositoryTestUtils.createInactiveRandomUser;
import static org.testng.Assert.*;

@Test(groups = "SingleClientInstaller")
public class UserServiceTest extends AbstractFullSpringTest {

    @Autowired
    AgentTestUtils agentTestUtils;

    @Autowired
    UserService userService;

    @Autowired
    UserJpaRepository userJpaRepository;

    @Autowired
    private UserSettingsJpaRepository userSettingsJpaRepository;

    @Test
    public void changePasswordPositiveTest() {
        // Given
        String oldPassword = randomPwd();
        User user = userJpaRepository.save(createActiveUserWithPassword(oldPassword));

        // When
        String newPassword = randomPwd();
        boolean isChanged = userService.changePassword(user.getId(), oldPassword, newPassword);

        // Then
        assertTrue(isChanged);
        assertTrue(BCrypt.checkpw(newPassword, user.getPassword()));
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void changePasswordOfNonExistingUserTest() {
        userService.changePassword(-1L, randomPwd(), randomPwd());
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void changePasswordForInactiveUserTest() {
        User user = userJpaRepository.save(createInactiveRandomUser());
        userService.changePassword(user.getId(), randomPwd(), randomPwd());
    }

    @Test
    public void changePasswordWithWrongOldPasswordTest() {
        // Given
        String oldPassword = randomPwd();
        User user = userJpaRepository.save(createActiveUserWithPassword(oldPassword));

        // When
        String newPassword = randomPwd();
        boolean isChanged = userService.changePassword(user.getId(), "invalid" + oldPassword, newPassword);

        // Then
        assertFalse(isChanged);
        assertTrue(BCrypt.checkpw(oldPassword, user.getPassword()));
    }

    @Test
    public void shouldUserServiceAlwaysReturnUserSettings() {
        User user = userJpaRepository.save(createInactiveRandomUser());
        UserSettings userSettings = userService.getUserSettings(user.getId());

        assertEquals(Long.valueOf(user.getId()), Long.valueOf(userSettings.getUserId()));
    }

    @Test
    public void updateStatisticsViewStartupInfoTest(){
        User user = userJpaRepository.save(createInactiveRandomUser());
        UserSettings userSettings = userService.getUserSettings(user.getId());

        assertNull(userSettings.getLastStatAgentId());

        userSettings = userService.updateLastStatAgentId(user.getId(), 200L);
        assertEquals(Long.valueOf(200), Long.valueOf(userSettings.getLastStatAgentId()));
    }

    @Test
    public void lastLogsSearchAgentIdTest(){
        User user = userJpaRepository.save(createInactiveRandomUser());
        UserSettings userSettings = userService.getUserSettings(user.getId());

        assertNull(userSettings.getLastLogsSearchAgentId());

        userSettings = userService.updateLastLogsSearchAgentId(user.getId(), 200L);
        assertEquals(Long.valueOf(200), Long.valueOf(userSettings.getLastLogsSearchAgentId()));
    }

    @Test
    public void lastSearchParamsTest(){
        User user = userJpaRepository.save(createInactiveRandomUser());
        UserSettings userSettings = userService.getUserSettings(user.getId());

        assertNull(userSettings.getLastAgentsSearchPhrase());
        assertNull(userSettings.getLastAgentsSearchStatus());

        userSettings = userService.updateLastLogsSearchSettings(user.getId(), "phraze", "status");
        assertEquals(userSettings.getLastAgentsSearchPhrase(), "phraze");
        assertEquals(userSettings.getLastAgentsSearchStatus(), "status");
    }
}
