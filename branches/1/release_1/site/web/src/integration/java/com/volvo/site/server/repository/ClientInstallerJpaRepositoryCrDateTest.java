package com.volvo.site.server.repository;


import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.repository.testing.HasCrDateTestBase;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

import static com.volvo.site.server.testutil.RepositoryTestUtils.random255Bytes;

public class ClientInstallerJpaRepositoryCrDateTest extends HasCrDateTestBase<ClientInstaller, ClientInstallerJpaRepository> {

    @Autowired
    @Getter
    ClientInstallerJpaRepository repository;

    public ClientInstallerJpaRepositoryCrDateTest() {
        super(ClientInstaller.class);
    }

    @Override
    protected ClientInstaller createModel() {
        ClientInstaller installer = super.createModel();
        installer.setData(random255Bytes);
        return installer;
    }
}
