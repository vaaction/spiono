<%@ page import="static com.volvo.site.server.util.ControllersPaths.appendSlash" %>
<%@ page import="static com.volvo.site.server.util.ControllersPaths.IndexController.REGISTER" %>
<%@ page import="static com.volvo.site.server.util.ControllersPaths.IndexController.LOGIN" %>
<%@ page import="com.volvo.site.server.util.ControllersPaths" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<h2 id="header-2">
    Registration confirmation failed
    <small>
        <a href="${pageContext.request.contextPath}<%= appendSlash(ControllersPaths.IndexController.LOGIN) %>">sign in</a>
        or
        <a href="${pageContext.request.contextPath}<%= appendSlash(ControllersPaths.IndexController.REGISTER) %>">register</a>
    </small>
</h2>


<div style="width: 550px;" class="alert alert-error">
    <strong>Can not the registration in cause of bad link</strong><br/><br/>
    Please make sure that the link correctly copied from the email that we sent to you after registration
    <ul>
        <li>
            <a class="blue-underline-link" href="${pageContext.request.contextPath}<%= REGISTER + "/"%>">Try to register again</a>
        </li>
        <li>
            <a class="blue-underline-link" href="${pageContext.request.contextPath}<%= LOGIN + "/"%>">Login</a>
        </li>
        <li>
            <a class="blue-underline-link" href="${pageContext.request.contextPath}/">Home page</a>
        </li>
    </ul>
</div>

