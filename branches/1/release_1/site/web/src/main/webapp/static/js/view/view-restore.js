var app = app || {};

$(function($) {
    'use strict';

    app.RestoreView = Backbone.View.extend({
        el: '#restoreForm',
        emailField: $('#email'),
        captchaField: $("#captcha"),

        initialize: function() {
            this.initFormValidation();
            app.Utils.initEmailAutoComplete(this.emailField);

            this.emailField.val(app.Utils.cookieWithoutQuotes("email"));
            if (this.emailField.val().length === 0) {
                this.emailField.focus();
            }
            else {
                this.captchaField.focus();
            }
        },

        initFormValidation: function() {
            $(this.el).validate({
                    rules: {
                        email: {
                            required: true,
                            minlength:4,
                            maxlength:50
                        },
                        captcha: {
                            required: true,
                            minlength:4,
                            maxlength:4
                        }
                    },
                    errorElement: 'span',
                    errorPlacement: function(error, element) {
                        error.insertAfter(element)
                        error.addClass('help-block');
                    }
                });
        }
    });
});