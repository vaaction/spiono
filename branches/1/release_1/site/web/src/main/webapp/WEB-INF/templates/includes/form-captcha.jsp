<%@ page import="com.volvo.site.server.util.Constants" %>
<%@ page import="static com.volvo.platform.java.ServletUtils.randomNoCache" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
   final String urlParam = "form.captcha.url";
%>

<spring:bind path="captcha">
    <c:set var="captcha_control_group_class" value="control-group" scope="request"/>
    <c:if test="${not empty status.errorMessages}">
        <c:set var="captcha_control_group_class" value="control-group error" scope="request" />
    </c:if>

    <div class="${captcha_control_group_class}" data-captcha-element>
        <input id="captchaUrlHidden" type="hidden" value='<%=request.getParameter(urlParam)%>' />
        <form:label path="captcha" cssClass="control-label"><spring:message code="register.are_you_robot" text="Are you robot? Type the text from image"/></form:label>
        <img id="captcha-image" src="<%=request.getParameter(urlParam) + randomNoCache()%>" alt="captcha"/><br>
        <small>
            <spring:message code="register.captcha.can_not_see_image" text="Can not see this image?"/>
            <a id="captcha-redraw-image-btn" href="javascript:void(0)"><spring:message code="register.captcha.redraw" text="Redraw"/></a>
        </small>
        <div class="controls">
            <form:input path="captcha" autocomplete="off" cssClass="required" maxlength="<%=String.valueOf(Constants.captchaLength)%>"/>
            <form:errors path="captcha" cssClass="help-block"/>
        </div>
    </div>
</spring:bind>