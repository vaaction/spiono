<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.volvo.site.server.model.ModelConstants" %>

<spring:bind path="email">
    <c:set var="email_control_group_class" value="control-group" scope="request"/>
    <c:if test="${not empty status.errorMessages}">
        <c:set var="email_control_group_class" value="control-group error" scope="request" />
    </c:if>

    <div class="${email_control_group_class}">
        <form:label path="email" cssClass="control-label"><spring:message code="label.form_field_email.email" text="Email:"/></form:label>
        <div class="controls">
            <form:input path="email" cssClass="required email" autocomplete="off"
                        maxlength="<%=String.valueOf(ModelConstants.emailMaxLength)%>"/>
            <form:errors path="email" cssClass="help-block" />
        </div>
    </div>
</spring:bind>