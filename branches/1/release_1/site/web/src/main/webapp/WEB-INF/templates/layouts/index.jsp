<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<h1 class="table-bordered" style="text-align: center; border-top: #002a80; border-right-width: 0; border-radius: 0;">
    <spring:message code="mainWelcomeOn" text="Welcome on Spiono"/><br>
    <small>
        <spring:message code="mainText1" text="Find out what happens on any computers you care about free"/><br>
        <spring:message code="mainText2" text="absolutely free by following the following 3 steps"/><br>
    </small>
</h1>

<div class="btn-block" style="text-align: center; color: #FFFFFF">
    <div class="btn-warning" style="height: 80px; padding: 5px; font-size: 20px;
        line-height: 30px; margin-bottom: 20px; border-radius: 6px;
        text-shadow: -1px -1px rgba(0, 0, 0, 0.25);">
        <span style="margin-top: 5px; float: left;font: italic small-caps bold 12px/12px verdana;font-size:25px;">1</span>
        <div style="margin-top: 20px">
            <a href="/register/"><spring:message code="mainRegisterText1" text="Register"/></a>
            <spring:message code="mainRegisterText2" text="now and"/>
            <a href="/user-cabinet/"><spring:message code="mainRegisterText3" text="log in"/></a>
            <spring:message code="mainRegisterText4" text="and log in to your personal cabinet."/>
        </div>
   </div>

   <div class="btn-info" style="height: 80px;padding: 5px; font-size: 20px;
        line-height: 30px; margin-bottom: 20px; border-radius: 6px;
        text-shadow: -1px -1px rgba(0, 0, 0, 0.25);">
        <span style="margin-top: 5px; float: left;font: italic small-caps bold 12px/12px verdana;font-size:25px;">2</span>
        <div style="margin-top: 20px">
            <spring:message code="mainDownloadInstallText1" text="Download your first agent and install it on a computer you care about."/>
        </div>
   </div>

   <div class="btn-danger" style="height: 80px; padding: 5px; font-size: 20px;
       line-height: 30px; margin-bottom: 20px; border-radius: 6px;
       text-shadow: -1px -1px rgba(0, 0, 0, 0.25);">
       <span style="margin-top: 5px; float: left;font: italic small-caps bold 12px/12px verdana;font-size:25px;">3</span>
       <div style="margin-top: 20px">
           <spring:message code="mainLoginText1" text="At any time you can Log in into your personal cabinet"/>
           <a href="/user-cabinet/"><spring:message code="mainLoginText2" text="log in"/></a>
           <spring:message code="mainLoginText3" text="and find out what happened on a interested computer"/>
       </div>
   </div>

</div>
