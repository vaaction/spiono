<%@ page import="com.volvo.site.server.util.ControllersPaths" %>
<%@ page import="org.springframework.web.servlet.support.RequestContextUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<header class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <c:set var="locale"  value="<%=RequestContextUtils.getLocaleResolver(request).resolveLocale(request).getLanguage()%>"/>
            <ul class="nav pull-left">
                <li>
                    <a href="${pageContext.request.contextPath}/">Spiono</a>
                </li>
                <sec:authorize access="hasRole('ROLE_USER')">
                    <li>
                        <a href="${pageContext.request.contextPath}<%= ControllersPaths.IndexController.USERCABINET + "/"  %>"><spring:message code="userCabinet" text="User cabinet"/></a>
                    </li>
                </sec:authorize>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <li>
                        <a href="#">Admin's Cabinet</a>
                    </li>
                </sec:authorize>
                <sec:authorize access="hasAnyRole('ROLE_USER','ROLE_ADMIN')">
                    <li>
                        <a href="${pageContext.request.contextPath}/j_spring_security_logout"><spring:message code="logout" text="Logout"/></a>
                    </li>
                </sec:authorize>
                <sec:authorize access="!hasAnyRole('ROLE_USER','ROLE_ADMIN')">
                    <li>
                        <a href="${pageContext.request.contextPath}<%= ControllersPaths.IndexController.LOGIN + "/"  %>"><spring:message code="label.signin_short" text="Log in"/></a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}<%= ControllersPaths.IndexController.REGISTER + "/"  %>"><spring:message code="label.signup_short" text="Sign up"/></a>
                    </li>
                </sec:authorize>
                <li class="divider-vertical"></li>
            </ul>
            <ul class="nav pull-right">
                <li class="divider-vertical"></li>
                <li>
                    <c:if test="${locale == 'en'}">
                        <a href="<c:url value='?locale=ru'/>">Русский</a>
                    </c:if>
                    <c:if test="${locale == 'ru'}">
                        <a href="<c:url value='?locale=en'/>">English</a>
                    </c:if>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</header>