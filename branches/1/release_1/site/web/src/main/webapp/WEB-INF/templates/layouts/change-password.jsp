<%@ page import="com.volvo.site.server.form.ChangePasswordForm" %>
<%@ page import="static com.volvo.site.server.util.ControllersPaths.appendSlash" %>
<%@ page import="static com.volvo.site.server.util.ControllersPaths.IndexController.LOGIN" %>
<%@ page import="static com.volvo.site.server.util.ControllersPaths.IndexController.RESTORE" %>
<%@ page import="com.volvo.site.server.util.ControllersPaths" %>
<%@ page import="static com.volvo.platform.spring.MvcUtil.getUncapitalizedSimpleClassName" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="../includes/localized-jquery-validate.jsp"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/view/view-changepwd.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/app/app-changepwd.js"></script>
<jsp:include page="../includes/localized-messages.jsp"/>

<h1>
    <spring:message code="change_password.header" text="Change password"/>
    <small>
        <a href="${pageContext.request.contextPath}<%= appendSlash(ControllersPaths.IndexController.LOGIN) %>"><spring:message code="label.signin" text="Sign in"/></a>
        <spring:message code="justrestored.or" text="or"/>
        <a href="${pageContext.request.contextPath}<%= appendSlash(ControllersPaths.IndexController.REGISTER) %>"><spring:message code="label.signup" text="Sign up"/></a>
    </small>
</h1>

<c:if test="${not empty entityNotFoundException}">
    <div style="width: 550px;" class="alert alert-error">
        <strong><spring:message code="change_password.text1" text="Bad link for changing the password"/></strong><br/><br/>
        <spring:message code="change_password.text2" text="Please make sure the link was correctly copied from the email"/>
        <spring:message code="change_password.text3" text="which we sent you after the password retrieval request"/>
        <ul>
            <li>
                <a class="blue-underline-link" href="${pageContext.request.contextPath}<%= RESTORE + "/"%>"><spring:message code="change_password.text4" text="Try to restore password again"/></a>
            </li>
            <li>
                <a class="blue-underline-link" href="${pageContext.request.contextPath}<%= LOGIN + "/"%>"><spring:message code="label.signin" text="Sign in"/></a>
            </li>
        </ul>
    </div>
</c:if>

<c:if test="${not empty passwordForm}">

    <p><spring:message code="change_password.text5" text="Please, provide your new password and login to system"/></p>

    <form:form
        cssStyle="width: 400px;"
        method="POST"
        action="<%= appendSlash(ControllersPaths.IndexController.CHANGE_PASSWORD)  %>"
        commandName="<%= getUncapitalizedSimpleClassName(ChangePasswordForm.class) %>">

        <fieldset>
            <jsp:include page="/WEB-INF/templates/includes/form-field-password.jsp" />
            <jsp:include page="/WEB-INF/templates/includes/form-field-confirm-password.jsp" />
            <input type="hidden" id="resCode" name="resCode" class="required" value="${resCode}" />
        </fieldset>

        <button type="submit" class="btn btn-primary"><spring:message code="change_password.text6" text="Change password"/></button> <spring:message code="change_password.text7" text="and sign in"/>
    </form:form>
</c:if>