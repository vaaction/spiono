<%@ page import="com.volvo.site.server.form.LoginForm" %>
<%@ page import="static com.volvo.site.server.util.ControllersPaths.appendSlash" %>
<%@ page import="com.volvo.site.server.util.ControllersPaths" %>
<%@ page import="static com.volvo.platform.spring.MvcUtil.getUncapitalizedSimpleClassName" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<jsp:include page="../includes/localized-jquery-validate.jsp"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/lib/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/view/view-login.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/app/app-login.js"></script>

<h1>
    <spring:message code="login.caption.signin" text="Sign in"/>
    <small>
        <spring:message code="login.caption.noaccount" text="No account yet?"/>
        <a href="${pageContext.request.contextPath}<%= appendSlash(ControllersPaths.IndexController.REGISTER) %>"><spring:message code="login.caption.register" text="Register for FREE!"/></a>
    </small>
</h1>

<form:form
        cssStyle="width: 400px;"
        method="POST"
        action="<%= appendSlash(ControllersPaths.IndexController.LOGIN)  %>"
        commandName="<%= getUncapitalizedSimpleClassName(LoginForm.class) %>">

    <c:set var="user_already_registered">
        <spring:message code="userAlreadyRegistered" text="User with such email already registered. Try to login."/>
    </c:set>
    <c:if test="${not empty confirmRegistration}">
        <jsp:include page="/WEB-INF/templates/includes/form-error.jsp">
            <jsp:param name="form.error.text" value="${user_already_registered}" />
        </jsp:include>
    </c:if>

    <c:set var="confirm_before_login">
        <spring:message code="confirmBeforeLogin"
                        text="You have to confirm registration before log in. We sent instructions to your email."/>
    </c:set>
    <c:if test="${not empty haveToConfirm}">
        <jsp:include page="/WEB-INF/templates/includes/form-error.jsp">
            <jsp:param name="form.error.text" value="${confirm_before_login}" />
        </jsp:include>
    </c:if>

    <c:if test="${not empty restorePasswordMessageSent}">
        <div class="alert alert-block">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong><spring:message code="login.looks_like" text="Looks like you forgot your password"/></strong></br>
            <spring:message code="login.do_not.worry" text="Do not worry - we have sent instructions for resetting your password to your email"/>
        </div>
    </c:if>

    <fieldset>
        <jsp:include page="/WEB-INF/templates/includes/form-field-email.jsp" />
        <jsp:include page="/WEB-INF/templates/includes/form-field-password.jsp" />
        <label class="checkbox">
            <input type="checkbox" name="_spring_security_remember_me" id="_spring_security_remember_me"><spring:message code="login.remember_me" text="Remember Me"/>
        </label>
    </fieldset>

    <c:set var="invalid_em_or_pass"><spring:message code="invalidUsernameOrPassword" text="Invalid username or password"/></c:set>
    <c:if test="${not empty loginError}">
        <jsp:include page="/WEB-INF/templates/includes/form-error.jsp">
            <jsp:param name="form.error.text" value="${invalid_em_or_pass}"/>
        </jsp:include>
    </c:if>

    <button type="submit" class="btn btn-primary"><spring:message code="label.signin" text="Sign in"/></button>
    <a href="${pageContext.request.contextPath}/restore/"><spring:message code="login.are_forgot.password" text="Forgot your password?"/></a>
</form:form>