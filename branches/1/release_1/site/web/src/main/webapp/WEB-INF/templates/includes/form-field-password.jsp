<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:bind path="password">
    <c:set var="password_control_group_class" value="control-group" scope="request"/>
    <c:if test="${not empty status.errorMessages}">
        <c:set var="password_control_group_class" value="control-group error" scope="request" />
    </c:if>

    <div class="${password_control_group_class}">
        <form:label path="password" cssClass="control-label"><spring:message code="label.form_field_email.password" text="Password:"/></form:label>
        <div class="controls">
            <form:password path="password" autocomplete="off" cssClass="required" maxlength="30" />
            <form:errors path="password" cssClass="help-block" />
        </div>
    </div>
</spring:bind>



