<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:bind path="confirmPassword">
    <c:set var="confirm_password_control_group_class" value="control-group" scope="request"/>
    <c:if test="${not empty status.errorMessages}">
        <c:set var="confirm_password_control_group_class" value="control-group error" scope="request" />
    </c:if>

    <div class="${confirm_password_control_group_class}">
        <form:label path="confirmPassword" cssClass="control-label"><spring:message code="confirm_password" text="Confirm password:"/></form:label>
        <div class="controls">
            <form:password path="confirmPassword" autocomplete="off" cssClass="required" maxlength="30" />
            <form:errors path="confirmPassword" cssClass="help-block" />
        </div>
    </div>
</spring:bind>


