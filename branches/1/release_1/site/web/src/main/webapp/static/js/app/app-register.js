var app = app || {};


$(function($) {
    'use strict';

    new app.RegisterView();
    new app.CaptchaView();
});