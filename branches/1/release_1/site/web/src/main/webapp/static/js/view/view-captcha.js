var app = app || {};

$(function($) {
    'use strict';

    app.CaptchaView = Backbone.View.extend({
        el: 'div[data-captcha-element]',
        captchaImage: $("#captcha-image"),
        captchaUrlHidden: $("#captchaUrlHidden"),

        events: {
            'click #captcha-redraw-image-btn': 'refreshCaptcha'
        },

        refreshCaptcha: function(ev) {
            this.captchaImage.attr("src", this.captchaUrlHidden.val() + "?" + "nocache=" + Math.random());
        }
    });
});