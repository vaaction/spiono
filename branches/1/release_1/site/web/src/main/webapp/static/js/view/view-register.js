var app = app || {};

$(function($) {
    'use strict';

    app.RegisterView = Backbone.View.extend({
        el: '#registerForm',
        emailField: $('#email'),

        initialize: function() {
            this.initFormValidation();
            this.initFocus();
            app.Utils.initEmailAutoComplete(this.emailField);
        },

        initFormValidation: function() {
            $(this.el).validate({
                rules: {
                    password: {
                        minlength: 5
                    },
                    captcha: {
                        required: true,
                        minlength:4,
                        maxlength:4
                    },
                    confirmPassword: {
                        required: true,
                        minlength: 5,
                        equalTo: '#password'
                    }
                },
                messages: {
                    confirmPassword: {
                        equalTo: app.Messages.err_passwords_did_not_match
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.insertAfter(element)
                    error.addClass('help-block');
                }
            });
        },

        initFocus: function() {
            this.emailField.val(app.Utils.cookieWithoutQuotes("email"));
            if (this.emailField.val().length === 0) {
                this.emailField.focus();
            }
            else {
                this.passwordField.focus();
            }
        }
    });
});