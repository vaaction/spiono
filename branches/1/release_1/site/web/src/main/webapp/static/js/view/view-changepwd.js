var app = app || {};

$(function($) {
    'use strict';

    app.ChangePwdView = Backbone.View.extend({
        el: '#changePasswordForm',

        initialize: function() {
            $(this.el).validate({
                rules: {
                    password: {
                        minlength: 5
                    },
                    confirmPassword: {
                        required: true,
                        minlength: 5,
                        equalTo: '#password'
                    }
                },
                messages: {
                    confirmPassword: {
                        equalTo: app.Messages.err_passwords_did_not_match
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.insertAfter(element)
                    error.addClass('help-block');
                }
            });
        }
    });
});