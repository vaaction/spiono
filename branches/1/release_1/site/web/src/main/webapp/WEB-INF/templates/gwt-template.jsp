<%@ page import="static com.volvo.platform.java.ServletUtils.randomNoCache" %>
<%@ page import="com.volvo.site.gwt.usercabinet.shared.constants.SharedGwtConsts" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="gwt:property" content="locale=${locale}">
    <title><tiles:getAsString name="title"/> - Spiono</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/normalize.css"
          type="text/css" />

    <script
        type="text/javascript"
        src="${pageContext.request.contextPath}<tiles:getAsString name="gwtJS" /><%=randomNoCache()%>">
    </script>

</head>
<body>

    <div class="btn btn-primary" id="modalPreloader"
         style="width: 200px; position: absolute; left: 43%; margin-top: 100px;padding: 14px;">
        <spring:message code="global.preloader.text" text=" The application is loading. Please wait a moment..."/>
        <div class="progress progress-striped active">
            <div class="bar" style="width: 100%"></div>
        </div>
    </div>
    <jsp:include page="includes/noscript.jsp"/>

    <sec:authorize access="hasRole('ROLE_USER')">
        <input type="hidden" id="<%= SharedGwtConsts.DIV_WITH_USERNAME%>"
               value="<sec:authentication property="principal.username" />"/>
    </sec:authorize>
</body>
</html>