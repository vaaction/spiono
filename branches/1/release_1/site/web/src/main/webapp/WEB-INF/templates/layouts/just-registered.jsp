<%@ page import="static com.volvo.site.server.util.ControllersPaths.appendSlash" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<h1>
    <spring:message code="just_registered.header" text="Congratulations!"/>
</h1>

<div class="alert alert-info" style="width: 600px;">
    <strong><spring:message code="just_registered.text1" text="Thank you for registration in our system."/></strong></br>
    <spring:message code="just_registered.text2" text="Please, "/> <a href="${email}" target="_blank"  style="text-decoration: underline;"><spring:message code="just_registered.text3" text="check your email at "/> ${email}</a> <spring:message code="just_registered.text4" text="to completed the registration and login to system. "/>
</div>