package com.volvo.site.server.controller.pages;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static com.volvo.site.server.util.ControllersPaths.IndexController.VIEW_INDEX;

@Controller
public class Index {


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return VIEW_INDEX;
	}
}
