package com.volvo.site.server.util;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.access.intercept.DefaultFilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.util.AntPathRequestMatcher;
import org.springframework.security.web.util.RequestMatcher;

import java.util.Collection;
import java.util.LinkedHashMap;

import static com.google.common.collect.Maps.newLinkedHashMap;
import static org.springframework.security.access.SecurityConfig.createListFromCommaDelimitedString;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class SecurityConfigUtil {

    public final static RequestCache NULL_REQUEST_CACHE = new NullRequestCache();

    public static final RequestMatcher ANT_PATH_MATCH_ALL = antPathRequestMatcher("/**");

    public static RequestMatcher antPathRequestMatcher(String pattern) {
        return antPathRequestMatcher(pattern, null);
    }

    public static RequestMatcher antPathRequestMatcher(String pattern, String httpMethod) {
         return new AntPathRequestMatcher(pattern, httpMethod);
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class DefaultSecurityMetadataSourceBuilder {

        private final LinkedHashMap<RequestMatcher, Collection<ConfigAttribute>> requestMap = newLinkedHashMap();

        public static DefaultSecurityMetadataSourceBuilder defaultSecurityMetadataSourceBuilder() {
            return new DefaultSecurityMetadataSourceBuilder();
        }

        public DefaultSecurityMetadataSourceBuilder add(String antPath, String roles) {
            requestMap.put(antPathRequestMatcher(antPath), createListFromCommaDelimitedString(roles));
            return this;
        }

        public FilterInvocationSecurityMetadataSource toMetadataSource() {
            return new DefaultFilterInvocationSecurityMetadataSource(requestMap);
        }
    }
}
