package com.volvo.site.server.service.impl.util;

import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.UserJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.volvo.site.server.service.exception.EntityNotFoundException.throwEntityNotFoundIfNull;

@Service
@Transactional(readOnly = true)
public class UserUtilService {

    private UserJpaRepository userJpaRepository;

    // for CGLIB
    @SuppressWarnings("unused")
    public UserUtilService() {
    }

    @Autowired
    public UserUtilService(UserJpaRepository userJpaRepository) {
        this.userJpaRepository = userJpaRepository;
    }

    public User getActiveUser(long userId) {
        return throwEntityNotFoundIfNull(userJpaRepository.findActiveUserById(userId), User.class, userId);
    }
}
