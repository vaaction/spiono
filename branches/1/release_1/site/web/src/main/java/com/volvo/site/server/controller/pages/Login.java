package com.volvo.site.server.controller.pages;

import com.volvo.site.server.component.Authenticator;
import com.volvo.site.server.form.LoginForm;
import com.volvo.site.server.service.RestorePasswordService;
import com.volvo.site.server.util.ControllersPaths;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;

import static com.volvo.platform.spring.MvcUtil.modelAndView;
import static com.volvo.platform.spring.MvcUtil.modelAndViewWithForm;
import static com.volvo.site.server.util.ControllersPaths.IndexController.LOGIN_VK;
import static com.volvo.site.server.util.ControllersPaths.IndexController.VIEW_LOGIN;

@Controller
@Slf4j
public class Login {

    private static final String CLIENT_ID_PARAM = "client_id";
    private static final String CLIENT_SECRET_PARAM = "client_secret";
    private static final String REDIRECT_URI_PARAM = "redirect_uri";
    private static final String CODE_PARAM = "code";
    private static final String USER_ID_RESPONSE_PARAM = "user_id";
    private static final String EXPIRES_IN_RESPONSE_PARAM = "expires_in";
    private static final String ACCESS_TOKEN_RESPONSE_PARAM = "access_token";

    @Autowired
    private Authenticator authenticator;

    @Autowired
    private RestorePasswordService restorePasswordService;

    @Value("${authorization.vk.client_id}")
    private String vkClientId;

    @Value("${authorization.vk.client_secret}")
    private String vkClientSecret;

    @Value("${authorization.vk.redirect_uri}")
    private String vkRedirectUri;

    @Value("${authorization.vk.access_token_url}")
    private String accessTokenUrl;

    @RequestMapping(value = ControllersPaths.IndexController.LOGIN, method = RequestMethod.GET)
    public ModelAndView login() {
        return modelAndViewWithForm(VIEW_LOGIN, LoginForm.class);
    }

    @RequestMapping(value = ControllersPaths.IndexController.LOGIN, method = RequestMethod.POST)
    public ModelAndView login(@Valid LoginForm loginForm, BindingResult bindingResult,
                              HttpServletRequest request, HttpServletResponse response) throws URISyntaxException, IOException {
        if (bindingResult.hasErrors()) {
            return modelAndView(VIEW_LOGIN, bindingResult);
        }

        return loginAndRedirect(loginForm, bindingResult, request, response);
    }

    @RequestMapping(value = LOGIN_VK, method = RequestMethod.GET)
    @ResponseBody
    public void vkAuthorization(@RequestParam(value = "code") String code){
        HttpURLConnection conn = null;
        try {
            URL url = new URL(getTokenRequestUrl(code));
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            JSONObject jsonObj = new JSONObject(br.readLine());

            long userId = jsonObj.getLong(USER_ID_RESPONSE_PARAM);
            int expiresIn = jsonObj.getInt(EXPIRES_IN_RESPONSE_PARAM);
            String accesToken = jsonObj.getString(ACCESS_TOKEN_RESPONSE_PARAM);
            System.out.println(userId + expiresIn + accesToken);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (conn != null){
                conn.disconnect();
            }
        }
    }

    private String getTokenRequestUrl(String code){
        StringBuilder result = new StringBuilder(accessTokenUrl + "?");
        addUrlParameter(result, CLIENT_ID_PARAM, vkClientId);
        addUrlParameter(result, CLIENT_SECRET_PARAM, vkClientSecret);
        addUrlParameter(result, REDIRECT_URI_PARAM, vkRedirectUri);
        addUrlParameter(result, CODE_PARAM, code);
        return result.toString();
    }

    private void addUrlParameter(StringBuilder sb, String paramName, String paramValue){
        sb.append("&").append(paramName).append("=").append(paramValue);
    }

    private ModelAndView loginAndRedirect(LoginForm loginForm, BindingResult bindingResult,
                                          HttpServletRequest request, HttpServletResponse response) {
        String email = loginForm.getEmail();
        String password = loginForm.getPassword();
        try {
            return authenticator.authenticateAndRedirect(email, password, request, response);
        }
        catch (AuthenticationException exception) {
            return handleAuthenticationException(bindingResult, exception, email, request);
        }
    }

    private ModelAndView handleAuthenticationException(BindingResult bindingResult, AuthenticationException exception,
                                                       String email, HttpServletRequest request) {
        if (exception instanceof com.volvo.site.server.service.exception.AuthenticationException) {
            com.volvo.site.server.service.exception.AuthenticationException e =
                    (com.volvo.site.server.service.exception.AuthenticationException)exception;
            if (Long.valueOf(2).equals(e.errorLoginsCount)) {
                restorePasswordService.requestPasswordReset(email, RequestContextUtils.getLocaleResolver(request).resolveLocale(request));
                return returnLoginWithError(bindingResult, "restorePasswordMessageSent");
            }
            if (e.haveToConfirmRegistration) {
                return returnLoginWithError(bindingResult, "haveToConfirm");
            }
        }
        return returnLoginWithError(bindingResult, "loginError");
    }

    private ModelAndView returnLoginWithError(BindingResult bindingResult, String errKey) {
        return modelAndView(VIEW_LOGIN, bindingResult).addObject(errKey, Boolean.TRUE);
    }
}
