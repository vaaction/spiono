package com.volvo.site.server.config.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AppProperties {
    public static final String InstallerFileUrl = "${winapp.installer.src}";
    public static final String AgentFileUrl = "${winapp.agent.src}";
    public static final String RootUsername="${root.un}";
    public static final String RootPassword="${root.pwd}";
    public static final String RootPasswordUseHash ="${root.pwd.usehash}";
    public static final String SiteName ="${sitename}";
    public static final String HostName ="${hostname}";
}
