package com.volvo.site.server.service;


import com.volvo.site.gwt.usercabinet.shared.dto.GetKeyLogDataDTO;
import com.volvo.site.server.dto.KeyLogDTO;
import com.volvo.site.server.model.entity.KeyLog;

import java.util.List;

public interface LogsService {

    int writeLogs(KeyLogDTO keyLogDTO);

    List<KeyLog> search(GetKeyLogDataDTO filter);
}
