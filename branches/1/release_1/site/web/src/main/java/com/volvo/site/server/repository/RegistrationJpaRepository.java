package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.Registration;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


@Repository
@Transactional(readOnly = true)
public interface RegistrationJpaRepository extends JpaRepositoryWithLongId<Registration> {
    Registration findByUniqueKey(String uniqueKey);

    Registration findByEmailAndStatus(String email, Registration.RegistrationStatus status);

    Registration findByEmail(String email);

    @Query("select count(r.id) from Registration r where r.email=:email and r.status='PENDING'")
    long countPendingByEmail(@Param("email")String email);

    @Modifying
    @Query("delete from Registration r where r.email=:email")
    int deleteByEmail(@Param("email")String email);

    @Modifying
    @Query("update Registration r set r.status='TIMED_OUT', r.password='timed_out_pwd_stub' where (r.status = 'PENDING') and (r.crDate < :deadLine)")
    int markTooOldRegistration(@Param("deadLine") Date deadLine);
}