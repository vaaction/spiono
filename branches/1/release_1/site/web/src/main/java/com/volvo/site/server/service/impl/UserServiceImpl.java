package com.volvo.site.server.service.impl;

import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.model.entity.UserSettings;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.repository.UserSettingsJpaRepository;
import com.volvo.site.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.volvo.site.server.service.exception.EntityNotFoundException.throwEntityNotFoundIfNull;


@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
    private final UserJpaRepository userJpaRepository;

	private final UserSettingsJpaRepository userSettingsJpaRepository;

    @Autowired
    public UserServiceImpl(UserJpaRepository userJpaRepository, UserSettingsJpaRepository userSettingsJpaRepository) {
        this.userJpaRepository = userJpaRepository;
		this.userSettingsJpaRepository = userSettingsJpaRepository;
    }

    @Modifying
    @Override
    public boolean changePassword(long userId, String oldPassword, String newPassword) {
        checkNotNull(oldPassword);
        checkNotNull(newPassword);

        User user = getActiveUser(userId);
        if (user.tryUpdatePassword(oldPassword, newPassword)) {
            userJpaRepository.save(user);
            return true;
        }
        return false;
    }

    @Modifying
	@Override
	public UserSettings updateLastLogsSearchSettings(long userId, String lastAgentsSearchPhrase, String lastAgentsSearchStatus){
        UserSettings userSettings = getUserSettings(userId);
        userSettings.setLastAgentsSearchPhrase(lastAgentsSearchPhrase);
        userSettings.setLastAgentsSearchStatus(lastAgentsSearchStatus);
        return userSettingsJpaRepository.save(userSettings);
    }

    @Modifying
	@Override
	public UserSettings updateLastLogsSearchAgentId(long userId, long selectedAgentId){
        UserSettings userSettings = getUserSettings(userId);
        userSettings.setLastLogsSearchAgentId(selectedAgentId);
        return userSettingsJpaRepository.save(userSettings);
	}

    @Modifying
    @Override
    public UserSettings updateLastStatAgentId(long userId, long selectedAgentId) {
        UserSettings userSettings = getUserSettings(userId);
        userSettings.setLastStatAgentId(selectedAgentId);
        return userSettingsJpaRepository.save(userSettings);
    }

    @Override
    public UserSettings getUserSettings(long userId){
        UserSettings userSettings = userSettingsJpaRepository.findByUserId(userId);
        if (userSettings == null) {
            userSettings = new UserSettings();
            userSettings.setUserId(userId);
            return userSettingsJpaRepository.save(userSettings);
        }
        return userSettings;
    }

    private User getActiveUser(long userId) {
        return throwEntityNotFoundIfNull(userJpaRepository.findActiveUserById(userId), User.class, userId);
    }
}
