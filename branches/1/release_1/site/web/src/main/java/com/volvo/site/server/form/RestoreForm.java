package com.volvo.site.server.form;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

import static com.volvo.site.server.model.ModelConstants.emailMaxLength;
import static com.volvo.site.server.util.Constants.captchaLength;

@ToString
@Getter
@Setter
public class RestoreForm {

    @NotNull
    @Email
    @NotEmpty
    @Length(max = emailMaxLength)
    private String email;

    @NotNull
    @Length(min = captchaLength, max = captchaLength)
    private String captcha;
}
