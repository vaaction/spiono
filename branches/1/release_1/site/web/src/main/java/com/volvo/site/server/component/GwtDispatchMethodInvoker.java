package com.volvo.site.server.component;

import com.google.common.collect.Maps;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.vm.AutoBeanFactorySource;
import com.google.web.bindery.autobean.vm.impl.JsonSplittable;
import com.volvo.platform.gwt.shared.command.CommandDTO;
import com.volvo.platform.java.JavaUtils;
import com.volvo.site.gwt.usercabinet.shared.dto.BeanFactory;
import com.volvo.site.server.util.exceptions.BadRequestException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.validation.Validator;
import javax.validation.groups.Default;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import static javax.validation.Validation.buildDefaultValidatorFactory;

@Component
public class GwtDispatchMethodInvoker {

    private final Validator validator = buildDefaultValidatorFactory().getValidator();

    private final Map<Class<?>, MethodInvoker> dispatchMethods = Maps.newConcurrentMap();

    private final ThreadLocal<BeanFactory> beanFactoryHolder = new ThreadLocal<BeanFactory>();

    public void allocateThreadLocalBeanFactory() {
        beanFactoryHolder.set(AutoBeanFactorySource.create(BeanFactory.class));
    }

    public void releaseCurrentThreadBeanFactory() {
        beanFactoryHolder.remove();
    }

    public Object invokeDispatcherMethod(Object objToInvoke, CommandDTO command) throws ClassNotFoundException,
            BadRequestException, InvocationTargetException, IllegalAccessException {
        Class<?> commandArgClazz = Class.forName(command.getName());
        Object commandArg = extractCommandArgObject(commandArgClazz, command);
        if (!isValid(commandArg)) {
            throw new BadRequestException();
        }
        return getMethodForInvocation(commandArgClazz, objToInvoke).invoke(commandArg);
    }

    public BeanFactory getThreadLocalBeanFactory() {
        return beanFactoryHolder.get();
    }

    public <T> AutoBean<T>  newAutoBean(Class<T> clazz) {
        return getThreadLocalBeanFactory().create(clazz);
    }

    public <T> T  newAutoBeanAs(Class<T> clazz) {
        return newAutoBean(clazz).as();
    }

    private Object extractCommandArgObject(Class<?> commandArgClazz, CommandDTO command) {
        AutoBean<?> commandArg = newAutoBean(commandArgClazz);
        AutoBeanCodex.decodeInto(JsonSplittable.create(command.getPayload()), commandArg);
        return commandArg.as();
    }

    private MethodInvoker getMethodForInvocation(Class<?> argType, Object objToInvoke) {
        MethodInvoker result = dispatchMethods.get(argType);
        if (result != null) {
            return result;
        }

        Method method = JavaUtils.findMethodByArgumentType(objToInvoke.getClass(), argType);
        if (method == null) {
            throw new IllegalStateException("Cant find method for dispatch " + argType);
        }
        result = new MethodInvoker(method, objToInvoke);
        dispatchMethods.put(argType, result);
        return result;
    }

    private boolean isValid(Object toValidate) {
        return validator.validate(toValidate, Default.class).isEmpty();
    }

    @AllArgsConstructor
    @Getter
    private static class MethodInvoker {
        private final Method method;
        private final Object object;

        public Object invoke(Object arg) throws InvocationTargetException, IllegalAccessException {
            return method.invoke(object, arg);
        }
    }
}
