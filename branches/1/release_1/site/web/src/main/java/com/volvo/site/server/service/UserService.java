package com.volvo.site.server.service;

import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.UserSettings;

import java.util.List;

public interface UserService {

    static final String SEARCHING_NAME_ANY = null;
    static final List<Agent.AgentStatus> FILTER_AGENT_STATUS_OFF = null;

    boolean changePassword(long userId, String oldPassword, String newPassword);

    UserSettings updateLastLogsSearchSettings(long userId, String lastAgentsSearchPhrase, String lastAgentsSearchStatus);

    UserSettings updateLastLogsSearchAgentId(long userId, long selectedAgentId);

    UserSettings updateLastStatAgentId(long userId, long selectedAgentId);

	UserSettings getUserSettings(long userId);
}
