package com.volvo.site.server.service.exception;

import com.volvo.site.server.model.entity.Registration;
import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class ConfirmRegistrationException extends RuntimeException {

    public final Reason reason;

    public final Registration registration;

    public static void throwWithReason(Reason reason, Registration registration) {
        throw new ConfirmRegistrationException(reason, registration);
    }

    public enum Reason {
        ALREADY_REGISTERED, TIMEOUT
    }
}
