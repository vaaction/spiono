package com.volvo.site.server.converter;

import com.google.common.base.Function;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.volvo.site.gwt.usercabinet.shared.dto.PagedAgentsListDTO;
import com.volvo.site.server.model.entity.Agent;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;

import javax.annotation.Nullable;

import static com.volvo.platform.java.CollectionsTransformer.transformToList;


@AllArgsConstructor
public class PageAgentToGetAgentsResultListDTOConverter implements Function<Page<Agent>, PagedAgentsListDTO> {

    private final AutoBeanFactory beanFactory;

    @Override
    public PagedAgentsListDTO apply(@Nullable Page<Agent> agentPage) {
        AgentToAgentsResultItemDTOConverter agentItemConverter = new AgentToAgentsResultItemDTOConverter(beanFactory);
        PagedAgentsListDTO result = beanFactory.create(PagedAgentsListDTO.class).as();
        result.setUserAgents(transformToList(agentPage.getContent(), agentItemConverter));
        new PagingToDTOConverter(agentPage).apply(result);
        return result;
    }
}
