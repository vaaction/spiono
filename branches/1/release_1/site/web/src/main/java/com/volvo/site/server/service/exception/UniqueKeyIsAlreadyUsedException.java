package com.volvo.site.server.service.exception;

public class UniqueKeyIsAlreadyUsedException extends ServiceException {

    public UniqueKeyIsAlreadyUsedException(String uniqueKey){
        super("Unique key " + uniqueKey  + " is not valid.");
    }
}
