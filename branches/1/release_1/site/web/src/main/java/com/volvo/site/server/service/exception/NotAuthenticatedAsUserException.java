package com.volvo.site.server.service.exception;

public class NotAuthenticatedAsUserException extends org.springframework.security.core.AuthenticationException {

    public NotAuthenticatedAsUserException(String msg, Throwable t) {
        super(msg, t);
    }

    public NotAuthenticatedAsUserException(String msg) {
        super(msg);
    }
}
