package com.volvo.site.server.config.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DatabaseProperties {
    public static final String DriverClass = "${db.driver}";
    public static final String Url = "${db.url}";
    public static final String UserName = "${db.username}";
    public static final String Password = "${db.password}";
    public static final String PackagesToScan = "${entitymanager.packages.to.scan}";
    public static final String HibernateDialect = "${hibernate.dialect}";
    public static final String HibernateFormatSql = "${hibernate.format_sql}";
    public static final String HibernateShowSql = "${hibernate.show_sql}";
    public static final String HibernateNamingStrategy = "${hibernate.ejb.naming_strategy}";
    public static final String HibernateSearchProvider = "${hibernate.search.default.directory_provider}";
    public static final String HibernateSearchDir = "${hibernate.search.default.indexBase}";
}
