package com.volvo.site.server.service.exception;


public class UniqueKeyTimedOutException extends ServiceException {

    public UniqueKeyTimedOutException(String uniqueKey){
        super("Unique key " + uniqueKey + " is expired.");
    }
}
