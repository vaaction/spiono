package com.volvo.site.server.service.impl;

import com.volvo.platform.java.JavaUtils;
import com.volvo.site.gwt.usercabinet.shared.dto.GetKeyLogDataDTO;
import com.volvo.site.server.converter.KeyLogDataDTOToKeyLogConverter;
import com.volvo.site.server.dto.KeyLogDTO;
import com.volvo.site.server.dto.KeyLogDataDTO;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.KeyLog;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.KeyLogJpaRepository;
import com.volvo.site.server.service.LogsService;
import com.volvo.site.server.service.impl.util.AgentUtilService;
import com.volvo.site.server.util.LuceneLogsSearchBuilder;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.volvo.site.server.service.impl.util.AgentUtilService.checkCertificate;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
@Transactional(readOnly = true)
public class LogsServiceImpl implements LogsService {

    @PersistenceContext
    EntityManager entityManager;

    private final AgentJpaRepository agentJpaRepository;
    private final KeyLogJpaRepository keyLogJpaRepository;

    @Autowired
    public LogsServiceImpl(AgentJpaRepository agentJpaRepository, KeyLogJpaRepository keyLogJpaRepository) {
        this.agentJpaRepository = agentJpaRepository;
        this.keyLogJpaRepository = keyLogJpaRepository;
    }

    @Modifying
    @Override
    public int writeLogs(KeyLogDTO keyLogDTO) {
        checkCertificate(keyLogDTO.getCertificate());
        Agent agent = agentJpaRepository.findByCertificate(keyLogDTO.getCertificate());
        if (!AgentUtilService.canAgentLog(agent)) {
            return 0;
        }

        KeyLogDataDTOToKeyLogConverter converter = new KeyLogDataDTOToKeyLogConverter(agent.getId());
        Collection<KeyLog> keyLogs = newArrayList();
        for (KeyLogDataDTO dto: keyLogDTO.getKeyLogs()) {
            if (JavaUtils.isPrintableString(dto.getText())) {
                keyLogs.add(converter.apply(dto));
            }
        }
        if (!keyLogs.isEmpty()) {
            keyLogJpaRepository.save(keyLogs);
        }
        return keyLogs.size();
    }

    @Override
    public List<KeyLog> search(GetKeyLogDataDTO filter) {
        checkNotNull(filter);

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
        QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory().buildQueryBuilder()
                .forEntity(KeyLog.class).get();
        FullTextQuery persistenceQuery = fullTextEntityManager.createFullTextQuery(createSearchQuery(queryBuilder, filter),
                KeyLog.class);

        setPaginationForQuery(persistenceQuery, filter);

        List<KeyLog> resultList =  persistenceQuery.getResultList();
        Collections.reverse(resultList);
        return resultList;
    }

    private static Query createSearchQuery(QueryBuilder queryBuilder, GetKeyLogDataDTO filter){
        LuceneLogsSearchBuilder builder = new LuceneLogsSearchBuilder(queryBuilder);
        addAgentFilter(builder, filter);
        addPhraseFilter(builder, filter);
        addStartDateFilter(builder, filter);
        addEndDateFilter(builder, filter);
        return builder.build();
    }

    private static void addAgentFilter(LuceneLogsSearchBuilder builder, GetKeyLogDataDTO filter) {
        if (filter.getAgentId() != GetKeyLogDataDTO.ALL_AGENTS) {
            builder.withAgent(filter.getAgentId());
        }
    }

    private static void addPhraseFilter(LuceneLogsSearchBuilder builder, GetKeyLogDataDTO filter) {
        if (isNotBlank(filter.getSearchingString())) {
            builder.withSearchString(filter.getSearchingString());
        }
    }

    private static void addStartDateFilter(LuceneLogsSearchBuilder builder, GetKeyLogDataDTO filter) {
        if (filter.getStartDate() != null) {
            builder.withStartDate(filter.getStartDate());
        }
    }

    private static void addEndDateFilter(LuceneLogsSearchBuilder builder, GetKeyLogDataDTO filter) {
        if (filter.getEndDate() != null) {
            builder.withEndDate(filter.getEndDate());
        }
    }

    private static void setPaginationForQuery(FullTextQuery persistenceQuery, GetKeyLogDataDTO filter){
        int searchFrom = getIndexToSearchFrom(persistenceQuery.getResultSize(), filter.getPageNumber(),	filter.getPageSize());
        int pageSize = filter.getPageSize();
        if(searchFrom < 0){
            pageSize = pageSize + searchFrom;
            searchFrom = 0;
        }
        persistenceQuery.setFirstResult(searchFrom);
        persistenceQuery.setMaxResults(pageSize);
    }

    private static int getIndexToSearchFrom(int resultSize, int pageNumber, int pageSize){
        return resultSize - ((pageNumber + 1) * pageSize);
    }
}
