package com.volvo.site.server.service.impl;

import com.volvo.site.server.converter.StatisticsDataDTOToStatisticsLogConverter;
import com.volvo.site.server.dto.StatisticsDataDTO;
import com.volvo.site.server.dto.StatisticsLogDTO;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.StatisticsLog;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.StatisticsJpaRepository;
import com.volvo.site.server.service.StatisticsService;
import com.volvo.site.server.util.ProcessBlackList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.volvo.platform.java.ArrayIterable.newArrayIterable;
import static com.volvo.platform.java.CollectionsTransformer.transformToArray;
import static com.volvo.site.server.service.impl.util.AgentUtilService.canAgentLog;
import static com.volvo.site.server.service.impl.util.AgentUtilService.checkCertificate;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
@Transactional(readOnly = true)
public class StatisticsServiceImpl implements StatisticsService {
	private final StatisticsJpaRepository statisticsJpaRepository;

	private final AgentJpaRepository agentJpaRepository;

	@Autowired
	public StatisticsServiceImpl(StatisticsJpaRepository statisticsJpaRepository,
                                 AgentJpaRepository agentJpaRepository){
		this.statisticsJpaRepository = statisticsJpaRepository;
		this.agentJpaRepository = agentJpaRepository;
	}

	@Override
	public void logAgentData(StatisticsLogDTO spyLog){
		checkCertificate(spyLog.getCertificate());
		Agent agent = agentJpaRepository.findByCertificate(spyLog.getCertificate());
		if (!canAgentLog(agent)) {
			return;
		}

		StatisticsDataDTOToStatisticsLogConverter converter = new StatisticsDataDTOToStatisticsLogConverter(agent.getId());
        StatisticsDataDTO[] spyLogsArray = spyLog.getSpyData();
		StatisticsLog[] statisticsLogs = transformToArray(spyLogsArray, new StatisticsLog[spyLogsArray.length], converter);
		statisticsJpaRepository.save(newArrayIterable(statisticsLogs));
	}

	@Override
	public Map<String, Integer> listStatisticForAgent(Long agentId, Date startDate, Date endDate){
		boolean searchByDate = startDate != null && endDate != null;
		List<StatisticsLog> keyLogList = statisticsJpaRepository.findByAgentId(agentId, searchByDate, startDate, endDate,
				ProcessBlackList.getList());
		return convertStatisticsLogToStatistic(keyLogList);
	}

	private Map<String, Integer> convertStatisticsLogToStatistic(List<StatisticsLog> statisticsLogList){
		long commonTime = calculateCommonUsageTime(statisticsLogList);
		Map<String, Long> commonProgramsTime = mergeToMapAndCalculateCommon(statisticsLogList);
		Map<String, Integer> result = new HashMap<String, Integer>();
		for(Map.Entry<String, Long> program : commonProgramsTime.entrySet()){
			result.put(program.getKey(), calculatePercentOfUsage(commonTime, program.getValue()));
		}
		return result;
	}

	private long calculateCommonUsageTime(List<StatisticsLog> statisticsLogList){
		long result = 0;
		for(StatisticsLog statisticsLog : statisticsLogList){
			result += statisticsLog.getTimeOfUse();
		}
		return result;
	}

	private Map<String, Long> mergeToMapAndCalculateCommon(List<StatisticsLog> source){
		Map<String, Long> result = new HashMap<String, Long>();
		String processDescription;
		for (StatisticsLog statisticsLog : source){

			if(isNotBlank(statisticsLog.getProcessDescription())){
				processDescription = statisticsLog.getProcessDescription();
			} else {
				processDescription = statisticsLog.getProcessName();
			}

			if(result.get(processDescription) == null){
				result.put(processDescription, statisticsLog.getTimeOfUse());
			} else {
				result.put(processDescription, statisticsLog.getTimeOfUse() + result.get(processDescription));
			}
		}
		return result;
	}

	private int calculatePercentOfUsage(long commonTime, long programTime){
		int time = (int)((programTime * 100) / commonTime);
		if(time == 0){
			//percent < 1% => return 1%;
			return 1;
		}
		return time;
	}
}
