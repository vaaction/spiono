package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.UserSettings;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface UserSettingsJpaRepository extends JpaRepositoryWithLongId<UserSettings> {

	UserSettings findByUserId(long userId);
}
