package com.volvo.site.server.util;

import com.volvo.platform.gwt.shared.dto.GwtDTO;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;

import java.io.IOException;
import java.lang.reflect.Type;

import static com.volvo.platform.java.IOUtils.writeStringAsUtf8ToStream;
import static com.volvo.site.server.util.ServerUtil.deserializeGwtJSON;
import static com.volvo.site.server.util.ServerUtil.serializeGwtJSON;

public class GwtJsonMessageConverter extends MappingJacksonHttpMessageConverter {

	@Override
	public boolean canWrite(Class<?> clazz, MediaType mediaType) {
		return canWrite(mediaType) && (super.canWrite(clazz, mediaType) || supportsGwtSerialization(clazz));
	}

	@Override
	protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage) throws IOException,
			HttpMessageNotReadableException {
		if (supportsGwtSerialization(clazz)) {
			return deserializeGwtJSON(inputMessage.getBody(), clazz);
		} else {
			return super.readInternal(clazz, inputMessage);
		}
	}

    @Override
    public Object read(Type type, Class<?> contextClass, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        if (type instanceof Class) {
            Class<?> clazz = (Class)type;
            if (supportsGwtSerialization(clazz)) {
                return deserializeGwtJSON(inputMessage.getBody(), clazz);
            }
        }
        return super.read(type, contextClass, inputMessage);
    }

    @Override
	protected void writeInternal(Object object, HttpOutputMessage outputMessage) throws IOException,
			HttpMessageNotWritableException {
		if (supportsGwtSerialization(object.getClass())) {
            writeStringAsUtf8ToStream(serializeGwtJSON(object), outputMessage.getBody());
		} else {
			super.writeInternal(object, outputMessage);
		}
	}

	private static boolean supportsGwtSerialization(Class<?> clazz) {
		return GwtDTO.class.isAssignableFrom(clazz);
	}
}
