package com.volvo.site.server.controller.android;

import com.volvo.site.server.dto.android.AndroidLogDTO;
import com.volvo.site.server.service.AndroidService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping(value = "android", method = POST,
        consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
@Slf4j
public class AndroidAgent {

	@Autowired
	private AndroidService androidService;

    @RequestMapping(value = "/androidLog")
    @ResponseBody
    public void androidLog(@RequestBody @Valid AndroidLogDTO androidLog) {
		AndroidAgent.log.info("> AndroidLogDTO: {}", androidLog);
		androidService.logAgentData(androidLog);
    }
}
