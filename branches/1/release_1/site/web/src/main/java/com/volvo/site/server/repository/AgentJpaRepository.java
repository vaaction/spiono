package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.Agent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(readOnly = true)
public interface AgentJpaRepository extends JpaRepositoryWithLongId<Agent> {

    Agent findByIdAndClientKey(long id, String clientKey);

    Agent findByIdAndStatus(long id, Agent.AgentStatus status);

    Agent findByClientKeyAndStatus(String clientKey, Agent.AgentStatus status);

    Agent findByCertificate(String certificate);

    @Query("select a from Agent a where a.userId=:userId" +
            " and (:searchingName is null or a.name like :searchingName)" +
            " and (:isStatusFilterEnabled=false or a.status in (:statusFilter))" +
            " order by a.id desc")
    Page<Agent> findAgentsForUser(@Param("userId") long userId, @Param("searchingName") String searchingName,
                                  @Param("isStatusFilterEnabled") boolean isStatusFilterEnabled,
                                  @Param("statusFilter") List<Agent.AgentStatus> statusFilter, Pageable pageable);

    @Query("select a from Agent a where a.userId=:userId" +
            " and (:isStatusFilterEnabled=false or a.status in (:statusFilter))" +
            " order by a.id desc")
    List<Agent> findAgentsForUser(@Param("userId") long userId,
                                  @Param("isStatusFilterEnabled") boolean isStatusFilterEnabled,
                                  @Param("statusFilter") List<Agent.AgentStatus> statusFilter);

    @Query("SELECT DISTINCT NEW com.volvo.site.server.model.entity.Agent(a.id, a.name, a.status) " +
            "FROM Agent a " +
            "INNER JOIN a.keyLogs k " +
            "WHERE a.userId=:userId ")
    List<Agent> findAgentsWithLogsForUser(@Param("userId") long userId);
}
