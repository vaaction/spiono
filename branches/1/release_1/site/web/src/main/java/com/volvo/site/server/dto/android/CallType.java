package com.volvo.site.server.dto.android;

public enum CallType {
    INCOMING, OUTGOING, MISSED
}
