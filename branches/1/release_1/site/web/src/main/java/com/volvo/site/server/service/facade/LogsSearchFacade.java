package com.volvo.site.server.service.facade;

import com.volvo.site.gwt.usercabinet.shared.dto.GetKeyLogDataDTO;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.KeyLog;
import com.volvo.site.server.service.AgentService;
import com.volvo.site.server.service.LogsService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Service
@Transactional(readOnly = true)
@NoArgsConstructor
public class LogsSearchFacade {

    public final static Agent.AgentStatus[] STATUSES_WITH_LOGS = {
            Agent.AgentStatus.AGENT_REGISTERED, Agent.AgentStatus.AGENT_DELETED};

    @Autowired
    LogsService logsService;

    @Autowired
    AgentService agentService;

    @Autowired
    public LogsSearchFacade(LogsService logsService, AgentService agentService) {
        this.logsService = logsService;
        this.agentService = agentService;
    }

    @Modifying
    public PerformLogsSearchResult performLogsSearch(long userId, GetKeyLogDataDTO searchParams) {
        List<KeyLog> keylogs = logsService.search(searchParams);
        List<Agent> agents = agentService.getAgents(userId, newArrayList(STATUSES_WITH_LOGS));
        return new PerformLogsSearchResult(keylogs, agents);
    }

    @AllArgsConstructor
    public static class PerformLogsSearchResult {
        public final List<KeyLog> keylogs;
        public List<Agent> agents;
    }
}
