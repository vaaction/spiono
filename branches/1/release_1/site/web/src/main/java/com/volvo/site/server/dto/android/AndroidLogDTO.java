package com.volvo.site.server.dto.android;

import lombok.*;
import org.codehaus.jackson.annotate.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@ToString(callSuper = true)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AndroidLogDTO {

    @JsonProperty(value = "AndroidCallData")
    @NotNull
    @NotEmpty
    private AndroidCallDataDTO[] callLogs;

	@JsonProperty(value = "AndroidSmsData")
	@NotNull
	@NotEmpty
	private AndroidSmsDataDTO[] smsLogs;
}
