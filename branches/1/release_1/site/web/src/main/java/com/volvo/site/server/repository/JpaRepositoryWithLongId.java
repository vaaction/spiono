package com.volvo.site.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaRepositoryWithLongId<T> extends JpaRepository<T, Long> {
}
