package com.volvo.site.server.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants {
    public static final int maxPageSize = 100;

    public static final int installerIdLen = 20;
    public static final int certificateLen = 80;

    public static final int errorMessageMax = 2000;
    public static final int captchaLength = 4;
}
