package com.volvo.site.server.controller.pages;

import com.volvo.platform.spring.captcha.Captcha;
import com.volvo.site.server.component.Authenticator;
import com.volvo.site.server.form.LoginForm;
import com.volvo.site.server.form.RegisterForm;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.service.LoginService;
import com.volvo.site.server.service.exception.ConfirmRegistrationException;
import com.volvo.site.server.service.exception.DuplicateEntityException;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.util.ControllersPaths;
import com.volvo.site.server.util.MessagesConsts;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.util.Locale;

import static com.volvo.platform.java.EmailUtils.getEmailSitename;
import static com.volvo.platform.spring.MvcUtil.modelAndView;
import static com.volvo.platform.spring.MvcUtil.modelAndViewWithForm;
import static com.volvo.platform.spring.captcha.CaptchaMvcUtils.generateBufferedImageCaptcha;
import static com.volvo.platform.spring.captcha.CaptchaMvcUtils.isCaptchaValid;
import static com.volvo.site.server.util.ControllersPaths.IndexController.*;

@SessionAttributes(Captcha.SESSION_CAPTCHA_ATTR)
@Controller
@RequestMapping(value = REGISTER)
public class Register {
    @Autowired
    private LoginService loginService;

    @Autowired
    private Authenticator authenticator;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView register() {
        return modelAndViewWithForm(VIEW_REGISTER, RegisterForm.class);
    }

    @RequestMapping(value = "/registerAjax", method = RequestMethod.POST)
    @ResponseBody
    public String registerAjax(@Valid RegisterForm form,
                               ModelMap modelMap) throws InterruptedException {
        if (!isCaptchaValid(form.getCaptcha(), modelMap)) {
            return "wrong_captcha";
        }

        try {
            loginService.register(form.getEmail(), form.getPassword(), Locale.ENGLISH);
            return "ok";
        }
        catch (DuplicateEntityException exception) {
            return "email_busy";
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ModelAndView register(ModelMap modelMap, @Valid RegisterForm form,
                                 BindingResult bindingResult, HttpServletRequest request) {
        if (!isCaptchaValid(form.getCaptcha(), modelMap)) {
            ((RegisterForm)bindingResult.getTarget()).setCaptcha("");
            bindingResult.rejectValue("captcha", MessagesConsts.ERR_WRONG_CAPTCHA);
        }
        if (!form.getPassword().equalsIgnoreCase(form.getConfirmPassword())) {
            ((RegisterForm)bindingResult.getTarget()).setPassword("");
            ((RegisterForm)bindingResult.getTarget()).setConfirmPassword("");
            bindingResult.rejectValue("confirmPassword", MessagesConsts.ERR_CONFIRM_PASSWORD);
        }
        if (bindingResult.hasErrors()) {
            return modelAndView(VIEW_REGISTER, bindingResult);
        }

        try {
            loginService.register(form.getEmail(), form.getPassword(),
		            RequestContextUtils.getLocaleResolver(request).resolveLocale(request));
        }
        catch (DuplicateEntityException exception) {
            return modelAndView(VIEW_REGISTER, bindingResult).addObject("duplicateEmail", Boolean.TRUE);
        }
        return justRegistered(form.getEmail());
    }

    @RequestMapping(value = "/registration-confirm/{regCode}", method = RequestMethod.GET)
    public ModelAndView registerConfirm(@PathVariable("regCode") String regCode,
                                        HttpServletRequest request,
                                        HttpServletResponse response) {
        try {
            Pair<String, User> confirmResult = loginService.confirmRegistration(regCode);
            String email = confirmResult.getRight().getEmail();
            String password = confirmResult.getLeft();
            return authenticator.authenticateAndRedirect(email, password, request, response);
        }
        catch (EntityNotFoundException exception) {
            return new ModelAndView("register-confirm");
        }
        catch (ConfirmRegistrationException exception) {
            String email = exception.registration.getEmail();
            switch (exception.reason) {
                case ALREADY_REGISTERED:
                    LoginForm loginForm = new LoginForm();
                    loginForm.setEmail(email);
                    return modelAndViewWithForm(VIEW_LOGIN, loginForm,
                            new ModelMap("confirmRegistration", Boolean.TRUE));
                case TIMEOUT:
                    RegisterForm registerForm = new RegisterForm();
                    registerForm.setEmail(email);
                    return modelAndViewWithForm(VIEW_REGISTER, registerForm,
                            new ModelMap("confirmRegistration", Boolean.TRUE));
                default:
                    throw new IllegalStateException("Unhandled enum value " + exception.reason);
            }
        }
    }

    @RequestMapping(value = "/just-registered/{email}", method = RequestMethod.GET)
    public ModelAndView justRegistered(@PathVariable("email") String email) {
        return new ModelAndView(VIEW_JUST_REGISTERED, "email", getEmailSitename(email));
    }

    @RequestMapping(value = ControllersPaths.IndexController.REGISTER_CAPTCHA, method = RequestMethod.GET,
            headers = "Accept=image/jpeg")
    @ResponseBody
    public BufferedImage captcha(ModelMap model) {
        return generateBufferedImageCaptcha(model);
    }
}
