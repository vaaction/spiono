package com.volvo.site.server.component;

import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.util.ExtendedUserDetails;
import org.hibernate.validator.constraints.impl.EmailValidator;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.volvo.site.server.config.constants.SecurityRoles.ROLE_USER;
import static java.util.Collections.singletonList;
import static java.util.Collections.unmodifiableList;

@Component
@Lazy
public class PwdAndEmailUserDetailsService implements UserDetailsService {

    private static final List<SimpleGrantedAuthority> ROLE_USER_LIST = unmodifiableList(singletonList(new SimpleGrantedAuthority(ROLE_USER)));

    @Inject
    private UserJpaRepository userRepository;

    @Override
    public ExtendedUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        checkNotNull(username);
        if (!new EmailValidator().isValid(username, null)) {
            throw new IllegalArgumentException("Can not find user because email is invalid: " + username);
        }

        User user = userRepository.findUserByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("Can not find user with email: " + username);
        }

        return new ExtendedUserDetails(user, ROLE_USER_LIST);
    }
}
