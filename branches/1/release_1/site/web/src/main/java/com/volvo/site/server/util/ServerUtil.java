package com.volvo.site.server.util;


import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;
import com.google.web.bindery.autobean.vm.AutoBeanFactorySource;
import com.volvo.site.gwt.usercabinet.shared.dto.BeanFactory;
import com.volvo.site.server.config.constants.SecurityRoles;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.security.core.Authentication;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Future;

import static com.volvo.platform.java.IOUtils.streamToUtf8String;
import static org.springframework.security.core.authority.AuthorityUtils.authorityListToSet;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ServerUtil {
    public static <T> T deserializeGwtJSON(InputStream stream, Class<T> clazz) throws IOException {
        return deserializeGwtJSON(streamToUtf8String(stream), clazz);
    }

	public static <T> T deserializeGwtJSON(String body, Class<T> clazz) {
		BeanFactory autoBeanFactory = AutoBeanFactorySource.create(BeanFactory.class);
		AutoBean<T> bean = AutoBeanCodex.decode(autoBeanFactory, clazz, body);
		return bean.as();
	}

	public static <T> String serializeGwtJSON(T dto) {
		return AutoBeanCodex.encode(AutoBeanUtils.getAutoBean(dto)).getPayload();
	}

    public static <T> Future<T> asyncResult(T result) {
        return new AsyncResult<T>(result);
    }

    public static boolean hasUserAuthority(Authentication authentication) {
        return isAuthorityGranted(authentication, SecurityRoles.ROLE_USER);
    }

    public static boolean hasAdminAuthority(Authentication authentication) {
        return isAuthorityGranted(authentication, SecurityRoles.ROLE_ADMIN);
    }

    public static boolean isAuthorityGranted(Authentication authentication, String expectedAuthority) {
        return authorityListToSet(authentication.getAuthorities()).contains(expectedAuthority);
    }
}
