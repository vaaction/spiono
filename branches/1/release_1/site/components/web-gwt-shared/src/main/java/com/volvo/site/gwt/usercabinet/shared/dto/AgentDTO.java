package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.site.gwt.usercabinet.shared.enums.AgentStatus;

public interface AgentDTO extends HasAgentIdDTO {

    void setClientKey(String clientKey);
    String getClientKey();

    void setStatus(AgentStatus agentStatus);
    AgentStatus getStatus();

    void setName(String agentName);
    String getName();
}
