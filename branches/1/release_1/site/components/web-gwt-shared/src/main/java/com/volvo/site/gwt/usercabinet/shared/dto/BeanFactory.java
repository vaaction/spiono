package com.volvo.site.gwt.usercabinet.shared.dto;

import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.volvo.platform.gwt.shared.command.CommandAutoBeanFactory;
import com.volvo.platform.gwt.shared.remerror.RemoteErrorBeanFactory;

public interface BeanFactory extends AutoBeanFactory, CommandAutoBeanFactory, RemoteErrorBeanFactory {

    AutoBean<ChangePwdDTO> changePwd();

    AutoBean<CreateAgentDTO> createAgentDTO();

    AutoBean<CreateAgentResultDTO> createAgentResultDTO();

    AutoBean<CheckAgentRegisteredDTO> checkAgentRegisteredDTO();

    AutoBean<DeleteAgentDTO> deleteAgentDTO();

    AutoBean<DeleteAndCreateNewAgentDTO> deleteAndCreateNewAgentDTO();

    AutoBean<SetAgentNameDTO> setAgentNameDTO();

    AutoBean<GetAgentsDTO> getAgentsDTO();

    AutoBean<PagedAgentsListDTO> getAgentsResultListDTO();

    AutoBean<SetAgentOnPauseDTO> setAgentOnPauseDTO();

	AutoBean<KeyLogDataListResultDTO> getKeyLogDataListResultDTO();

	AutoBean<GetKeyLogDataDTO> getKeyLogDataDTO();

	AutoBean<StartupInformationDTO> startupInformationDTO();

    AutoBean<GetStartupInformationDTO> getStartupInformation();

    AutoBean<UpdateAgentsViewStartupInfoDTO> updateAgentsViewStartupInfoDTO();

	AutoBean<UpdateLogsViewStartupInfoDTO> updateLogsViewStartupInfoDTO();

	AutoBean<UpdateStatisticViewStartupInfoDTO> updateStatisticViewStartupInfoDTO();

	AutoBean<GetAgentStatisticDTO> getAgentStatisticDTO();

	AutoBean<GetAgentStatisticResultDTO> getAgentStatisticResultDTO();
}
