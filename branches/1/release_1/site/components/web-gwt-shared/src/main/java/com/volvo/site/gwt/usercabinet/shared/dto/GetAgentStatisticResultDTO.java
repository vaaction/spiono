package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.GwtDTO;

import java.util.Map;

public interface GetAgentStatisticResultDTO extends GwtDTO {

	// Process -> Usage percentage
	Map<String, Integer> getStatistic();
	void setStatistic(Map<String, Integer> statistic);
}
