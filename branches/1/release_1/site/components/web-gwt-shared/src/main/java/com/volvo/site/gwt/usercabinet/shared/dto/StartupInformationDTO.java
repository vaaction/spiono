package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.GwtDTO;
import com.volvo.site.gwt.usercabinet.shared.enums.StatusFilter;

public interface StartupInformationDTO extends GwtDTO {

	String getLastAgentsSearchPhrase();
	void setLastAgentsSearchPhrase(String searchingString);

	StatusFilter getLastAgentsSearchAgentStatus();
	void setLastAgentsSearchAgentStatus(StatusFilter status);

	Long getLastLogsSearchAgentId();
	void setLastLogsSearchAgentId(Long selectedAgentId);

	Long getLastStatAgentId();
	void setLastStatAgentId(Long selectedAgentId);
}

