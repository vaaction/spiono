package com.volvo.site.mailing.service;

import com.volvo.platform.spring.qualifiers.SyncImplementation;
import org.springframework.stereotype.Service;

@Service
@SyncImplementation
class EmailServiceImplSync extends EmailServiceImplBase {
}
