package com.volvo.site.mailing.config;


import com.volvo.platform.spring.email.FreemarkerEmalingConfigBase;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {
        com.volvo.platform.spring.email.service.PackageInfo.PACKAGE_NAME,
        com.volvo.site.mailing.service.PackageInfo.PACKAGE_NAME})
@Configuration
public class EmailingConfig extends FreemarkerEmalingConfigBase {
    @Override
    protected String getTemplatesPackageClasspath() {
        return com.volvo.site.mailing.templates.PackageInfo.PACKAGE_CLASSPATH;
    }
}
