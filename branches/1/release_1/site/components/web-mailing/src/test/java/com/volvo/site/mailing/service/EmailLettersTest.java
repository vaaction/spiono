package com.volvo.site.mailing.service;

import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static com.volvo.platform.java.testing.TestUtils.isEnumValuesAsStringAreUnique;
import static com.volvo.platform.java.testing.TestUtils.isFileExistsInClassPath;
import static org.testng.Assert.assertTrue;

public class EmailLettersTest {

    @Test
    public void checkFilesExists() throws FileNotFoundException {
        for (EmailLetters letter: EmailLetters.values()) {
            assertTrue(isFileExistsInClassPath(letter.getSubjectPath()));
            assertTrue(isFileExistsInClassPath(letter.getBodyPath()));
        }
    }

    @Test
    public void checkEnumValuesUnique() {
        assertTrue(isEnumValuesAsStringAreUnique(EmailLetters.values()));
    }
}
