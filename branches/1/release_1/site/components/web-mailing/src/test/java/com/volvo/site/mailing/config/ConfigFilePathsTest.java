package com.volvo.site.mailing.config;

import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static com.volvo.platform.java.testing.TestUtils.isFileExistsInPath;
import static org.testng.Assert.assertTrue;

public class ConfigFilePathsTest {

    @Test
    public void checkPropertiesFilesExists() throws FileNotFoundException {
        assertTrue(isFileExistsInPath(ConfigFilePaths.CONF_DEV_PATH));
        assertTrue(isFileExistsInPath(ConfigFilePaths.CONF_PROD_PATH));
    }
}
