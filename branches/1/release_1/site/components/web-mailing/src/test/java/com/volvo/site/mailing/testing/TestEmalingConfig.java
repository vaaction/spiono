package com.volvo.site.mailing.testing;

import freemarker.template.TemplateModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class TestEmalingConfig {

    @Autowired
    private freemarker.template.Configuration freemarkerConfiguration;

    @PostConstruct
    private void postConstruct() throws TemplateModelException {
        freemarkerConfiguration.setSharedVariable("sitename", "localhost:8080");
        freemarkerConfiguration.setSharedVariable("hostname", "spiono-test");
    }
}
