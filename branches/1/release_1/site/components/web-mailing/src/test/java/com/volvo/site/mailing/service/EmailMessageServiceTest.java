package com.volvo.site.mailing.service;

import com.volvo.platform.spring.email.conf.TestMailSenderConfig;
import com.volvo.site.mailing.config.EmailingConfig;
import com.volvo.site.mailing.testing.TestEmalingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Locale;

import static com.volvo.platform.java.testing.TestUtils.randomEmail;

@ContextConfiguration(classes = {
        EmailingConfig.class, TestEmalingConfig.class, TestMailSenderConfig.class
})
public class EmailMessageServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    EmailTemplateService emailTemplateService;

    @Test
    public void registrationEmailTest() {
        emailTemplateService.registrationEmail(randomEmail(), "123", Locale.ENGLISH);
    }
}
