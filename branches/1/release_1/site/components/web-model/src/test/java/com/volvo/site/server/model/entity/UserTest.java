package com.volvo.site.server.model.entity;

import org.mockito.Mock;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

import static com.volvo.platform.java.JavaUtils.invokePrivateMethod;
import static com.volvo.platform.java.testing.TestUtils.randomEmail;
import static com.volvo.platform.java.testing.TestUtils.randomPwd;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

public class UserTest {

    private final String email = randomEmail();

    private final String password = randomPwd();

    private final Registration registration = Registration.of(email, password);

    @Mock
    private ForgotPassword forgotPassword;

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void of1NpeTest() {
        User.of(null);
    }

    @Test
    public void of1Test() {
        checkCommons(User.of(registration));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void of2NpeTest1() {
        User.of(email, null);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void of2NpeTest2() {
        User.of(null, email);
    }

    @Test
    public void of2Test() {
        checkCommons(User.of(email, password));
    }

    @Test
    public void prePersistTest() throws InvocationTargetException, IllegalAccessException {
        User user = User.of(email.toUpperCase(), password);
        user.incrementErrLogins();

        invokePrivateMethod(User.class, user, "prePersist");

        assertEquals(user.getErrLogins(), 0L);
        assertNotNull(user.getCrDate());
        assertEquals(user.getEmail(), email);
    }

    @Test
    public void preUpdateTest() throws InvocationTargetException, IllegalAccessException {
        User user = User.of(email.toUpperCase(), password);
        invokePrivateMethod(User.class, user, "preUpdate");
        assertEquals(user.getEmail(), email);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void addForgotPasswordNpeTest() {
        User.of(registration).addForgotPassword(null);
    }

    @Test
    public void addForgotPasswordTest() {
        User user = User.of(registration);
        user.addForgotPassword(forgotPassword);
        assertEquals(user.getForgotPasswords().get(0), forgotPassword);
    }

    @Test
    public void getForgotPasswordsTest() {
        User user = User.of(registration);
        assertEquals(user.getForgotPasswords().size(), 0);

        user.addForgotPassword(forgotPassword);
        assertEquals(user.getForgotPasswords().size(), 1);
        assertEquals(user.getForgotPasswords().get(0), forgotPassword);
    }

    @Test
    public void incrementErrLoginsTest() {
        User user = User.of(registration);
        assertEquals(user.getErrLogins(), 0L);
        assertEquals(user.incrementErrLogins(), 1L);
        assertEquals(user.getErrLogins(), 1L);
    }

    @Test
    public void resetErrLoginsTest() {
        User user = User.of(registration);
        user.incrementErrLogins();
        assertEquals(user.getErrLogins(), 1L);

        user.resetErrLogins();
        assertEquals(user.getErrLogins(), 0L);
    }

    @Test
    public void tryUpdatePasswordPositiveTest() {
        // Given
        String oldPwd = randomPwd();
        User user = User.of(randomEmail(), oldPwd);

        // When
        String newPwd = randomPwd();
        boolean isPwdChanged = user.tryUpdatePassword(oldPwd, newPwd);

        // Then
        assertTrue(isPwdChanged);
        assertTrue(BCrypt.checkpw(newPwd, user.getPassword()));
    }

    @Test
    public void tryUpdatePasswordNegativeTest() {
        // Given
        String oldPwd = randomPwd();
        User user = User.of(randomEmail(), oldPwd);

        // When
        boolean isPwdChanged = user.tryUpdatePassword(randomPwd(), randomPwd());

        // Then
        assertFalse(isPwdChanged);
        assertTrue(BCrypt.checkpw(oldPwd, user.getPassword()));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldBeNPEWhenCallingTryUpdatePasswordWithNullOldPassword() {
        User.of(randomEmail(), randomPwd()).tryUpdatePassword(null, randomPwd());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldBeNPEWhenCallingTryUpdatePasswordWithNullNewPassword() {
        User.of(randomEmail(), randomPwd()).tryUpdatePassword(randomPwd(), null);
    }

    private void checkCommons(User user) {
        assertNull(user.getId());
        assertEquals(user.getEmail(), email);
        assertTrue(BCrypt.checkpw(password, user.getPassword()));
        assertNull(user.getCrDate());
        assertFalse(user.isActive());
        assertEquals(user.getErrLogins(), 0L);
    }
}
