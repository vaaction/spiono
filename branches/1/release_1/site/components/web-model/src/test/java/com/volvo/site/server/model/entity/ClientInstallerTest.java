package com.volvo.site.server.model.entity;

import org.mockito.Mock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

import static com.volvo.platform.java.JavaUtils.invokePrivateMethod;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

public class ClientInstallerTest {

    private final byte[] data = new byte[0];

    @Mock
    private AgentInstaller agentInstaller;

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    @Test
    public void ofTest() {
        ClientInstaller client = ClientInstaller.of(data);

        assertNull(client.getId());
        assertEquals(client.getAgentInstallers().size(), 0);
        assertNull(client.getCrDate());
        assertEquals(client.getData(), data);
        assertFalse(client.isActive());
        assertFalse(client.isDownloaded());
        assertFalse(client.isActiveOrDownloaded());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void addAgentInstallerNpeTest() {
        ClientInstaller client = ClientInstaller.of(data);
        client.addAgentInstaller(null);
    }

    @Test
    public void addAgentInstallerTest() {
        ClientInstaller client = ClientInstaller.of(data);
        client.addAgentInstaller(agentInstaller);

        assertEquals(client.getAgentInstallers().size(), 1);
        assertEquals(client.getAgentInstallers().get(0), agentInstaller);
    }

    @Test
    public void getAgentInstallersTest() {
        ClientInstaller client = ClientInstaller.of(data);
        assertEquals(client.getAgentInstallers().size(), 0);

        client.addAgentInstaller(agentInstaller);

        assertEquals(client.getAgentInstallers().size(), 1);
        assertEquals(client.getAgentInstallers().get(0), agentInstaller);
    }

    @Test(dataProvider = "bitwizeFlags")
    public void isActiveOrDownloadedTest(boolean active, boolean downloaded, boolean expectedResult) {
        ClientInstaller client = ClientInstaller.of(data);
        client.setActive(active);
        client.setDownloaded(downloaded);

        assertEquals(client.isActive(), active);
        assertEquals(client.isDownloaded(), downloaded);
        assertEquals(client.isActiveOrDownloaded(), expectedResult);
    }

    @Test
    public void prePersistTest() throws InvocationTargetException, IllegalAccessException {
        ClientInstaller client = ClientInstaller.of(data);
        assertNull(client.getCrDate());

        invokePrivateMethod(ClientInstaller.class, client, "prePersist");

        assertNotNull(client.getCrDate());
    }

    @DataProvider(name="bitwizeFlags")
    public Object[][] bitwizeFlags() {
        return new Object[][]{
                {false, false, false},
                {true, false, true},
                {false, true, true},
                {true, true, true},
        };
    }
}
