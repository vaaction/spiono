package com.volvo.site.server.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "user_settings")
@NoArgsConstructor
@Getter
@Setter
public class UserSettings implements Serializable {
    @Id
    private long id;

    @Column
    private long userId;

    @Column
    @Size(max = 50)
    private String lastAgentsSearchPhrase;

    @Column
    private String lastAgentsSearchStatus;

	@Column
	private Long lastLogsSearchAgentId;

	@Column
	private Long lastStatAgentId;
}
