package com.volvo.site.server.model.entity;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "vk_users")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class VKUser implements Serializable {

    public static VKUser of(long userId, int expiresIn, String accessToken, String userName){
        VKUser vkUser = new VKUser();
        vkUser.setUserId(userId);
        vkUser.setExpiresIn(expiresIn);
        vkUser.setAccessToken(accessToken);
        vkUser.setUserName(userName);
        return vkUser;
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column
    @NotNull
    private Long userId;

    @Column
    @NotNull
    private Integer expiresIn; // in seconds

    @Column
    @NotNull
    private String accessToken;

    @Column
    private String userName;

    @Column(nullable = false)
    @Setter(AccessLevel.NONE)
    private Date crDate;

    @OneToMany(targetEntity = Agent.class, fetch = FetchType.LAZY, mappedBy = "vkUser")
    @Setter(AccessLevel.NONE)
    private List<Agent> listOfAgents;

    public void updateAccessToken(String newAccessToken){
        this.accessToken = newAccessToken;
    }

    @PrePersist
    private void prePersist() {
        crDate = new Date();
    }
}
