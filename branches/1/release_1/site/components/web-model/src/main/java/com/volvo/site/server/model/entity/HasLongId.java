package com.volvo.site.server.model.entity;

public interface HasLongId {
    Long getId();

    void setId(Long id);
}
