package com.volvo.site.server.model.entity;

import com.volvo.site.server.model.ModelConstants;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.UUID.randomUUID;

@Entity
@Table(name = "registration")
@Data
@NoArgsConstructor
public class Registration implements Serializable {

    public static final Registration of(String email, String password) {
        return of(email, password, String.valueOf(randomUUID()));
    }

    public static final Registration of(String email, String password, String uniqueKey) {
        checkNotNull(email);
        checkNotNull(password);
        checkNotNull(uniqueKey);

        Registration registration = new Registration();
        registration.setEmail(email);
        registration.setPassword(password);
        registration.setUniqueKey(uniqueKey);
        return registration;
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 35, nullable = false)
    @Email
    @NotNull
    @Size(max= ModelConstants.emailMaxLength)
    private String email;

    @Column(length = 65, nullable = false)
    @NotNull
    @Size(min = 5, max = 60)
    private String password;

    @Column(nullable = false)
    @Setter(AccessLevel.NONE)
    private Date crDate;

    @Enumerated(EnumType.STRING)
    @NotNull
    private RegistrationStatus status;

    @Column(length = 45, nullable = false)
    @NotNull
    @Size(min = 10)
    private String uniqueKey;

    @PrePersist
    private void prePersist() {
        resetCreationDate();
        status = RegistrationStatus.PENDING;
    }

    @PreUpdate
    private void preUpdate() {
        resetCreationDate();
    }

    public void resetCreationDate() {
        crDate = new Date();
    }

    public enum RegistrationStatus {
        PENDING,
        CONFIRMED,
        TIMED_OUT
    }
}
