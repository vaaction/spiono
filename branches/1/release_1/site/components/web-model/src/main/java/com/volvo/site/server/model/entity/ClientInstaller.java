package com.volvo.site.server.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.volvo.platform.java.JavaUtils.newArrayListIfNull;

@Entity
@Table(name=ClientInstaller.TABLE_NAME)
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class ClientInstaller implements Serializable, Installer {

    public static final String TABLE_NAME = "client_installer";

    @Id
    @GeneratedValue
    private Long id;

    public static ClientInstaller of(byte[] data) {
        ClientInstaller result = new ClientInstaller();
        result.setData(data);
        return result;
    }

    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false)
    private byte[] data;

    @Column(nullable = false, insertable = false)
    @Setter(AccessLevel.NONE)
    private Date crDate;

    @Column(nullable = false)
    private boolean active;

    @Column(nullable = false)
    private boolean downloaded;

    @OneToMany(targetEntity = AgentInstaller.class, fetch = FetchType.LAZY, mappedBy = "clientInstaller")
    @Setter(AccessLevel.NONE)
    private List<AgentInstaller> agentInstallers;

    public boolean addAgentInstaller(AgentInstaller agentInstaller) {
        checkNotNull(agentInstaller);
        return getAgentInstallers().add(agentInstaller);
    }

    public List<AgentInstaller> getAgentInstallers() {
        return agentInstallers = newArrayListIfNull(agentInstallers);
    }

    public boolean isActiveOrDownloaded() {
        return isActive() || isDownloaded();
    }

    @PrePersist
    private void prePersist() {
        crDate = new Date();
    }
}
