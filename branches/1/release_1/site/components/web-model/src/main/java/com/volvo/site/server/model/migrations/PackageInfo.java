package com.volvo.site.server.model.migrations;


import static org.springframework.util.ClassUtils.classPackageAsResourcePath;
import static org.springframework.util.ClassUtils.getPackageName;

public final class PackageInfo {

    public static final String PACKAGE_NAME = getPackageName(PackageInfo.class);

    public static final String PACKAGE_PATH = classPackageAsResourcePath(PackageInfo.class);
}
