
CREATE TABLE `registration` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `email` VARCHAR (35) NOT NULL,
    `password` VARCHAR (65) NOT NULL,
    `cr_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`unique_key` VARCHAR (50) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `unique_key` (`unique_key`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;