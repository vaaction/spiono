package com.volvo.site.server.model.entity;


import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import org.apache.solr.analysis.LowerCaseFilterFactory;
import org.apache.solr.analysis.SnowballPorterFilterFactory;
import org.apache.solr.analysis.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import static com.volvo.site.server.model.ModelConstants.spyItemMaxTextLen;
import static com.volvo.site.server.model.ModelConstants.spyItemMaxWindowTextLen;
import static org.hibernate.search.annotations.Index.TOKENIZED;


@Entity
@Data
@Indexed
@AnalyzerDefs( value = {
		@AnalyzerDef(name = "default_analyzer",
				tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
				filters = {
						@TokenFilterDef(factory = LowerCaseFilterFactory.class),
						@TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
								@Parameter(name = "language", value = "English")
						})
				})
}
)
@EqualsAndHashCode(exclude = {"agentId", "text", "startDate", "endDate", "hwnd", "processName"})
public class KeyLog implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "agent_id", nullable = false)
	@Field(index = TOKENIZED,store = Store.NO)
    private long agentId;

    @Column(nullable = false)
    @NotNull
    @NotEmpty
    @Size(max = spyItemMaxTextLen)
    @Field(index = TOKENIZED,store = Store.NO)
	@Analyzer( definition = "default_analyzer")
    private String text;

    @Column(nullable = false)
    @NotNull
	@Field(index = TOKENIZED,store = Store.NO)
    @DateBridge(resolution = Resolution.MINUTE)
    private Date startDate;

    @Column(nullable = false)
    @NotNull
	@Field(index = TOKENIZED,store = Store.NO)
    @DateBridge(resolution = Resolution.MINUTE)
    private Date endDate;

    @Column(nullable = false)
    private long hwnd;

    @Column(nullable = true)
    @Size(max = spyItemMaxWindowTextLen)
	@Field(index = TOKENIZED,store = Store.NO)
	@Analyzer( definition = "default_analyzer")
    private String windowText;

    @Column(nullable = true)
    @Size(max = spyItemMaxWindowTextLen)
    @Field(index = TOKENIZED,store = Store.NO)
    @Analyzer( definition = "default_analyzer")
    private String processName;

    @ManyToOne(targetEntity = Agent.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", insertable = false, updatable = false)
    @Setter(AccessLevel.NONE)
    private Agent agent;
}
