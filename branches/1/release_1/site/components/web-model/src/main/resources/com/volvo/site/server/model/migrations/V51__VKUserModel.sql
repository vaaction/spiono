CREATE TABLE vk_users (
	id INT(11) NOT NULL AUTO_INCREMENT,
	user_id BIGINT(20) NOT NULL,
	expires_in INT(10) NOT NULL,
	access_token VARCHAR(100) NOT NULL,
	cr_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	user_name VARCHAR(100) NULL DEFAULT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX user_id (user_id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `agent`
	ADD COLUMN `vk_user` INT(11) NULL DEFAULT NULL AFTER `name`;

ALTER TABLE `agent`
	ADD CONSTRAINT `agent_vk_user` FOREIGN KEY (`vk_user`) REFERENCES `vk_users` (`id`);

ALTER TABLE `agent`
	ALTER `user` DROP DEFAULT;
ALTER TABLE `agent`
	CHANGE COLUMN `user` `user` INT(11) NULL AFTER `vk_user`;
ALTER TABLE `agent`
	CHANGE COLUMN `user` `user` INT(11) NULL DEFAULT NULL AFTER `vk_user`;
