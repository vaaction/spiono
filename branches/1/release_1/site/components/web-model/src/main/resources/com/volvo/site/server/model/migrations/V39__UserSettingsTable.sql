CREATE TABLE `user_settings` (
	`user_id` INT(11) NOT NULL,
	`filter_quick_search` VARCHAR(50) NULL,
	`filter_agent_status` VARCHAR(20) NULL,
	PRIMARY KEY (`user_id`),
	CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
