ALTER TABLE `agent_installer`
	ADD COLUMN `client_installer` INT(11) NOT NULL AFTER `active`,
	ADD CONSTRAINT `client_installer_fk` FOREIGN KEY (`client_installer`) REFERENCES `client_installer` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE;
