package com.volvo.site.gwt.usercabinet.client.agents;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.view.client.AbstractDataProvider;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;
import com.volvo.site.gwt.usercabinet.shared.enums.StatusFilter;

import java.util.List;

public interface AgentsView extends IsWidget {
	void setPresenter(AgentsPresenter presenter);
	void scheduleCheckAgentRegisteredTimer();
	void scheduleFirstAgentInstalledTimer();
    void onStop();
    void redrawTable();

    StatusFilter getStatusFilter();
    String getSearchString();
	String getNewAgentName();
	void setFilterFields(String searchingString, StatusFilter statusFilter);
	void setRowData(int start, int totalCount, List<AgentDTO> data);
	void assignDataProvider(int pageSize, AbstractDataProvider<AgentDTO> data);
	void setAgentsTableElementsVisible(boolean isVisible);
	void setInstallFirstAgentPlaceVisible(boolean isVisible);
	void setDownloadAgentLink(String downloadAgentLink);
	void showHelpBlock(String helpBlockCaption);
	void clearHelpBlock();

	public interface AgentsPresenter {
        void deleteAgent(long agentId);
        void setAgentOnPause(boolean onPause, long agentId);
        void downloadAgent(long agentId, String clientKey);
        void onCreateNewAgent(String name);
        void onRenameAgent(long id, String newName);
        void refreshTable();
        void saveUserSettings();
		void downloadFirstAgent();
	}
}
