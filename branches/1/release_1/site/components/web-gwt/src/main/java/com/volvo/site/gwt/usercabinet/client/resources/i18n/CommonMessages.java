package com.volvo.site.gwt.usercabinet.client.resources.i18n;

import com.google.gwt.i18n.client.Messages;

public interface CommonMessages extends Messages {

	//Common
	String infoPasswordsDoNotMatch();
	String infoPasswordsChanged();
	String infoPasswordIncorrect();
	String infoPasswordsEqual();
	String infoServerCallFailure();
	String infoMatchPasswordToShort();
	String infoPasswordsMatch();
	String infoPasswordToShort();

	String errServerCallTitle();

	//AgentsView
	String agentStatusNotRegistered();
	String agentStatusOnline();
	String agentStatusOffline();
	String agentStatusOnPause();
	String agentStatusDeleted();
	String agentStatusLinkAllAgents();
	String agentStatusLinkOnlyRegistered();
	String agentStatusLinkOnlyNotRegistered();
	String agentStatusLinkOnlyDeleted();
	String btnCancel();
	String btnDownload();
    String btnNext();
	String btnOthersShow();
	String btnOthersHide();
	String btnNewAgent();
	String btnRefresh();
	String btnSave();
	String btnSaveChanges();
	String headerLabelAgents();
	String helpBlockEmptyName();
	String lblCurrentPassword();
	String lblNewPassword();
	String lblReNewPassword();
	String linkViewLogs();
	String linkViewStatistics();
	String loadingTextSaving();
	String modalChangePassword();
	String msgRenameAgentTitle();
	String msgCreateAgentTitle();
	String navLinkChangePassword();
	String navLinkLogout();
	String placeHolderQuickSearch();
	String placeHolderSetAgentName();
	String tabAgents();
	String tabLogs();
	String tabStatistics();
	String tblHeaderStatus();
	String tblHeaderAgentName();
	String tblHeaderActions();
	String tooltipTextDownload();
	String tooltipTextEdit();
	String tooltipTextPause();
	String tooltipTextRemove();
	String tooltipTextStart();
	String tooltipTextSearch();
	String tooltipAgentStatusNotRegistered();
	String tooltipAgentStatusOnline();
	String tooltipAgentStatusOffline();
	String tooltipAgentStatusOnpause();
	String tooltipAgentStatusDeleted();

	String headerLabelLogs();
	String headerLabelStatistic();
	String optionLabelStatus();
	String optionLabelAgent();
	String optionLabelSearchByDate();
	String optionLabelSearchByKeywords();
	String optionBtnSearch();
	String showStatisticsButton();
	String optionHolderPlaceSearchByPhrase();
	String optionTooltipClearSearch();
	String statisticOptionLabelForPeriod();
	String subHeaderOptions();
	String subHeaderResults();
    String noResultBySearch();
    String noLogs();
	String noStatistics();

	String loadingTextLoading();
	String refreshing();
	String searching();
	String loadingTextInstalling();
    String readMore();
    String clickHere();

	String helpBlockFirstAgentInstalled();
	String agentsViewFirstAgentInstallationPlaceHeader();
	String agentsViewFirstAgentInstallationPlaceTitle1();
	String agentsViewFirstAgentInstallationPlaceTitle2();
    String userCabinetText();
}
