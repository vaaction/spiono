package com.volvo.site.gwt.usercabinet.client.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

import java.util.Iterator;

public class PopoverPanel extends Composite implements HasWidgets {
    interface PopoverPanelUiBinder extends UiBinder<HTMLPanel, PopoverPanel> {
    }

    private static PopoverPanelUiBinder ourUiBinder = GWT.create(PopoverPanelUiBinder.class);

    @UiField
    HTMLPanel content;

    @UiField
    HeadingElement title;

    public PopoverPanel() {
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    @Override
    public void setTitle(String title) {
        this.title.setInnerText(title);
    }

    public String getTitle() {
        return this.title.getTitle();
    }

    public void add(Widget widget, Element elem) {
        content.add(widget, elem);
    }

    public void add(Widget widget, String id) {
        content.add(widget, id);
    }

    @Override
    public void add(Widget w) {
        content.add(w);
    }

    @Override
    public void clear() {
        content.clear();
    }

    @Override
    public Iterator<Widget> iterator() {
        return content.iterator();
    }

    @Override
    public boolean remove(Widget w) {
        return content.remove(w);
    }
}