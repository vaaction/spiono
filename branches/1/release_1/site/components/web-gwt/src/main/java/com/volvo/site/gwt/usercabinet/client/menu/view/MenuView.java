package com.volvo.site.gwt.usercabinet.client.menu.view;

import com.google.gwt.user.client.ui.IsWidget;

public interface MenuView extends IsWidget {

    void setPresenter(MenuPresenter presenter);
	void setTabsVisible(boolean visible);

    public interface MenuPresenter {
        void onChangePasswordClicked();
        void setLogsTabVisible(boolean visible);
    }
}
