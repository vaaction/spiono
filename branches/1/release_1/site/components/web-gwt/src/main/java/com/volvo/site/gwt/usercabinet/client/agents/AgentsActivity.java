package com.volvo.site.gwt.usercabinet.client.agents;


import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.volvo.platform.gwt.client.http.DummyHttpRequestCallback;
import com.volvo.platform.gwt.client.http.HttpRequestCallback;
import com.volvo.platform.gwt.client.http.HttpRequestSuccessCallback;
import com.volvo.platform.gwt.client.util.GwtUtil;
import com.volvo.site.gwt.usercabinet.client.mvp.activity.BaseActivity;
import com.volvo.site.gwt.usercabinet.client.mvp.activity.HasAgentName;
import com.volvo.site.gwt.usercabinet.client.util.AgentUtil;
import com.volvo.site.gwt.usercabinet.shared.dto.*;
import com.volvo.site.gwt.usercabinet.shared.enums.AgentStatus;
import com.volvo.site.gwt.usercabinet.shared.enums.StatusFilter;

import javax.inject.Inject;

import static com.volvo.platform.gwt.client.http.HttpUtils.responseAsAutoBean;
import static com.volvo.platform.gwt.client.util.StrUtil.nullOrEmpty;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.*;
import static com.volvo.site.gwt.usercabinet.client.util.AgentUtil.*;
import static com.volvo.site.gwt.usercabinet.client.util.HttpRequestCallbackBuilder.newCallbackWithDefaultErrorHandler;

public class AgentsActivity extends BaseActivity implements AgentsView.AgentsPresenter, HasAgentName {

	private final AgentsView view;
	private static final int PAGE_SIZE = 10;
	private HandlerRegistration handlerRegistration;
	private CreateAgentResultDTO downloadAgentAfterRefresh;

	private final AgentsDataProvider dataProvider = new AgentsDataProvider();

	@Inject
	public AgentsActivity(AgentsView view) {
		this.view = view;
	}

	@Override
	public void start(AcceptsOneWidget container, EventBus eventBus) {
		view.setPresenter(this);
		container.setWidget(view.asWidget());
		view.scheduleCheckAgentRegisteredTimer();
		handlerRegistration = GwtUtil.callActivityOnStopWhenOnPlaceChangeEvent(this, eventBus);
		view.assignDataProvider(PAGE_SIZE, dataProvider);
		setupUserSettings();
		view.setAgentsTableElementsVisible(false);
	}

	@Override
	public void onStop() {
		view.onStop();
		handlerRegistration.removeHandler();
	}

	@Override
	public void onCreateNewAgent(String name) {
		AutoBean<CreateAgentDTO> createAgent = getBeanFactory().createAgentDTO();
		createAgent.as().setName(name);
		HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
			@Override
			public void onSuccess(Request request, Response response) {
				downloadAgentAfterRefresh = responseAsAutoBean(response, CreateAgentResultDTO.class, getBeanFactory());
				refreshTable();
			}
		});
		getCommandDispatcher().dispatchCommand(createAgent, callback);
	}

	@Override
	public void onRenameAgent(long id, String newName) {
		AgentUtil.setAgentName(this, newName, id);
	}

	@Override
	public void refreshTable() {
		dataProvider.update();
	}

	@Override
	public void saveUserSettings(){
		AutoBean<UpdateAgentsViewStartupInfoDTO> updateRequest = getBeanFactory().updateAgentsViewStartupInfoDTO();
		updateRequest.as().setLastAgentsSearchPhrase(view.getSearchString());
		updateRequest.as().setLastAgentsSearchAgentStatus(view.getStatusFilter());
		HttpRequestCallback updateCallback = newCallbackWithDefaultErrorHandler(new DummyHttpRequestCallback());
		getCommandDispatcher().dispatchCommand(updateRequest, updateCallback);
	}

	@Override
	public void downloadFirstAgent(){
		AutoBean<CreateAgentDTO> createAgent = getBeanFactory().createAgentDTO();
		createAgent.as().setName(view.getNewAgentName());
		HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
			@Override
			public void onSuccess(Request request, Response response) {
				startInstallingAgent(responseAsAutoBean(response, CreateAgentResultDTO.class, getBeanFactory()));
			}
		});
		getCommandDispatcher().dispatchCommand(createAgent, callback);
	}

	private void startInstallingAgent(final CreateAgentResultDTO agent) {
		AutoBean<UpdateLogsViewStartupInfoDTO> logsInfo = getBeanFactory().updateLogsViewStartupInfoDTO();
		logsInfo.as().setLastLogsSearchAgentId(agent.getAgentId());

		HttpRequestCallback updateLogsCallback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
			@Override
			public void onSuccess(Request request, Response response) {
				String downloadAgentLink = AgentUtil.getAgentDownloadLink(agent.getAgentId(), agent.getDownloadKey());
				view.setDownloadAgentLink(downloadAgentLink);
				AgentUtil.assignToDownloadAgent(downloadAgentLink);
				view.scheduleFirstAgentInstalledTimer();
				view.setInstallFirstAgentPlaceVisible(false);
				getClientFactory().menuActivity().setLogsTabVisible(true);
			}
		});

		updateSelectedAgentId(agent.getAgentId());
		getCommandDispatcher().dispatchCommand(logsInfo, updateLogsCallback);

	}

	@Override
	public void onAgentNameAssigned() {
		view.redrawTable();
	}

	@Override
	public void deleteAgent(long agentId) {
		AutoBean<DeleteAgentDTO> agent = getBeanFactory().deleteAgentDTO();
		agent.as().setAgentId(agentId);

		HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
			@Override
			public void onSuccess(Request request, Response response) {
				refreshTable();
			}
		});

		getCommandDispatcher().dispatchCommand(agent, callback);
	}

	@Override
	public void setAgentOnPause(boolean onPause, long agentId) {
		AutoBean<SetAgentOnPauseDTO> agent = getBeanFactory().setAgentOnPauseDTO();
		agent.as().setAgentId(agentId);
		agent.as().setIsOnPause(onPause);

		HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
			@Override
			public void onSuccess(Request request, Response response) {
				refreshTable();
			}
		});

		getCommandDispatcher().dispatchCommand(agent, callback);
	}

	@Override
	public void downloadAgent(long agentId, String clientKey) {
		assignToDownloadAgent(getAgentDownloadLink(agentId, clientKey));
	}

	private void setupUserSettings(){
		AutoBean<GetStartupInformationDTO> request = getBeanFactory().getStartupInformation();
		HttpRequestCallback updateCallback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
			@Override
			public void onSuccess(Request request, Response response) {
				StartupInformationDTO setup = responseAsAutoBean(response, StartupInformationDTO.class, getBeanFactory());
				view.setFilterFields(setup.getLastAgentsSearchPhrase(), setup.getLastAgentsSearchAgentStatus());
			}
		});
		getCommandDispatcher().dispatchCommand(request, updateCallback);
	}

	private class AgentsDataProvider extends AsyncDataProvider<AgentDTO> {
		public void update() {
			for (HasData<AgentDTO> display: getDataDisplays()) {
				onRangeChanged(display);
			}
		}

		@Override
		protected void onRangeChanged(HasData<AgentDTO> display) {
			final Range range = display.getVisibleRange();
			AutoBean<GetAgentsDTO> requestData = createRequestData(range);
			HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
				@Override
				public void onSuccess(Request request, Response response) {
					handleResponse(response, range);
				}
			});
			getCommandDispatcher().dispatchCommand(requestData, callback);
		}

		private AutoBean<GetAgentsDTO> createRequestData(Range range) {
			AutoBean<GetAgentsDTO> agents = getBeanFactory().getAgentsDTO();
			agents.as().setPageNumber(range.getStart() / PAGE_SIZE);
			agents.as().setPageSize(range.getLength());
			agents.as().setStatusFilter(view.getStatusFilter());
			agents.as().setSearchingString(view.getSearchString());
			return agents;
		}

		private void handleResponse(Response response, Range range) {
			PagedAgentsListDTO agentsResultListDTO = responseAsAutoBean(response, PagedAgentsListDTO.class, getBeanFactory());
			int totalCount = (int)agentsResultListDTO.getTotalElements();
			if (isNoAgentsAndNoFilters(totalCount)) {
				view.setInstallFirstAgentPlaceVisible(true);
				getClientFactory().menuActivity().setLogsTabVisible(false);
			}
			else {
				if(totalCount == 1 && agentsResultListDTO.getUserAgents().get(0).getStatus() == AgentStatus.NOT_REGISTERED){
					view.clearHelpBlock();
					view.showHelpBlock(commonMessages.helpBlockFirstAgentInstalled());
				} else {
					view.clearHelpBlock();
				}
				view.setAgentsTableElementsVisible(true);
				view.setRowData(range.getStart(), totalCount, agentsResultListDTO.getUserAgents());
				downloadNewAgentIfRequired();
			}
		}

		private void downloadNewAgentIfRequired() {
			if (downloadAgentAfterRefresh != null) {
				downloadAgent(downloadAgentAfterRefresh.getAgentId(), downloadAgentAfterRefresh.getDownloadKey());
				downloadAgentAfterRefresh = null;
			}
		}

		private boolean isNoAgentsAndNoFilters(int totalCount) {
			return isFiltersDisabled() && totalCount == 0;
		}

		private boolean isFiltersDisabled() {
			return nullOrEmpty(view.getSearchString()) && view.getStatusFilter() == StatusFilter.ALL_AGENTS;
		}
	}
}
