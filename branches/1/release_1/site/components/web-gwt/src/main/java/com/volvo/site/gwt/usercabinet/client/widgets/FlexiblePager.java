package com.volvo.site.gwt.usercabinet.client.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.view.client.HasRows;
import com.google.gwt.view.client.Range;

public class FlexiblePager extends SimplePager {

    private static SimplePager.Resources DEFAULT_RESOURCES;

    private static Resources getDefaultResources() {
        if (DEFAULT_RESOURCES == null) {
            DEFAULT_RESOURCES = GWT.create(SimplePager.Resources.class);
        }
        return DEFAULT_RESOURCES;
    }

    @UiConstructor
    public FlexiblePager(TextLocation location, boolean showFastForwardButton, int fastForwardRows, boolean showLastPageButton) {
        super(location, getDefaultResources(), showFastForwardButton, fastForwardRows, showLastPageButton);
    }

    @Override
    public void setPageStart(int index) {
        HasRows display = getDisplay();
        if (display != null) {
            Range range = display.getVisibleRange();
            int pageSize = range.getLength();
            index = Math.max(0, index);
            if (index != range.getStart()) {
                display.setVisibleRange(index, pageSize);
            }
        }
    }
}
