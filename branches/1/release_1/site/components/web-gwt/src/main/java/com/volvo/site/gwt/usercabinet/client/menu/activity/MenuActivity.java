package com.volvo.site.gwt.usercabinet.client.menu.activity;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.volvo.site.gwt.usercabinet.client.menu.view.MenuView;
import com.volvo.site.gwt.usercabinet.client.mvp.activity.BaseActivity;

import javax.inject.Inject;
import javax.inject.Provider;

public class MenuActivity extends BaseActivity implements MenuView.MenuPresenter {

    private final MenuView view;
    private EventBus eventBus;

    private final Provider<ChangePasswordActivity> changePasswordActivityProvider;

    @Inject
    public MenuActivity(MenuView view, Provider<ChangePasswordActivity> changePasswordActivityProvider) {
        this.view = view;
        this.changePasswordActivityProvider = changePasswordActivityProvider;
    }

    @Override
    public void start(AcceptsOneWidget panel, EventBus eventBus) {
        this.eventBus = eventBus;
        view.setPresenter(this);
        panel.setWidget(view.asWidget());
    }

    @Override
    public void onChangePasswordClicked() {
        changePasswordActivityProvider.get().start(null, eventBus);
    }

    @Override
    public void setLogsTabVisible(boolean visible) {
        view.setTabsVisible(visible);
    }
}
