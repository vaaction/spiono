package com.volvo.site.gwt.usercabinet.client.widgets;

import com.github.gwtbootstrap.client.ui.Hero;
import com.github.gwtbootstrap.client.ui.Label;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.volvo.platform.gwt.client.util.UiUtil;
import com.volvo.site.gwt.usercabinet.client.AppWorkflow;
import com.volvo.site.gwt.usercabinet.client.resources.KeyLogsCss;
import com.volvo.site.gwt.usercabinet.shared.dto.KeyLogDataDTO;

import java.util.Date;

import static com.volvo.platform.gwt.client.util.UiUtil.formatDateMedium;
import static com.volvo.platform.gwt.client.util.UiUtil.formatTimeShort;

public class KeyLogHero extends Hero {

    private static final KeyLogsCss css = AppWorkflow.commonRes.keylogsCss();

    private KeyLogDataDTO lastLog;

    public KeyLogHero(KeyLogDataDTO keyLogData) {
        setStyleName(css.heroStyle());
        addHeadingLabel(keyLogData);
        addLog(keyLogData);
    }

    public boolean canLogBeAppended(KeyLogDataDTO keyLogData) {
        return keyLogData.getHWND() == lastLog.getHWND() &&
                keyLogData.getProcessName().equals(lastLog.getProcessName()) &&
                keyLogData.getWindowText().equals(lastLog.getWindowText()) &&
                formatDateMedium(keyLogData.getStartDate()).equals(formatDateMedium(lastLog.getStartDate()));
    }

    public void addLog(KeyLogDataDTO keyLogData) {
        HTMLPanel panel = new HTMLPanel("");
        if (hasTimeChanged(keyLogData.getStartDate())) {
            panel.add(createTimeLabel(keyLogData.getStartDate()));
        }
        panel.add(createTextLabel(keyLogData.getText()));
        insert(panel, 1);
        this.lastLog = keyLogData;
    }

    private void addHeadingLabel(KeyLogDataDTO keyLog) {
        HTMLPanel panel = new HTMLPanel("");
        panel.add(createDateLabel(keyLog));
        panel.add(createProcessAndWindowNameLabel(keyLog));
        add(panel);
    }

    private boolean hasTimeChanged(Date newDate) {
        if (lastLog == null) {
            return true;
        }
        return !formatTimeShort(lastLog.getStartDate()).equals(formatTimeShort(newDate));
    }

    private static Label createTimeLabel(Date startDate) {
        Label timeLabel =  new Label(formatTimeShort(startDate));
        timeLabel.setStyleName(css.floatRight());
        return timeLabel;
    }

    private static Label createTextLabel(String text) {
        Label textLabel = new Label(text);
        textLabel.setStyleName(css.fontStyleItalic());
        return textLabel;
    }

    private static Label createDateLabel(KeyLogDataDTO keyLog) {
        Label dateLabel = new Label(UiUtil.formatDateMedium(keyLog.getStartDate()));
        dateLabel.setStyleName(css.floatRight());
        return dateLabel;
    }

    private static Label createProcessAndWindowNameLabel(KeyLogDataDTO keyLog) {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(keyLog.getProcessName()).append("] ");
        sb.append("\"").append(keyLog.getWindowText()).append("\"");
        Label processAndWindowNameLabel = new Label(sb.toString());
        processAndWindowNameLabel.setStyleName(css.boldText());
        return processAndWindowNameLabel;
    }
}
