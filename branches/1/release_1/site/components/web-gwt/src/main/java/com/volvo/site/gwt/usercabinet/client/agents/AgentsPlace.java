package com.volvo.site.gwt.usercabinet.client.agents;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class AgentsPlace extends Place {
	private static final String VIEW_HISTORY_TOKEN = "agents";
	
	public AgentsPlace() { }
	
	@Prefix(value = VIEW_HISTORY_TOKEN)
	public static class Tokenizer implements PlaceTokenizer<AgentsPlace> {
		@Override
		public AgentsPlace getPlace(String token) {
			return new AgentsPlace();
		}

		@Override
		public String getToken(AgentsPlace place) {
			return "";
		}
	}
}
