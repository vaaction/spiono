package com.volvo.site.gwt.usercabinet.client.util;

import com.github.gwtbootstrap.client.ui.constants.LabelType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.volvo.platform.gwt.client.http.DummyHttpRequestCallback;
import com.volvo.platform.gwt.client.http.HttpRequestCallback;
import com.volvo.platform.gwt.client.http.HttpRequestSuccessCallback;
import com.volvo.site.gwt.usercabinet.client.mvp.activity.HasAgentName;
import com.volvo.site.gwt.usercabinet.shared.dto.SetAgentNameDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.UpdateStatisticViewStartupInfoDTO;
import com.volvo.site.gwt.usercabinet.shared.enums.AgentStatus;

import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.*;
import static com.volvo.site.gwt.usercabinet.client.util.HttpRequestCallbackBuilder.newCallbackWithDefaultErrorHandler;

public final class AgentUtil {
	public static void setAgentName(final HasAgentName activity, final String agentName, long agentId) {
        AutoBean<SetAgentNameDTO> setAgentName = getBeanFactory().setAgentNameDTO();
        setAgentName.as().setAgentId(agentId);
        setAgentName.as().setAgentName(agentName);

        HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
            @Override
            public void onSuccess(Request request, Response response) {
                if (response != null) {
                    activity.onAgentNameAssigned();
                }
            }
        });

        getCommandDispatcher().dispatchCommand(setAgentName, callback);
    }

    public static void assignToDownloadAgent(String url) {
        Window.Location.assign(GWT.getHostPageBaseURL() + url);
    }

    public static String getAgentDownloadLink(long agentId, String clientKey) {
        return "agent/" + agentId + "/" + clientKey + "/download";
    }

	public static LabelType getLabelTypeByStatus(AgentStatus status){
		switch (status) {
			case NOT_REGISTERED:
				return LabelType.IMPORTANT;
			case ONLINE:
				return LabelType.SUCCESS;
			case OFFLINE:
				return LabelType.WARNING;
			case ON_PAUSE:
				return LabelType.INFO;
			case DELETED:
				return LabelType.DEFAULT;
			default:
				throw new IllegalStateException();
		}
	}

	public static String getLabelTextByStatus(AgentStatus status){
		switch (status) {
			case NOT_REGISTERED:
				return commonMessages.agentStatusNotRegistered();
			case ONLINE:
				return commonMessages.agentStatusOnline();
			case OFFLINE:
				return commonMessages.agentStatusOffline();
			case ON_PAUSE:
				return commonMessages.agentStatusOnPause();
			case DELETED:
				return commonMessages.agentStatusDeleted();
			default:
				throw new IllegalStateException();
		}
	}

	public static String getTooltipTextByStatus(AgentStatus status){
		switch (status) {
			case NOT_REGISTERED:
				return commonMessages.tooltipAgentStatusNotRegistered();
			case ONLINE:
				return commonMessages.tooltipAgentStatusOnline();
			case OFFLINE:
				return commonMessages.tooltipAgentStatusOffline();
			case ON_PAUSE:
				return commonMessages.tooltipAgentStatusOnpause();
			case DELETED:
				return commonMessages.tooltipAgentStatusDeleted();
			default:
				throw new IllegalStateException();
		}
	}

	public static void updateSelectedAgentId(long agentId){
		AutoBean<UpdateStatisticViewStartupInfoDTO> request = getBeanFactory().updateStatisticViewStartupInfoDTO();
		request.as().setLastStatAgentId(agentId);
		HttpRequestCallback updateCallback = newCallbackWithDefaultErrorHandler(new DummyHttpRequestCallback());
		getCommandDispatcher().dispatchCommand(request, updateCallback);
	}
}
