package com.volvo.site.gwt.usercabinet.client.resources;

import com.google.gwt.resources.client.CssResource;

public interface KeyLogsCss extends CssResource {
    String heroStyle();
    String floatRight();
    String boldText();
    String fontStyleItalic();
}
