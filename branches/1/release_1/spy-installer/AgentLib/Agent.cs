﻿

using AgentLib.Spy;
using Shared;
using Shared.Logging;

namespace AgentLib
{
    public class Agent
    {
        private static readonly ILogger Logger = LogFactory.GetLogger(typeof (Agent));

        private const string VERSION = "0.035";

        private readonly SpyController _controller = new SpyController();

        public bool Run()
        {
            Logger.Entering("Run");
            Logger.Info("Run", "Version: {0}; IsShowingErrorsMode: {1}", VERSION, AppEnvironment.IsShowingErrorsMode);

            return _controller.Start();
        }

        public void Unload()
        {
            _controller.Stop();
            Logger.Info("Unload", "Agent unloaded. Version: {0}", VERSION);
        }
    }
}
