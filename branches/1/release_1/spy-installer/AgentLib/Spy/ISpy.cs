﻿using System;


namespace AgentLib.Spy
{
    interface ISpy : IDisposable
    {
        void Start();
    }
}
