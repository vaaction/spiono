﻿

using Shared.Logging;
using Shared;

namespace AgentLib.Spy
{
    public class SpyController
    {
        private static readonly ILogger Logger = LogFactory.GetLogger(typeof (SpyController));

        private static readonly ILogger SpyFormLogger = LogFactory.GetLogger(typeof (SpyForm));

        private SpyForm _form;

        private volatile bool _isUninstall;

        public bool Start()
        {
            Logger.Entering("Start()");

            _form = new SpyForm();
            var spy = new Spy(_form.UiThreadControl);
            spy.OnAppUninstall += StopAndUninstall;

            _form.OnStartSpyServices += () =>
            {
                spy.Start();
                SpyFormLogger.Info("OnStartSpyServices", "Agent form was loaded");
            };

            _form.OnStopSpyServices += () =>
            {
                spy.DisposeQuitely();
                spy = null;
                SpyFormLogger.Info("OnStopSpyServices", "Agent form was unloaded");
            };
            _form.StartSpyForm();

            return !_isUninstall;
        }

        private void StopAndUninstall()
        {
            _isUninstall = true;
            Stop();
        }

        public void Stop()
        {
            Logger.Entering("Stop()");

            _form.StopSpyForm();
        }
    }
}
