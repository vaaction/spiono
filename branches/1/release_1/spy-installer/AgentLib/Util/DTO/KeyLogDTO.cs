﻿using System.Collections.Generic;

namespace AgentLib.Util.DTO
{
    public class KeyLogDTO
    {
        public IList<KeyLogDataDTO> SpyData { get; set; }

        public string Certificate;
    }
}
