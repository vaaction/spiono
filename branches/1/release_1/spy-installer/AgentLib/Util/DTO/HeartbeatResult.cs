﻿

namespace AgentLib.Util.DTO
{
    public enum HeartbeatResult
    {

        Ok, Pause, Uninstall
    }
}
