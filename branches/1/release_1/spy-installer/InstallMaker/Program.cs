﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Build.BuildEngine;

namespace InstallMaker
{
    class Program
    {
#if DEBUG
        private const string ConfigurationMode = "Debug";
#else
        private const string ConfigurationMode = "Release";
#endif

        private const string WorkSpacePath = @"D:\projects\gsp\notNet\";

        [STAThread]
        static void Main(string[] args)
        {
            new Program().Run();
        }

        private readonly Engine _engine = new Engine();

        private void Run()
        {
            try
            {
                _engine.RegisterLogger(new ConsoleLogger());
                BuildAll();
            }   
            finally
            {
                _engine.Shutdown();
                Console.ReadLine();
            }
        }

        private void BuildAll()
        {
            BuildProject(WorkSpacePath + @"Shared\Shared.csproj");
            BuildProject(WorkSpacePath + @"spyloader\Installer.csproj");
            BuildProject(WorkSpacePath + @"InstallLauncher\InstallLauncher.csproj");

            var sharedProjectBytes = GetProjectOutput("Shared", "Shared.dll");
            var installerProjectBytes = GetProjectOutput("spyloader", "msctrlsysadm.exe");
            var installLauncherBytes = GetProjectOutput("InstallLauncher", "InstallLauncher.exe");

            var installerDictionary = new Dictionary<string, object>();
            installerDictionary.Add("Shared", sharedProjectBytes);
            installerDictionary.Add("Installer", installerProjectBytes);

            byte[] serializedProperties;
            using (var stream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(stream, installerDictionary);
                serializedProperties = stream.ToArray();
            }

            using (var fileStream = new FileStream(GetBinFileName("InstallLauncher", "ClientInstaller.exe"), FileMode.Create))
            {
                var binaryWriter = new BinaryWriter(fileStream);
                binaryWriter.Write(installLauncherBytes);
                binaryWriter.Write(serializedProperties);
                binaryWriter.Write(serializedProperties.Length);
            }
        }

        private void BuildProject(String pathToProject)
        {
            var project = new Project(_engine);
            project.Load(pathToProject);
            project.SetProperty("Configuration", ConfigurationMode);
            if (!project.Build())
            {
                throw new Exception("Project Build Failed");
            }    
        }

        private byte[] GetProjectOutput(string projectPath, string executableName)
        {
            return File.ReadAllBytes(GetBinFileName(projectPath, executableName));
        }

        private string GetBinFileName(string projectPath, string executableName)
        {
            return WorkSpacePath + projectPath + @"\bin\" + ConfigurationMode + @"\" + executableName;
        }
    }
}
