﻿
using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using Shared.DTO;
using Shared.Logging;
using Shared.Util;

namespace Shared
{
    public static class WebUtils
    {
        private const int MaxErrorLength = 2000;

        private static readonly ILogger Logger = LogFactory.GetLogger(typeof (WebUtils));

        private static readonly FileLoggerEventHandler FileLogger =
            FileLoggerEventHandler.GetHandler(AppEnvironment.LogFilePath);

        public static void ReportError(string error)
        {
            Logger.Entering("ReportError");
            try
            {
                Logger.Info("ReportError", "Error: {0};", error);

                var errorData = new ErrorDTO
                                    {
                                        Certificate = AppEnvironment.Certificate,
                                        InstallerId = AppEnvironment.InstallerId,
                                        IsShowingErrorsMode = AppEnvironment.IsShowingErrorsMode,
                                        Message = error.Crop(MaxErrorLength),
                                        LogFileTail = FileLogger.GetLogTail().Crop(MaxErrorLength)
                                    };
                PostJson(AppEnvironment.InstallerServiceUrl + "reportError/", errorData);
                
            }
            catch(Exception e)
            {
                Logger.Warn("ReportError", e);
            }
        }

        public static TResult PostAndGetJsonWithCertificate<TResult>(string url, CertificatedDTO data)
        {
            data.Certificate = AppEnvironment.Certificate;
            return PostAndGetJson<TResult>(url, data);
        }

        public static T PostAndGetJson<T>(string url, object data)
        {
            var result = PostJson(url, data);
            return new JavaScriptSerializer().Deserialize<T>(result);
        }

        public static string PostJson(string url, object data)
        {
            return PostJson(url, new JavaScriptSerializer().Serialize(data));
        }

        public static string PostJson(string url, string data)
        {
            var request = WebRequest.Create(AppEnvironment.EndPoint + url);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = "POST";
            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(data);
            }
            var stream = request.GetResponse().GetResponseStream();
            if (stream == null)
            {
                return string.Empty;
            }
            var json = string.Empty;
            using (var reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    json += reader.ReadLine();
                }
            }
            return json;
        }
    }
}
