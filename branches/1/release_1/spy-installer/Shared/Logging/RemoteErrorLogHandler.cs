﻿
namespace Shared.Logging
{
    public static class RemoteErrorLogHandler
    {
        public static void LoggerEventHandler(ILogMessage logMessage)
        {
            if (logMessage.Level == LogLevel.Error)
            {
                WebUtils.ReportError(logMessage.FormatMessage());
            }
        }
    }
}
