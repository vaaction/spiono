﻿

namespace Shared.Logging
{
    public interface ILoggerEventHandler<in TKey>
    {
        void InitLogHandler(TKey key);
    }
}
