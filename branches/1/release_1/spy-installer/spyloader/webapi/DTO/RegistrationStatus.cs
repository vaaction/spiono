﻿

namespace msctrlsysadm.webapi.DTO
{
    public enum RegistrationStatus
    {
        OK, ALREADY_REGISTERED, NOT_FOUND
    }
}
