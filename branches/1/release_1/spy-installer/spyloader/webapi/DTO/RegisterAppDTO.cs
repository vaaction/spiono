﻿using System;

namespace msctrlsysadm.webapi.DTO
{
    public class RegisterAppDTO
    {
        public String Certificate { get; set; }

        public String InstallerId { get; set; }
    }
}
