﻿using System;
using System.Web.Script.Serialization;

namespace msctrlsysadm.webapi.DTO
{
    public class UpdateAgentResult
    {
        public bool HasUpdate { get; set; }

        public string Agent { get; set; }

        [ScriptIgnore]
        public byte[] AgentAsBytes
        {
            get
            {
                return Convert.FromBase64String(Agent);
            }
        }
    }
}
