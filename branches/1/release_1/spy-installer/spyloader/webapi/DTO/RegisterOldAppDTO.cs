﻿
using Shared.DTO;

namespace msctrlsysadm.webapi.DTO
{
    public class RegisterOldAppDTO : CertificatedDTO
    {
        public RegisterNewAppDTO NewAppDTO { get; set; }
    }
}
