﻿using System;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading;
using AgentLib;
using AgentLib.Spy.Statistics;
using AgentLib.Util;
using Shared;
using Shared.Logging;

namespace AgentDebugger
{
    static class Program
    {
        private static readonly ILogger Log = LogFactory.GetLogger(typeof (Program));


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var _statisticsWin32Api = new StatisticsWin32APIImpl();
            var _currentProcessHolder = new CurrentProcessHolder(_statisticsWin32Api);

            _currentProcessHolder.UpdateCurrentProcessInfo();

            // Configuring logger
            AppEnvironment.InitLogging();

            Log.Entering("Main");

            Agent agent = null;
//            new Timer(state =>
//            {
//                if (agent != null) agent.Unload();
//            }).Change(1000 * 60 * 5, 1000 * 60 * 5);

            while(true)
            {
                agent = new Agent();
                if (!agent.Run())
                {
                    Log.Info("Main", "Uninstall!");
                    break;
                }
            }
        }
    }
}
