﻿
using System.Windows.Forms;

namespace Shared.Util
{
    public static class MsgBox
    {
        public static void ShowIfDebug(string message)
        {
#if DEBUG
            MessageBox.Show(message);
#endif
        }
    }
}
