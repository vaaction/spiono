﻿

using System.Collections.Generic;
using System.IO;
using System.Text;


namespace Shared.Logging
{
    public class FileLoggerEventHandler : MultitonLoggerEventHandler<string, FileLoggerEventHandler>
    {
        private const int LogTailMaxSize = 50;

        private object _locker = new object();

        private LinkedList<string> _logTail = new LinkedList<string>();

        private string _fileName;

        public override void InitLogHandler(string fileName)
        {
            _fileName = fileName;
        }

        public string GetLogTail()
        {
            var sb = new StringBuilder();
            lock (_locker)
            {
                foreach (var str in _logTail)
                {
                    sb.AppendLine(str);
                }    
            }
            return sb.ToString();
        }

        public void LoggerEventHandler(ILogMessage logMessage)
        {
            var message = logMessage.FormatMessage();
            lock (_locker)
            {
                AppendQuitely(message);

                _logTail.AddLast(message);
                while (_logTail.Count > LogTailMaxSize) _logTail.RemoveFirst();
            }
        }

        private void AppendQuitely(string message)
        {
            try
            {
                File.AppendAllText(_fileName, "\r\n" + message);
            }
            catch
            {
                // nothing to do here
            }
        }
    }
}
