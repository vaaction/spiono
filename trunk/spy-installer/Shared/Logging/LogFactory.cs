﻿

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Shared.Logging
{
    public static class LogFactory
    {
        private static readonly object Mutex = new object();

        private static readonly Dictionary<Type, WeakReference> Loggers = new Dictionary<Type, WeakReference>();

        private static event OnLogEvent _onLogEvent;

        public static event OnLogEvent OnLogEvent { add { _onLogEvent += value; } remove { _onLogEvent -= value; } }

        public static ILogger GetLogger(object clazz)
        {
            return GetLogger(clazz.GetType());
        }

        public static ILogger GetLogger(Type type)
        {
            lock (Mutex)
            {
                WeakReference loggerReference;
                Loggers.TryGetValue(type, out loggerReference);
                if (loggerReference != null && loggerReference.IsAlive)
                {
                    return loggerReference.Target as ILogger;
                }
                var newLogger = new Logger(type);
                newLogger.OnLog += OnLog;
                Loggers.Add(type, new WeakReference(newLogger));
                return newLogger;
            }
        }

        public static void DebugLoggerEventHandler(ILogMessage logMessage)
        {
#if DEBUG
            Debug.WriteLine(logMessage.FormatMessage());
#endif
        }

        private static void OnLog(ILogMessage logMessage)
        {
            if (_onLogEvent != null)
            {
                _onLogEvent(logMessage);
            }
        }
    }
}
