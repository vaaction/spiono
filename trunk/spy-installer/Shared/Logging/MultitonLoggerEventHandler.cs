﻿

using System.Collections.Generic;

namespace Shared.Logging
{
    public abstract class MultitonLoggerEventHandler<TKey, TValue> : ILoggerEventHandler<TKey> where TValue : ILoggerEventHandler<TKey>, new()
    {
        private static readonly object Locker = new object();

        private static readonly Dictionary<TKey, TValue> Loggers = new Dictionary<TKey, TValue>();

        public static TValue GetHandler(TKey key)
        {
            TValue existingItem;
            lock (Locker)
            {
                if (!Loggers.TryGetValue(key, out existingItem))
                {
                    existingItem = new TValue();
                    existingItem.InitLogHandler(key);
                    Loggers.Add(key, existingItem);
                }
            }
            return existingItem;
        }

        public abstract void InitLogHandler(TKey key);
    }
}
