using System;

namespace Shared.DTO
{
    public class CertificatedDTO
    {
        public String Certificate { get; set; }
    }
}
