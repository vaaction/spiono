﻿

namespace AgentLib.Util.DTO
{
    public class StatisticsDataDTO
    {
        public long StartDate { get; set; }
        public long EndDate { get; set; }
        public string ProcessName { get; set; }
        public string ProcessDescription { get; set; } 
    }
}
