﻿

namespace AgentLib.Util.DTO
{
// ReSharper disable InconsistentNaming
    public class KeyLogDataDTO
// ReSharper restore InconsistentNaming
    {
        public string Text { get; set; }

        public long StartDate { get; set; }

        public long EndDate { get; set; }

// ReSharper disable InconsistentNaming
        public int HWND { get; set; }
// ReSharper restore InconsistentNaming

        public string WindowText { get; set; }

        public string ProcessName { get; set; }
    }
}
