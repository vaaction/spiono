﻿
using System.Collections.Generic;
using Shared.DTO;

namespace AgentLib.Util.DTO
{
    public class CertificatedLogDTO : CertificatedDTO
    {

        public IList<SpyLogDataDTO> SpyData { get; set; }
    }
}
