﻿

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace AgentLib.Util
{
    public static class Win32Api
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr GetFocus();

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int GetWindowThreadProcessId(int hWnd, int lpdwProcessId);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int GetCurrentThreadId();

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int AttachThreadInput(int currentForegroundThread, int makeThisThreadForegrouond, bool boolAttach);

        [DllImport("user32.dll")]
        public static extern IntPtr GetKeyboardLayout(int idThread);

        [DllImport("user32.dll")]
        public static extern int ToUnicodeEx(int wVirtKey, int wScanCode, byte[]
            lpKeyState, [Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pwszBuff,
            int cchBuff, uint wFlags, IntPtr dwhkl);

        [DllImport("user32.dll")]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out int processId);

        public static IntPtr GetKeyboardLayoutForWindow(IntPtr handle)
        {
            return GetKeyboardLayout(GetWindowThreadProcessId(handle.ToInt32(), 0));
        }

        public static string GetWindowText(IntPtr hwnd)
        {
            var len = GetWindowTextLength(hwnd) + 1;
            var sb = new StringBuilder(len);
            GetWindowText(hwnd, sb, sb.Capacity);
            return sb.ToString();
        }

        public static string GetProcessNameByWindowHandler(IntPtr hwnd)
        {
            var process = GetProcessByWindowHandler(hwnd);
            return process == null ? "" : process.ProcessName;
        }

        public static Process GetForegroundAppProcess()
        {
            return GetProcessByWindowHandler(GetForegroundWindow());
        }

        public static Process GetProcessByWindowHandler(IntPtr windowHandler)
        {
            int processId;
            GetWindowThreadProcessId(windowHandler, out processId);

            foreach (var process in Process.GetProcesses())
            {
                if (process.Id == processId)
                {
                    return process;
                }
            }
            return null;
        }
    }
}
