﻿using System;
using System.Windows.Forms;

namespace AgentLib.Gma
{
    /// <summary>
    /// This component monitors all mouse activities globally (also outside of the application) 
    /// and provides appropriate events.
    /// </summary>
    public class GlobalEventProvider
    {
        //################################################################
        #region Mouse events

        private event MouseEventHandler MMouseMove;

        /// <summary>
        /// Occurs when the mouse pointer is moved. 
        /// </summary>
        public event MouseEventHandler MouseMove
        {
            add
            {
                if (MMouseMove == null)
                {
                    HookManager.MouseMove += HookManagerMouseMove;
                }
                MMouseMove += value;
            }

            remove
            {
                MMouseMove -= value;
                if (MMouseMove == null)
                {
                    HookManager.MouseMove -= HookManagerMouseMove;
                }
            }
        }

        void HookManagerMouseMove(object sender, MouseEventArgs e)
        {
            if (MMouseMove != null)
            {
                MMouseMove.Invoke(this, e);
            }
        }

        private event MouseEventHandler MMouseClick;
        /// <summary>
        /// Occurs when a click was performed by the mouse. 
        /// </summary>
        public event MouseEventHandler MouseClick
        {
            add
            {
                if (MMouseClick == null)
                {
                    HookManager.MouseClick += HookManagerMouseClick;
                }
                MMouseClick += value;
            }

            remove
            {
                MMouseClick -= value;
                if (MMouseClick == null)
                {
                    HookManager.MouseClick -= HookManagerMouseClick;
                }
            }
        }

        void HookManagerMouseClick(object sender, MouseEventArgs e)
        {
            if (MMouseClick != null)
            {
                MMouseClick.Invoke(this, e);
            }
        }

        private event MouseEventHandler MMouseDown;

        /// <summary>
        /// Occurs when the mouse a mouse button is pressed. 
        /// </summary>
        public event MouseEventHandler MouseDown
        {
            add
            {
                if (MMouseDown == null)
                {
                    HookManager.MouseDown += HookManagerMouseDown;
                }
                MMouseDown += value;
            }

            remove
            {
                MMouseDown -= value;
                if (MMouseDown == null)
                {
                    HookManager.MouseDown -= HookManagerMouseDown;
                }
            }
        }

        void HookManagerMouseDown(object sender, MouseEventArgs e)
        {
            if (MMouseDown != null)
            {
                MMouseDown.Invoke(this, e);
            }
        }


        private event MouseEventHandler MMouseUp;

        /// <summary>
        /// Occurs when a mouse button is released. 
        /// </summary>
        public event MouseEventHandler MouseUp
        {
            add
            {
                if (MMouseUp == null)
                {
                    HookManager.MouseUp += HookManagerMouseUp;
                }
                MMouseUp += value;
            }

            remove
            {
                MMouseUp -= value;
                if (MMouseUp == null)
                {
                    HookManager.MouseUp -= HookManagerMouseUp;
                }
            }
        }

        void HookManagerMouseUp(object sender, MouseEventArgs e)
        {
            if (MMouseUp != null)
            {
                MMouseUp.Invoke(this, e);
            }
        }

        private event MouseEventHandler MMouseDoubleClick;

        /// <summary>
        /// Occurs when a double clicked was performed by the mouse. 
        /// </summary>
        public event MouseEventHandler MouseDoubleClick
        {
            add
            {
                if (MMouseDoubleClick == null)
                {
                    HookManager.MouseDoubleClick += HookManagerMouseDoubleClick;
                }
                MMouseDoubleClick += value;
            }

            remove
            {
                MMouseDoubleClick -= value;
                if (MMouseDoubleClick == null)
                {
                    HookManager.MouseDoubleClick -= HookManagerMouseDoubleClick;
                }
            }
        }

        void HookManagerMouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (MMouseDoubleClick != null)
            {
                MMouseDoubleClick.Invoke(this, e);
            }
        }


        private event EventHandler<MouseEventExtArgs> MMouseMoveExt;

        /// <summary>
        /// Occurs when the mouse pointer is moved. 
        /// </summary>
        /// <remarks>
        /// This event provides extended arguments of type <see cref="MouseEventArgs"/> enabling you to 
        /// supress further processing of mouse movement in other applications.
        /// </remarks>
        public event EventHandler<MouseEventExtArgs> MouseMoveExt
        {
            add
            {
                if (MMouseMoveExt == null)
                {
                    HookManager.MouseMoveExt += HookManagerMouseMoveExt;
                }
                MMouseMoveExt += value;
            }

            remove
            {
                MMouseMoveExt -= value;
                if (MMouseMoveExt == null)
                {
                    HookManager.MouseMoveExt -= HookManagerMouseMoveExt;
                }
            }
        }

        void HookManagerMouseMoveExt(object sender, MouseEventExtArgs e)
        {
            if (MMouseMoveExt != null)
            {
                MMouseMoveExt.Invoke(this, e);
            }
        }

        private event EventHandler<MouseEventExtArgs> MMouseClickExt;

        /// <summary>
        /// Occurs when a click was performed by the mouse. 
        /// </summary>
        /// <remarks>
        /// This event provides extended arguments of type <see cref="MouseEventArgs"/> enabling you to 
        /// supress further processing of mouse click in other applications.
        /// </remarks>
        public event EventHandler<MouseEventExtArgs> MouseClickExt
        {
            add
            {
                if (MMouseClickExt == null)
                {
                    HookManager.MouseClickExt += HookManagerMouseClickExt;
                }
                MMouseClickExt += value;
            }

            remove
            {
                MMouseClickExt -= value;
                if (MMouseClickExt == null)
                {
                    HookManager.MouseClickExt -= HookManagerMouseClickExt;
                }
            }
        }

        void HookManagerMouseClickExt(object sender, MouseEventExtArgs e)
        {
            if (MMouseClickExt != null)
            {
                MMouseClickExt.Invoke(this, e);
            }
        }


        #endregion

        //################################################################
        #region Keyboard events

        private event KeyPressEventHandler MKeyPress;

        /// <summary>
        /// Occurs when a key is pressed.
        /// </summary>
        /// <remarks>
        /// Key events occur in the following order: 
        /// <list type="number">
        /// <item>KeyDown</item>
        /// <item>KeyPress</item>
        /// <item>KeyUp</item>
        /// </list>
        ///The KeyPress event is not raised by noncharacter keys; however, the noncharacter keys do raise the KeyDown and KeyUp events. 
        ///Use the KeyChar property to sample keystrokes at run time and to consume or modify a subset of common keystrokes. 
        ///To handle keyboard events only in your application and not enable other applications to receive keyboard events, 
        /// set the KeyPressEventArgs.Handled property in your form's KeyPress event-handling method to <b>true</b>. 
        /// </remarks>
        public event KeyPressEventHandler KeyPress
        {
            add
            {
                if (MKeyPress==null)
                {
                    HookManager.KeyPress +=HookManagerKeyPress;
                }
                MKeyPress += value;
            }
            remove
            {
                MKeyPress -= value;
                if (MKeyPress == null)
                {
                    HookManager.KeyPress -= HookManagerKeyPress;
                }
            }
        }

        void HookManagerKeyPress(object sender, KeyPressEventArgs e)
        {
            if (MKeyPress != null)
            {
                MKeyPress.Invoke(this, e);
            }
        }

        private event KeyEventHandler MKeyUp;

        /// <summary>
        /// Occurs when a key is released. 
        /// </summary>
        public event KeyEventHandler KeyUp
        {
            add
            {
                if (MKeyUp == null)
                {
                    HookManager.KeyUp += HookManagerKeyUp;
                }
                MKeyUp += value;
            }
            remove
            {
                MKeyUp -= value;
                if (MKeyUp == null)
                {
                    HookManager.KeyUp -= HookManagerKeyUp;
                }
            }
        }

        private void HookManagerKeyUp(object sender, KeyEventArgs e)
        {
            if (MKeyUp != null)
            {
                MKeyUp.Invoke(this, e);
            }
        }

        private event KeyEventHandler MKeyDown;

        /// <summary>
        /// Occurs when a key is preseed. 
        /// </summary>
        public event KeyEventHandler KeyDown
        {
            add
            {
                if (MKeyDown == null)
                {
                    HookManager.KeyDown += HookManagerKeyDown;
                }
                MKeyDown += value;
            }
            remove
            {
                MKeyDown -= value;
                if (MKeyDown == null)
                {
                    HookManager.KeyDown -= HookManagerKeyDown;
                }
            }
        }

        private void HookManagerKeyDown(object sender, KeyEventArgs e)
        {
            MKeyDown.Invoke(this, e);
        }

        #endregion

        
    }
}
