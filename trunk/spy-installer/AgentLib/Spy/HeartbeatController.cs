﻿using System;
using System.Net;
using System.Threading;
using AgentLib.Util;
using AgentLib.Util.DTO;
using Shared;
using Shared.Logging;
using Shared.Util;

namespace AgentLib.Spy
{
    public class HeartbeatController : IDisposable
    {
        private static readonly ILogger Logger = LogFactory.GetLogger(typeof (HeartbeatController));

        private const int HeartBeatTimerInterval = 1000 * 30;

        private readonly Timer _heartBeatTimer;

        private event Delegates.VoidDelegate _onSpyServiceStart;

        private event Delegates.VoidDelegate _onSpyServiceStop;

        private event Delegates.VoidDelegate _onAppUninstall;

        public event Delegates.VoidDelegate OnSpyServiceStart
        {
            add { _onSpyServiceStart += value; }
            remove { _onSpyServiceStart -= value; }
        }

        public event Delegates.VoidDelegate OnSpyServiceStop
        {
            add { _onSpyServiceStop += value; }
            remove { _onSpyServiceStop -= value; }
        }

        public event Delegates.VoidDelegate OnAppUninstall
        {
            add { _onAppUninstall += value; }
            remove { _onAppUninstall -= value; }
        }

        public HeartbeatController()
        {
            Logger.Entering("Constructor");
            _heartBeatTimer = new Timer(state => MakeHeartBeat());
        }

        public void Start()
        {
            Logger.Entering("Start");
            MakeHeartBeat();
            _heartBeatTimer.Change(HeartBeatTimerInterval, HeartBeatTimerInterval);
        }

        public void Dispose()
        {
            Logger.Entering("Dispose");
            _heartBeatTimer.Dispose();
        }

        public void MakeHeartBeat()
        {
            Logger.Entering("MakeHeartBeat");
            var heartbeatOk = false;
            var uninstallApp = false;
            try
            {
                var result = WebService.SendHeartBeat();
                Logger.Info("MakeHeartBeat", "Status: {0}", result);

                heartbeatOk = result == HeartbeatResult.Ok;
                uninstallApp = result == HeartbeatResult.Uninstall;

                ShowHeartbeatResultMessageBox(result);
            }
            catch(WebException exception)
            {
                Logger.Info("MakeHeartBeat", "Http request failed with: {0}", exception);
                AppEnvironment.ShowMessageBoxLastTime(AgentRes.Err_Connection_to_internet_failed);
            }
            catch (Exception exception)
            {
                Logger.Error("InstallNewApp failed", exception);
            }
            finally
            {
                FireExternalEvents(uninstallApp, heartbeatOk);
            }
        }

        private void FireExternalEvents(bool uninstallApp, bool heartbeatOk)
        {
            if (heartbeatOk)
            {
                if (_onSpyServiceStart != null) _onSpyServiceStart.Invoke();
            }
            else
            {
                if (_onSpyServiceStop != null) _onSpyServiceStop.Invoke();
            }
            if (uninstallApp)
            {
                if (_onAppUninstall != null) _onAppUninstall.Invoke();
            }
        }

        private void ShowHeartbeatResultMessageBox(HeartbeatResult status)
        {
            switch (status)
            {
                case HeartbeatResult.Ok:
                    AppEnvironment.ShowMessageBoxLastTime(AgentRes.Info_Application_installed_successfully);
                    break;
                case HeartbeatResult.Pause:
                    AppEnvironment.ShowMessageBoxLastTime(AgentRes.Info_Application_is_paused);
                    break;
                case HeartbeatResult.Uninstall:
                    AppEnvironment.ShowMessageBoxLastTime(AgentRes.Err_Validation_of_application_failed);
                    break;
                default:
                    throw new ArgumentException();
            }
        }
    }
}