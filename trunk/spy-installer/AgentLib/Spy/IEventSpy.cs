﻿

using System.Collections.Generic;
using AgentLib.Util.DTO;

namespace AgentLib.Spy
{
    interface IEventSpy : ISpy
    {
        event SpyEvent SpyEvent;
    }

    internal delegate void SpyEvent(List<KeyLogDataDTO> data);
}
