package com.volvo.platform.spring.conf;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public class AwsCredentialProperties {

    public static final String AWS_CRED_ACCESS_KEY = "${aws.credentials.accessKey}";

    public static final String AWS_CRED_SECRET_KEY = "${aws.credentials.secretKey}";
}
