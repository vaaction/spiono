package com.volvo.platform.spring.conf;


import org.springframework.context.annotation.Profile;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Profile(value = ProfileNames.PRODUCTION)
public @interface ProfileProduction {
	String name = ProfileNames.PRODUCTION;
}
