package com.volvo.platform.gwt.client.http;

import com.google.gwt.http.client.*;

import static com.volvo.platform.gwt.client.http.RequestFailureReason.*;

public class RequestFailure {

    private final RequestFailureReason reason;

    private final Throwable throwable;

    private final Request request;

    private final Response response;

    private RequestFailure(RequestFailureReason reason, Throwable throwable, Request request, Response response) {
        this.reason = reason;
        this.throwable = throwable;
        this.request = request;
        this.response = response;
    }

    public static RequestFailure requestTimeOutException(RequestTimeoutException exception) {
        return new RequestFailure(REQUEST_TIMEOUT_EXCEPTION, exception, null, null);
    }

    public static RequestFailure requestPermissionException(RequestPermissionException exception) {
        return new RequestFailure(REQUEST_PERMISSION_EXCEPTION, exception, null, null);
    }

    public static RequestFailure requestException(RequestException exception) {
        if (exception instanceof RequestTimeoutException) {
            return requestTimeOutException((RequestTimeoutException)exception);
        }
        else if (exception instanceof RequestPermissionException) {
            return requestPermissionException((RequestPermissionException)exception);
        }
        else return new RequestFailure(REQUEST_GENERAL_EXCEPTION, exception, null, null);
    }

    public static RequestFailure callbackError(Request request, Throwable exception) {
        if (exception instanceof RequestException) {
            return requestException((RequestException)exception);
        }
        return new RequestFailure(CALLBACK_ERROR, exception, request, null);
    }

    public static RequestFailure wrongStatusCode(Request request, Response response) {
        if (response.getStatusCode() != HttpStatus.OK.value()) {
            return new RequestFailure(WRONG_STATUS_CODE, null, request, response);
        }
        throw new IllegalStateException("Status code is VALID, can not produce RequestFailure");
    }

    public void handle(HttpFailureHandler handler) {
        switch (reason) {
            case CALLBACK_ERROR:
                handler.onCallbackError(request, throwable);
                break;
            case REQUEST_GENERAL_EXCEPTION:
                handler.onRequestException((RequestException) throwable);
                break;
            case REQUEST_PERMISSION_EXCEPTION:
                handler.onRequestPermissionException((RequestPermissionException)throwable);
                break;
            case REQUEST_TIMEOUT_EXCEPTION:
                handler.onRequestTimeOutException((RequestTimeoutException)throwable);
                break;
            case WRONG_STATUS_CODE:
                handler.onWrongStatusCode(request, response);
                break;
            default:
                throw new IllegalStateException("Unhandled RequestFailure reason: " + toString());
        }
    }

    public boolean isAccessDenied() {
        return reason.equals(RequestFailureReason.WRONG_STATUS_CODE) && response.getStatusCode() == HttpStatus.FORBIDDEN.value();
    }

    @Override
    public String toString() {
        return "RequestFailure{" +
                "reason=" + reason +
                ", throwable=" + throwable +
                ", request=" + request +
                ", response=" + response +
                '}';
    }
}
