package com.volvo.platform.gwt.client.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public final class LangUtil {

	private LangUtil() {
	}

	public static <K, V> HashMap<K, V> hashMap() {
		return new HashMap<K, V>();
	}

	public static <T> ArrayList<T> arrayList() {
		return new ArrayList<T>();
	}

    public static <T> ArrayList<T> arrayList(int capacity) {
        return new ArrayList<T>(capacity);
    }

    public static<T> boolean isArrayNullOrEmpty(T[] array) {
        return array == null || array.length == 0;
    }

    public static<T> Set<T> hashSet() {
        return new HashSet<T>();
    }

    public static String throwableToString(Throwable throwable) {
        Set<Throwable> throwables = hashSet();
        StringBuilder sb = new StringBuilder();
        boolean firstLoop = true;
        while (!throwables.contains(throwable) && throwable != null) {
            // Append throwable
            if (!firstLoop) {
                sb.append("Caused by: ");
            }
            sb.append(throwable).append("\r\n");

            // Append stack trace
            for (StackTraceElement elem: throwable.getStackTrace()) {
                sb.append("\tto ").append(elem).append("\r\n");
            }

            // Change the throwable
            throwables.add(throwable);
            throwable = throwable.getCause();
            firstLoop = false;
        }

        return sb.toString();
    }
}
