package com.volvo.platform.gwt.client.remerror;

import com.google.web.bindery.autobean.shared.AutoBean;
import com.volvo.platform.gwt.client.command.CommandDispatcher;
import com.volvo.platform.gwt.client.http.DummyHttpRequestCallback;
import com.volvo.platform.gwt.client.util.LangUtil;
import com.volvo.platform.gwt.shared.remerror.RemoteErrorBeanFactory;
import com.volvo.platform.gwt.shared.remerror.RemoteErrorDTO;

import static com.google.gwt.core.client.GWT.create;

public final class RemoteErrorUtils {

    private final static RemoteErrorBeanFactory factory = create(RemoteErrorBeanFactory.class);

    private RemoteErrorUtils(){}

    public static void sendRemoteErrorCommand(CommandDispatcher dispatcher, Throwable throwable) {
        sendRemoteErrorCommand(dispatcher, LangUtil.throwableToString(throwable));
    }

    public static void sendRemoteErrorCommand(CommandDispatcher dispatcher, String message) {
        AutoBean<RemoteErrorDTO> remoteError = factory.remoteErrorBean();
        remoteError.as().setMessage(message);
        dispatcher.dispatchCommand(remoteError, new DummyHttpRequestCallback());
    }
}
