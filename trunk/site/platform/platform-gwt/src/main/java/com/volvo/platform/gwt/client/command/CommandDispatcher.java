package com.volvo.platform.gwt.client.command;

import com.google.web.bindery.autobean.shared.AutoBean;
import com.volvo.platform.gwt.client.http.HttpRequestCallback;
import com.volvo.platform.gwt.client.http.HttpUtils;
import com.volvo.platform.gwt.shared.command.CommandDTO;

public class CommandDispatcher {

    private final String url;

    public CommandDispatcher(String url) {
        this.url = url;
    }

    public void dispatchCommand(AutoBean<?> bean, HttpRequestCallback callback) {
        AutoBean<CommandDTO> commandBean = CommandUtils.createCommandBean(bean);
        dispatchCommand(CommandUtils.getAutoBeanPayload(commandBean), callback);
    }

    private void dispatchCommand(String data, HttpRequestCallback callback) {
        HttpUtils.sendJSON(url, data, callback);
    }
}
