package com.volvo.platform.gwt.client.http;

public interface HttpRequestFailureCallback {

    void onFailure(RequestFailure failure);
}
