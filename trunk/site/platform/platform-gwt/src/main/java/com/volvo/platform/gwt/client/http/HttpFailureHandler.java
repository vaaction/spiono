package com.volvo.platform.gwt.client.http;

import com.google.gwt.http.client.*;

public interface HttpFailureHandler {

    void onRequestTimeOutException(RequestTimeoutException exception);

    void onRequestPermissionException(RequestPermissionException exception);

    void onRequestException(RequestException exception);

    void onCallbackError(Request request, Throwable exception);

    void onWrongStatusCode(Request request, Response response);
}
