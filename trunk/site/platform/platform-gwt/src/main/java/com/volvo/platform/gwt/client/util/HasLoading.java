package com.volvo.platform.gwt.client.util;

public interface HasLoading {

    void loading();

    void completed();

    void reset();
}
