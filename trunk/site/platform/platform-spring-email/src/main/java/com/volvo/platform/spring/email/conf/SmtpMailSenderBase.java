package com.volvo.platform.spring.email.conf;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.*;
import com.volvo.platform.spring.conf.AwsCredentialProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import static java.util.Arrays.asList;

abstract public class SmtpMailSenderBase {

    @Value(MailConfigProperties.USERNAME)
    private String username;

    @Value(AwsCredentialProperties.AWS_CRED_ACCESS_KEY)
    private String awsAccessKey;

    @Value(AwsCredentialProperties.AWS_CRED_SECRET_KEY)
    private String awsSecretKey;

    @Bean
    public MailSender mailSender() {
        return new AmazonMailSender(awsAccessKey, awsSecretKey, username);
    }

    private static class AmazonMailSender implements MailSender {

        private final AWSCredentials awsCredentials;

        private final String sender;

        public AmazonMailSender(String accessKey, String secretKey, String sender) {
            this.awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
            this.sender = sender;
        }

        @Override
        public void send(SimpleMailMessage[] simpleMailMessages) throws MailException {
            for (SimpleMailMessage mailMessage : simpleMailMessages) {
                send(mailMessage);
            }
        }

        @Override
        public void send(SimpleMailMessage simpleMailMessage) throws MailException {
            Destination destination = new Destination(asList(simpleMailMessage.getTo()));

            Content subjectContent = new Content(simpleMailMessage.getSubject());
            Body msgBody = new Body(new Content(simpleMailMessage.getText()));
            Message msg = new Message(subjectContent, msgBody);

            SendEmailRequest request = new SendEmailRequest(sender, destination, msg);
            AmazonSimpleEmailServiceClient sesClient = new AmazonSimpleEmailServiceClient(awsCredentials);
            sesClient.sendEmail(request);
        }
    }
}
