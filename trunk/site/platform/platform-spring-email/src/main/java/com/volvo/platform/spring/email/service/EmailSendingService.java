package com.volvo.platform.spring.email.service;


import org.springframework.mail.SimpleMailMessage;

public interface EmailSendingService {

    void send(SimpleMailMessage message);

    void sendAsync(SimpleMailMessage message);
}
