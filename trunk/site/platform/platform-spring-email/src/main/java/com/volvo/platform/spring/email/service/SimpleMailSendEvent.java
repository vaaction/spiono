package com.volvo.platform.spring.email.service;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;
import org.springframework.mail.SimpleMailMessage;

@ToString
@Getter
@EqualsAndHashCode
public class SimpleMailSendEvent extends ApplicationEvent {

    private final SimpleMailMessage message;

    public SimpleMailSendEvent(Object source, SimpleMailMessage message) {
        super(source);

        this.message = message;
    }
}
