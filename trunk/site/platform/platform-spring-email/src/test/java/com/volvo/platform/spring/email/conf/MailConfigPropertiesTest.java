package com.volvo.platform.spring.email.conf;

import org.testng.annotations.Test;

import static com.volvo.platform.java.testing.TestUtils.getStaticFinalStringValues;
import static com.volvo.platform.java.testing.TestUtils.isStringsAreUnique;
import static org.testng.Assert.assertTrue;

public class MailConfigPropertiesTest {

    @Test
    public void testEnumValieAreUnique() {
        assertTrue(isStringsAreUnique(getStaticFinalStringValues(MailConfigProperties.class)));
    }
}
