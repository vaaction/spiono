package com.volvo.platform.java;

import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class JavaUtilsTest {

    @Test
    public void isPrintableCharTest() {
        assertPrintable('a');
        assertPrintable('!');
        assertPrintable(' ');
        //assertPrintable('Ж');
        assertPrintable('<');
    }

    @Test
    public void isStringPrintableTest() {
        assertIsStringPrintable("Hello");
        assertIsStringPrintable("!@#$$$$$$$$$%^&*()_+<>?~");
        assertIsStringPrintable(",");
        assertIsStringPrintable(" Привет ");
    }

    @Test
    public void isStringNonPrintableTest() {
        assertIsStringNonPrintable(" ");
        assertIsStringNonPrintable("    ");
        assertIsStringNonPrintable("\r");
        assertIsStringNonPrintable("\n");
    }

    private static void assertIsStringPrintable(String s) {
        assertTrue(JavaUtils.isPrintableString(s));
    }

    private static void assertIsStringNonPrintable(String s) {
        assertFalse(JavaUtils.isPrintableString(s));
    }

    private static void assertPrintable(char c) {
        assertTrue(JavaUtils.isPrintableChar(c));
    }
}
