package com.volvo.platform.java.testing;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static com.volvo.platform.java.EnumUtils.enumValuesUniqueStringSetSize;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TestUtils {

    public static Object[][] invalidEmailsDataProvider() {
        Object[][] result = new Object[INVALID_EMAILS.length][1];
        for (int j = 0; j < INVALID_EMAILS.length; ++j) {
            result[j][0] = INVALID_EMAILS[j];
        }
        return result;
    }

    public static final String[] INVALID_EMAILS = {
            "Abc.example.com",
            "Abc.@example.com",
            "Abc..123@example.com",
            "A@b@c@example.com",
            "a\"b(c)d,e:f;g<h>i[j\\k]l@example.com",
            "just\"not\"right@example.com",
            "this is\"not\\allowed@example.com",
            "this\\ still\\\"not\\\\allowed@example.com"
    };

    public static String randomUUIDAsString() {
        return String.valueOf(UUID.randomUUID());
    }

    public static void assertValidUUID(String uuid) {
        UUID.fromString(uuid);
    }

    public static int randomInt() {
        return new Random().nextInt();
    }

    public static long randomLong() {
        return new Random().nextLong();
    }

    public static String randomPwd() {
        return randomAlphabetic(15);
    }

    public static String randomPwdHash() {
        return randomAlphabetic(60);
    }

    public static String randomEmail() {
        return randomEmail(16);
    }

    public static String randomEmail(int length) {
        int dogAndAfterLength = "@".length() + 2 + ".com".length();
        int beforeDogLength = Math.max(1, length - dogAndAfterLength);
        return randomAlphabetic(beforeDogLength).toLowerCase() + "@" + randomAlphabetic(2).toLowerCase() + ".com";
    }

    public static boolean isEnumValuesAsStringAreUnique(Enum[] values) {
        return enumValuesUniqueStringSetSize(values) == values.length;
    }

    public static boolean isStringsAreUnique(Collection<String> strings) {
        return newHashSet(strings).size() == strings.size();
    }

    public static boolean isFileExistsInPath(String location) throws FileNotFoundException {
        return isFileExistsInClassPath(CLASSPATH_URL_PREFIX + location);
    }

    public static boolean isFileExistsInClassPath(String location) throws FileNotFoundException {
        return ResourceUtils.getFile(location) != null;
    }

    public static Collection<String> getStaticFinalStringValues(Class<?> clazz) {
        final List<String> fieldValues = newArrayList();
        ReflectionUtils.doWithFields(clazz, new ReflectionUtils.FieldCallback() {
            @Override
            public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                fieldValues.add((String) ReflectionUtils.getField(field, null));
            }
        }, new ReflectionUtils.FieldFilter() {
                    @Override
                    public boolean matches(Field field) {
                        return ReflectionUtils.isPublicStaticFinal(field) && field.getType().equals(String.class);
                    }
                });
        return fieldValues;
    }
}
