package com.volvo.platform.java;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.reflections.Reflections;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.util.ReflectionUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.reflect.ConstructorUtils.invokeConstructor;
import static org.springframework.util.ReflectionUtils.rethrowRuntimeException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JavaUtils {

    public static boolean isPrintableString(String s) {
        int len = s.length();
        for (int j = 0; j < len; ++j) {
            char c = s.charAt(j);
            if (Character.isWhitespace(c) || Character.isSpaceChar(c)) {
                continue; // Skip the whitespaces
            }
            if (isPrintableChar(c)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isPrintableChar( char c ) {
        Character.UnicodeBlock block = Character.UnicodeBlock.of(c);
        return (!Character.isISOControl(c)) && c != 0xFFFF && block != null && block != Character.UnicodeBlock.SPECIALS;
    }

    public static Method findMethodByArgumentType(Class<?> clazz, Class<?> argType) {
        for (Method method: clazz.getMethods()) {
            Class<?>[] parameters = method.getParameterTypes();
            if (parameters.length != 1) {
                continue;
            }

            if (parameters[0].getCanonicalName().equals(argType.getCanonicalName())) {
                return method;
            }
        }
        return null;
    }

    public static<T> Object invokePrivateMethod(Class<T> clazz, T object, String methodName) throws InvocationTargetException, IllegalAccessException {
        Method method = ReflectionUtils.findMethod(clazz, methodName);
        method.setAccessible(true);
        return method.invoke(object, null);
    }

    public static<T> void setPrivateFieldValue(Class<T> clazz, T object, String fieldName, Object newValue) throws IllegalAccessException {
        Field field = ReflectionUtils.findField(clazz, fieldName);
        field.setAccessible(true);
        field.set(object, newValue);
    }

    public static Set<Class<?>> getClassesFromPackageWithAnnotation(String packageName,
                                                                    Class<? extends Annotation> annotation) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage(packageName))
                .setScanners(new TypeAnnotationsScanner()));
        return reflections.getTypesAnnotatedWith(annotation);
    }

    public static<T> T invokeConstructorQuietly(Class<T> clazz) {
          try {
              return invokeConstructor(clazz);
          }
          catch (Exception e) {
              rethrowRuntimeException(e);
              return null;
          }
    }

    public static <T> List<T> newArrayListIfNull(List<T> set) {
        return set == null ? (List<T>) newArrayList() : set;
    }
}
