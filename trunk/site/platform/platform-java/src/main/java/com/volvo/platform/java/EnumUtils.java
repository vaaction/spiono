package com.volvo.platform.java;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Set;

import static com.google.common.collect.Sets.newHashSetWithExpectedSize;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EnumUtils {
    public static int enumValuesUniqueStringSetSize(Enum[] values) {
        return enumValuesToUniqueStringSet(values).size();
    }

    public static Set<String> enumValuesToUniqueStringSet(Enum[] values) {
        Set<String> result = newHashSetWithExpectedSize(values.length);
        for (Object property: values) {
            result.add(property.toString());
        }
        return result;
    }
}
