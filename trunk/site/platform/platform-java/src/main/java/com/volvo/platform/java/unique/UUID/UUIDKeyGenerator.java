package com.volvo.platform.java.unique.UUID;

import com.volvo.platform.java.unique.UniqueKeyGenerator;

import java.util.UUID;

public class UUIDKeyGenerator implements UniqueKeyGenerator<UUID> {
    @Override
    public UUID generateNew() {
        return UUID.randomUUID();
    }
}
