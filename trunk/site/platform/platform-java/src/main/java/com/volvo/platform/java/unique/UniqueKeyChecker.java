package com.volvo.platform.java.unique;

public interface UniqueKeyChecker<TKeyType> {

    boolean isKeyUnique(TKeyType key);
}
