package com.volvo.site.mailing.service;

import com.volvo.platform.spring.email.service.EmailSendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;

import java.util.Locale;

abstract class EmailServiceImplBase implements EmailService {

    @Autowired
    private EmailSendingService mailSendingService;

    @Autowired
    private EmailTemplateService mailTemplateService;

    @Value("${mail.username}")
    private String fromUserName;

    @Override
    public void sendRegistrationEmail(String to, String unique_key, Locale locale) {
        send(mailTemplateService.registrationEmail(to, unique_key, locale));
    }
    @Override
    public void sendForgotPasswordEmail(String to, String unique_key, Locale locale) {
        send(mailTemplateService.forgotPasswordEmail(to, unique_key, locale));
    }

    private void send(SimpleMailMessage message) {
        message.setFrom(fromUserName);
        mailSendingService.send(message);
    }
}
