package com.volvo.site.mailing.config;

import com.volvo.platform.spring.conf.ProfileDev;
import com.volvo.platform.spring.email.conf.SmtpMailSenderBase;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;

@Configuration
@PropertySource(CLASSPATH_URL_PREFIX + ConfigFilePaths.CONF_DEV_PATH)
@ProfileDev
public class DevMailSenderConfig extends SmtpMailSenderBase {
}
