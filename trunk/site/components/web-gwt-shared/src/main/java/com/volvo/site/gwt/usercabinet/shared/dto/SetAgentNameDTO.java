package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.GwtDTO;

public interface SetAgentNameDTO extends GwtDTO, HasAgentIdDTO {

    void setAgentName(String agentName);

    String getAgentName();
}
