package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.PageResultDTO;

import java.util.List;

public interface PagedAgentsListDTO extends PageResultDTO {

    void setUserAgents(List<AgentDTO> userAgents);
    List<AgentDTO> getUserAgents();

}
