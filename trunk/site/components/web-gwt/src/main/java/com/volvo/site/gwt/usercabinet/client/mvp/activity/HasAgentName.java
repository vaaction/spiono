package com.volvo.site.gwt.usercabinet.client.mvp.activity;


public interface HasAgentName {
    void onAgentNameAssigned();
}
