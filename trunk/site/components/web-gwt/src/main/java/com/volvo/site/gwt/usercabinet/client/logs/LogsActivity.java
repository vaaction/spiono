package com.volvo.site.gwt.usercabinet.client.logs;


import com.google.gwt.core.client.Scheduler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.event.shared.EventBus;
import com.volvo.platform.gwt.client.http.DummyHttpRequestCallback;
import com.volvo.platform.gwt.client.http.HttpRequestCallback;
import com.volvo.platform.gwt.client.http.HttpRequestSuccessCallback;
import com.volvo.site.gwt.usercabinet.client.mvp.activity.BaseActivity;
import com.volvo.site.gwt.usercabinet.client.widgets.DateRangeFilter;
import com.volvo.site.gwt.usercabinet.shared.dto.*;
import com.volvo.site.gwt.usercabinet.shared.enums.StatusFilter;

import javax.inject.Inject;

import static com.volvo.platform.gwt.client.http.HttpUtils.responseAsAutoBean;
import static com.volvo.platform.gwt.client.util.UiUtil.scheduleDeferred;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.getBeanFactory;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.getCommandDispatcher;
import static com.volvo.site.gwt.usercabinet.client.util.HttpRequestCallbackBuilder.appendHasLoadingResetToCallback;
import static com.volvo.site.gwt.usercabinet.client.util.HttpRequestCallbackBuilder.newCallbackWithDefaultErrorHandler;


public class LogsActivity extends BaseActivity implements LogsView.LogsPresenter {

	private final LogsView view;
	private long agentId;
    private static final int REQUESTED_AGENTS_PAGE_SIZE = 50;
	public static final int LOGS_PAGE_SIZE = 50;

	@Inject
	public LogsActivity(LogsView view) {
		this.view = view;
	}

	@Override
	public void start(AcceptsOneWidget container, EventBus eventBus) {
		view.setPresenter(this);
		container.setWidget(view.asWidget());
	}

	@Override
	public void onStop() {
		view.onStop();
	}

	public void setAgentId(String agentId) {
        try {
            setAgentId(Long.parseLong(agentId));
        }
        catch (NumberFormatException xfe) {
            // Just do nothing
        }
	}

    private void setAgentId(long agentId) {
        this.agentId = agentId;
        view.setAgentId(this.agentId);
        setAgents();
        setLogs(DateRangeFilter.DATE_RANGE_DISABLED, null, 0);
    }

	private void setAgents() {
		AutoBean<GetAgentsDTO> agents = getBeanFactory().getAgentsDTO();
		agents.as().setPageNumber(0);
		agents.as().setPageSize(REQUESTED_AGENTS_PAGE_SIZE);
		agents.as().setStatusFilter(StatusFilter.ALL_AGENTS);

		HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
			@Override
			public void onSuccess(Request request, Response response) {
				PagedAgentsListDTO agentsResultListDTO = responseAsAutoBean(response, PagedAgentsListDTO.class, getBeanFactory());
				view.setAgents(agentsResultListDTO.getUserAgents());
			}
		});

		getCommandDispatcher().dispatchCommand(agents, callback);
	}

    @Override
	public void setLogs(DateRangeFilter.DateRangeValue dateRangeValue, String keywords, int lastKeyLogId) {
		boolean searchFlag = false;

		AutoBean<GetKeyLogDataDTO> logs = getBeanFactory().getKeyLogDataDTO();
		logs.as().setAgentId(agentId);
		logs.as().setPageSize(LOGS_PAGE_SIZE);
		logs.as().setPageNumber(lastKeyLogId / LOGS_PAGE_SIZE);

		if (dateRangeValue.enabled) {
            logs.as().setStartDate(dateRangeValue.startDate);
			logs.as().setEndDate(dateRangeValue.endDate);
			searchFlag = true;
		}
		if (keywords != null && !keywords.isEmpty()) {
			logs.as().setSearchingString(keywords);
			searchFlag = true;
		}

		final boolean isSearch = searchFlag;
		HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
			@Override
			public void onSuccess(Request request, final Response response) {
				scheduleDeferred(new Scheduler.ScheduledCommand() {
					@Override
					public void execute() {
						KeyLogDataListResultDTO keyLogDataListResultDTO = responseAsAutoBean(response, KeyLogDataListResultDTO.class, getBeanFactory());
						view.setLogs(keyLogDataListResultDTO.getKeyLogData(), isSearch, LOGS_PAGE_SIZE);
					}
				});

			}
		});

		callback = appendHasLoadingResetToCallback(callback, view.loadingNextLogs(), view.refreshing(), view.searching());
		view.loadingNextLogs().loading();
		view.refreshing().loading();
		view.searching().loading();

		getCommandDispatcher().dispatchCommand(logs, callback);
	}

}
