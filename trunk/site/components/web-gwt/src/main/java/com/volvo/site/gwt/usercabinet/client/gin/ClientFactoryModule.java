package com.volvo.site.gwt.usercabinet.client.gin;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import com.volvo.site.gwt.usercabinet.client.agents.AgentsView;
import com.volvo.site.gwt.usercabinet.client.agents.view.AgentsViewImpl;
import com.volvo.site.gwt.usercabinet.client.logs.LogsView;
import com.volvo.site.gwt.usercabinet.client.logs.view.LogsViewImpl;
import com.volvo.site.gwt.usercabinet.client.menu.activity.ChangePasswordActivity;
import com.volvo.site.gwt.usercabinet.client.menu.view.ChangePasswordView;
import com.volvo.site.gwt.usercabinet.client.menu.view.MenuView;
import com.volvo.site.gwt.usercabinet.client.statistics.StatisticsView;
import com.volvo.site.gwt.usercabinet.client.statistics.view.StatisticsViewImpl;

public class ClientFactoryModule extends AbstractGinModule {

    protected void configure() {
        // bind the views
        bind(AgentsView.class).to(AgentsViewImpl.class).in(Singleton.class);
        bind(LogsView.class).to(LogsViewImpl.class).in(Singleton.class);
        bind(MenuView.class).to(com.volvo.site.gwt.usercabinet.client.menu.view.menu.MenuView.class).in(Singleton.class);
        bind(ChangePasswordView.class).to(com.volvo.site.gwt.usercabinet.client.menu.view.changepassword.ChangePasswordView.class).in(Singleton.class);
	    bind(StatisticsView.class).to(StatisticsViewImpl.class).in(Singleton.class);

        // bind the activities
        bind(ChangePasswordActivity.class).in(Singleton.class);
    }
}
