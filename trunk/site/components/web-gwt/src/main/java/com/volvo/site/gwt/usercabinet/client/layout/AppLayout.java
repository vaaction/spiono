package com.volvo.site.gwt.usercabinet.client.layout;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;

public class AppLayout extends Composite {
    interface LayoutUiBinder extends UiBinder<Widget, AppLayout> {
    }

    @UiField
    AcceptsOneWidget appContent;

    @UiField
    AcceptsOneWidget topMenu;

    private static LayoutUiBinder ourUiBinder = GWT.create(LayoutUiBinder.class);

    public AppLayout() {
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    public AcceptsOneWidget getAppContentHolder() {
        return this.appContent;
    }

    public AcceptsOneWidget getTopMenuHolder() {
        return topMenu;
    }
}
