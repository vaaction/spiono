package com.volvo.site.gwt.usercabinet.client.menu.view.changepassword;


import com.github.gwtbootstrap.client.ui.*;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.github.gwtbootstrap.client.ui.event.HideEvent;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.volvo.platform.gwt.client.util.HasLoading;
import com.volvo.platform.gwt.client.util.UiUtil;
import com.volvo.site.gwt.usercabinet.shared.constants.SharedGwtConsts;

import static com.volvo.platform.gwt.client.util.UiUtil.*;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.getCommonMessages;


public class ChangePasswordView extends Composite implements com.volvo.site.gwt.usercabinet.client.menu.view.ChangePasswordView {
    interface ChangePasswordViewUiBinder extends UiBinder<Widget, ChangePasswordView> { }
    private static ChangePasswordViewUiBinder uiBinder = GWT.create(ChangePasswordViewUiBinder.class);
    private ChangePasswordPresenter presenter;

    @UiField
    Modal changePasswordModal;

    @UiField
    PasswordTextBox currentPassword;

    @UiField
    PasswordTextBox newPassword;

    @UiField
    PasswordTextBox reTypeNewPassword;

    @UiField
    Button saveButton;

    @UiField
    Button cancelButton;

    @UiField
    HelpBlock currentPasswordHelpBlock;

    @UiField
    HelpBlock newPasswordHelpBlock;

    @UiField
    HelpBlock reTypeNewPasswordHelpBlock;

	@UiField
	ControlLabel currentPasswordControlLabel;

    @UiField
    ControlGroup currentPasswordControlGroup;

	@UiField
	ControlLabel newPasswordControlLabel;

    @UiField
    ControlGroup newPasswordControlGroup;

	@UiField
	ControlLabel reTypeNewPasswordControlLabel;

    @UiField
    ControlGroup reTypeNewPasswordControlGroup;

    public ChangePasswordView() {
        initWidget(uiBinder.createAndBindUi(this));

		changePasswordModal.addDomHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER && saveButton.isEnabled()) {
					onSave();
				}
			}
		}, KeyDownEvent.getType());
    }

    @Override
    public void showModal() {
        setTextToEmptyString(currentPassword, newPassword, reTypeNewPassword);
        setTextToEmptyStringForParagraph(currentPasswordHelpBlock, newPasswordHelpBlock, reTypeNewPasswordHelpBlock);
        setControlGroupType(ControlGroupType.NONE,
                            currentPasswordControlGroup, newPasswordControlGroup, reTypeNewPasswordControlGroup);

        saveButton.setEnabled(false);
        UiUtil.setFocusAfterModalIsShown(changePasswordModal, currentPassword);
        changePasswordModal.show();
    }

    @Override
    public void close() {
        changePasswordModal.hide();
    }

    public void setPresenter(ChangePasswordPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public HasLoading loadingState() {
        return UiUtil.hasLoadingForButtons(saveButton);
    }

    @Override
    public void showIncorrectPassword() {
		showIncorrectPassword(getCommonMessages().infoPasswordIncorrect());
    }

	private void showIncorrectPassword(String message) {
		UiUtil.setFocus(currentPassword);
		currentPasswordHelpBlock.setText(message);
		currentPasswordControlGroup.setType(ControlGroupType.ERROR);
		saveButton.setEnabled(false);
	}

    @UiHandler("saveButton")
    void onSaveChangesButtonClicked(ClickEvent event) {
		onSave();
    }

	private void onSave() {
		if (currentPassword.getText().compareTo(newPassword.getText()) == 0) {
			showIncorrectPassword(getCommonMessages().infoPasswordsEqual());
		}
		else {
			presenter.onSaveChangesButtonClicked(currentPassword.getText(), newPassword.getText());
		}
	}

    @UiHandler("cancelButton")
    void onCancelButtonClicked(ClickEvent event) {
        changePasswordModal.hide();
    }

    @UiHandler("currentPassword")
    void onKeyPressedCurrentPassword(KeyUpEvent event) {
        if (currentPassword.getText().length() == 0) {
            currentPasswordControlGroup.setType(ControlGroupType.NONE);
            currentPasswordHelpBlock.setText("");
            saveButton.setEnabled(false);
        }
        else if(currentPassword.getText().length() < SharedGwtConsts.PWD_LEN_MIN) {
            currentPasswordControlGroup.setType(ControlGroupType.WARNING);
            currentPasswordHelpBlock.setText(getCommonMessages().infoPasswordToShort());
            saveButton.setEnabled(false);
        }
        else {
            currentPasswordControlGroup.setType(ControlGroupType.NONE);
            currentPasswordHelpBlock.setText("");
            if(newPassword.getText().length() >= SharedGwtConsts.PWD_LEN_MIN &&
               newPassword.getText().equals(reTypeNewPassword.getText())) {
                saveButton.setEnabled(true);
            }
        }
    }

    @UiHandler("newPassword")
    void onKeyPressedNewPassword(KeyUpEvent event) {
        matchPasswords();
        if (newPassword.getText().length() == 0) {
            newPasswordControlGroup.setType(ControlGroupType.NONE);
            newPasswordHelpBlock.setText("");
            saveButton.setEnabled(false);
        }
        else if (newPassword.getText().length() < SharedGwtConsts.PWD_LEN_MIN) {
            newPasswordControlGroup.setType(ControlGroupType.WARNING);
            newPasswordHelpBlock.setText(getCommonMessages().infoPasswordToShort());
            saveButton.setEnabled(false);
        }
        else {
            newPasswordControlGroup.setType(ControlGroupType.NONE);
            newPasswordHelpBlock.setText("");
        }
    }

    @UiHandler("reTypeNewPassword")
    void onKeyPressedReTypeNewPassword(KeyUpEvent event) {
        if (reTypeNewPassword.getText().length() < SharedGwtConsts.PWD_LEN_MIN){
            reTypeNewPasswordControlGroup.setType(ControlGroupType.WARNING);
            reTypeNewPasswordHelpBlock.setText(getCommonMessages().infoMatchPasswordToShort());
            saveButton.setEnabled(false);
        }
        matchPasswords();
    }

    @UiHandler("changePasswordModal")
    void onHidePopup(HideEvent hideEvent) {
        loadingState().reset();
    }

    private void matchPasswords() {
        if (reTypeNewPassword.getText().length() == 0) {
            reTypeNewPasswordControlGroup.setType(ControlGroupType.NONE);
            reTypeNewPasswordHelpBlock.setText("");
            saveButton.setEnabled(false);
        }
        else if (reTypeNewPassword.getText().equals(newPassword.getText())) {
            reTypeNewPasswordControlGroup.setType(ControlGroupType.SUCCESS);
            reTypeNewPasswordHelpBlock.setText(getCommonMessages().infoPasswordsMatch());
            if (currentPassword.getText().length() >= SharedGwtConsts.PWD_LEN_MIN &&
                newPassword.getText().length() >= SharedGwtConsts.PWD_LEN_MIN) {
                saveButton.setEnabled(true);
            }
        }
        else {
            reTypeNewPasswordControlGroup.setType(ControlGroupType.WARNING);
            reTypeNewPasswordHelpBlock.setText(getCommonMessages().infoPasswordsDoNotMatch());
            saveButton.setEnabled(false);
        }
    }
}