package com.volvo.site.gwt.usercabinet.client.menu.view.menu;

import com.github.gwtbootstrap.client.ui.Dropdown;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.volvo.site.gwt.usercabinet.shared.constants.SharedGwtConsts;

import static com.volvo.platform.gwt.client.util.UiUtil.getHiddenFieldValue;

public class MenuView extends Composite implements com.volvo.site.gwt.usercabinet.client.menu.view.MenuView {
    interface MenuViewUiBinder extends UiBinder<Widget, MenuView> { }
    private static MenuViewUiBinder uiBinder = GWT.create(MenuViewUiBinder.class);
    private MenuPresenter presenter;

    @UiField
    Dropdown userNameDropdown;

    @UiField
    NavLink changePasswordLink;

    public MenuView() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    @UiHandler("changePasswordLink")
    void onChangePasswordClicked(ClickEvent event) {
        if (presenter != null) {
            presenter.onChangePasswordClicked();
        }
    }

    @Override
    public void setPresenter(MenuPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setUserName(String userName) {
        userNameDropdown.setText(userName);
    }

    @Override
    public String getUserEmail() {
        return getHiddenFieldValue(SharedGwtConsts.DIV_WITH_USERNAME);
    }
}