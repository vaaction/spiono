package com.volvo.site.gwt.usercabinet.client.statistics;


import com.google.gwt.core.client.Scheduler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.event.shared.EventBus;
import com.volvo.platform.gwt.client.http.HttpRequestCallback;
import com.volvo.platform.gwt.client.http.HttpRequestSuccessCallback;
import com.volvo.site.gwt.usercabinet.client.mvp.activity.BaseActivity;
import com.volvo.site.gwt.usercabinet.client.widgets.DateRangeFilter;
import com.volvo.site.gwt.usercabinet.shared.dto.GetAgentStatisticDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.GetAgentStatisticResultDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.GetAgentsDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.PagedAgentsListDTO;
import com.volvo.site.gwt.usercabinet.shared.enums.StatusFilter;

import javax.inject.Inject;

import static com.volvo.platform.gwt.client.http.HttpUtils.responseAsAutoBean;
import static com.volvo.platform.gwt.client.util.UiUtil.scheduleDeferred;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.getBeanFactory;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.getCommandDispatcher;
import static com.volvo.site.gwt.usercabinet.client.util.HttpRequestCallbackBuilder.appendHasLoadingResetToCallback;
import static com.volvo.site.gwt.usercabinet.client.util.HttpRequestCallbackBuilder.newCallbackWithDefaultErrorHandler;


public class StatisticsActivity extends BaseActivity implements StatisticsView.StatisticPresenter {

	private final StatisticsView view;
	private long agentId;
    private static final int REQUESTED_AGENTS_PAGE_SIZE = 50;

	@Inject
	public StatisticsActivity(StatisticsView view) {
		this.view = view;
	}

	@Override
	public void start(AcceptsOneWidget container, EventBus eventBus) {
		view.setPresenter(this);
		container.setWidget(view.asWidget());
	}

	@Override
	public void onStop() {
		view.onStop();
	}

    public void setAgentId(String agentId) {
        try {
            setAgentId(Long.parseLong(agentId));
        }
        catch (NumberFormatException xfe) {
            // Just do nothing
        }
    }

    private void setAgentId(long agentId) {
        this.agentId = agentId;
        view.setAgentId(this.agentId);
        setAgents();
        setStatistics(DateRangeFilter.DATE_RANGE_DISABLED);

    }

	private void setAgents() {
		AutoBean<GetAgentsDTO> agents = getBeanFactory().getAgentsDTO();
		agents.as().setPageNumber(0);
		agents.as().setPageSize(REQUESTED_AGENTS_PAGE_SIZE);
		agents.as().setStatusFilter(StatusFilter.ALL_AGENTS);

		HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
			@Override
			public void onSuccess(Request request, Response response) {
				PagedAgentsListDTO agentsResultListDTO = responseAsAutoBean(response, PagedAgentsListDTO.class, getBeanFactory());
				view.setAgents(agentsResultListDTO.getUserAgents());
			}
		});

		getCommandDispatcher().dispatchCommand(agents, callback);
	}

    @Override
	public void setStatistics(DateRangeFilter.DateRangeValue dateRange) {
		AutoBean<GetAgentStatisticDTO> statistics = getBeanFactory().getAgentStatisticDTO();
		statistics.as().setAgentId(agentId);

		if (dateRange.enabled) {
            statistics.as().setStartDate(dateRange.startDate);
			statistics.as().setEndDate(dateRange.endDate);
		}

		HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
			@Override
			public void onSuccess(Request request, final Response response) {
				scheduleDeferred(new Scheduler.ScheduledCommand() {
					@Override
					public void execute() {
						GetAgentStatisticResultDTO resultDTO = responseAsAutoBean(response, GetAgentStatisticResultDTO.class, getBeanFactory());
						view.setStatistics(resultDTO.getStatistic());
					}
				});
			}
		});
		callback = appendHasLoadingResetToCallback(callback, view.refreshing(), view.searching());
		view.refreshing().loading();
		view.searching().loading();
		getCommandDispatcher().dispatchCommand(statistics, callback);
	}

}
