package com.volvo.site.gwt.usercabinet.client.agents.view;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.*;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.view.client.AbstractDataProvider;
import com.volvo.platform.gwt.client.util.UiUtil;
import com.volvo.site.gwt.usercabinet.client.agents.AgentsView;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;
import com.volvo.site.gwt.usercabinet.shared.enums.StatusFilter;

import java.util.List;

import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.commonMessages;

public class AgentsViewImpl extends Composite implements AgentsView {
    interface AgentsViewImplUiBinder extends UiBinder<Widget, AgentsViewImpl> {	}

    private static AgentsViewImplUiBinder uiBinder = GWT.create(AgentsViewImplUiBinder.class);
    private static final int CHECK_AGENT_REGISTERED_DELAY = 60 * 1000;

    private AgentsPresenter presenter;
    private StatusFilter statusFilter = StatusFilter.ALL_AGENTS;
    private Timer checkAgentRegisteredTimer;
	private Timer firstAgentInstalledTimer;

    @UiField
    AgentsTable table;

	@UiField
	WellForm helpHeaderBlock;

	@UiField
	Panel agentsTableControlGroup;

	@UiField
	VerticalPanel agentsTablePlace;

    @UiField
    Button newAgentButton;

	@UiField
	InputAddOn searchPlace;

    @UiField
    TextBox searchTextBox;

    @UiField
    Tooltip searchTooltip;

    @UiField
    Icon searchIcon;

	@UiField
	WellForm installFirstAgentPlace;

	@UiField
	Button downloadFirstAgent;

	@UiField
	TextBox newAgentName;

	Anchor agentLink = new Anchor();

    public AgentsViewImpl() {
        initWidget(uiBinder.createAndBindUi(this));

        searchIcon.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                presenter.refreshTable();
            }
        }, ClickEvent.getType());
    }

    @Override
    public void setPresenter(AgentsPresenter presenter) {
        this.presenter = presenter;
        this.table.setPresenter(presenter);
	}

    @Override
    public void scheduleCheckAgentRegisteredTimer() {
        checkAgentRegisteredTimer = new Timer() {
            public void run() {
                presenter.refreshTable();
            }
        };
        checkAgentRegisteredTimer.scheduleRepeating(CHECK_AGENT_REGISTERED_DELAY);
    }

	@Override
	public void scheduleFirstAgentInstalledTimer() {
		firstAgentInstalledTimer = new Timer() {
			public void run() {
				firstAgentInstalledTimer.cancel();
				presenter.refreshTable();
			}
		};
		firstAgentInstalledTimer.scheduleRepeating(300);
	}

    @Override
    public void onStop() {
        if (checkAgentRegisteredTimer != null) {
            checkAgentRegisteredTimer.cancel();
        }

        searchTextBox.setText("");

        UiUtil.hideAllTooltips();
    }

    @Override
    public void redrawTable() {
        UiUtil.hideAllTooltips();
        table.redraw();
    }

    @Override
    public StatusFilter getStatusFilter() {
        return statusFilter;
    }

    @Override
    public String getSearchString() {
        return searchTextBox.getValue();
    }

    @Override
    public void setRowData(int start, int totalCount, List<AgentDTO> data) {
        table.setRowCount(totalCount);
        table.setRowData(start, data);
    }

    @Override
    public void assignDataProvider(int pageSize, AbstractDataProvider<AgentDTO> data) {
        table.setPageSize(pageSize);
        data.addDataDisplay(table);
    }

	@Override
	public void setAgentsTableElementsVisible(boolean isVisible){
		agentsTableControlGroup.setVisible(isVisible);
		agentsTablePlace.setVisible(isVisible);
	}

	@Override
	public void setInstallFirstAgentPlaceVisible(boolean isVisible){
		installFirstAgentPlace.setVisible(isVisible);
		newAgentName.setFocus(true);
	}

	@Override
	public void setDownloadAgentLink(String downloadAgentLink) {
		agentLink.setHref(downloadAgentLink);
	}

	@Override
	public void showHelpBlock(String helpBlockCaption){
		Label helpBlockLabel = new Label(helpBlockCaption);
		helpHeaderBlock.add(helpBlockLabel);
		helpHeaderBlock.setVisible(true);
	}

	public void clearHelpBlock(){
		helpHeaderBlock.clear();
		helpHeaderBlock.setVisible(false);
	}

	@Override
	public String getNewAgentName(){
		if(this.newAgentName.getText() == null || this.newAgentName.getText().equals("")){
			return "Unnamed";
		} else {
			return this.newAgentName.getText();
		}
	}

    @UiHandler("newAgentButton")
    void onNewAgentButtonClicked(ClickEvent event) {
        final NameEditor panel = new NameEditor("", new NameEditor.NameEditorCallback() {
            @Override
            public void onNameEdited(String lastName, String newName) {
                presenter.onCreateNewAgent(newName);
            }
        });
        panel.setTitle(commonMessages.msgCreateAgentTitle());
        panel.setSaveButtonText(commonMessages.btnDownload());
        int left = newAgentButton.getAbsoluteLeft() - 50;
        int top = newAgentButton.getAbsoluteTop() + 30;
        panel.showWithCoordinates(left, top);
    }

    @UiHandler("searchTextBox")
    void onKeyUpPressedComputerName(KeyUpEvent event) {
        if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
            presenter.refreshTable();
        }
    }

	@UiHandler("downloadFirstAgent")
	void onDownloadFirstAgentButtonClicked(ClickEvent event) {
		presenter.downloadFirstAgent();
	}

	@UiHandler("newAgentName")
	void onNewAgentNameEnterPressed(KeyUpEvent event){
		if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER){
			presenter.downloadFirstAgent();
		}
	}
}
