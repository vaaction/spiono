package com.volvo.site.gwt.usercabinet.client;


import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.volvo.platform.gwt.client.command.CommandDispatcher;
import com.volvo.platform.gwt.client.util.GwtUtil;
import com.volvo.site.gwt.usercabinet.client.gin.ClientFactoryGinjector;
import com.volvo.site.gwt.usercabinet.client.resources.CommonCss;
import com.volvo.site.gwt.usercabinet.client.resources.CommonRes;
import com.volvo.site.gwt.usercabinet.client.resources.HtmlTemplates;
import com.volvo.site.gwt.usercabinet.client.resources.i18n.CommonMessages;
import com.volvo.site.gwt.usercabinet.shared.dto.BeanFactory;

import static com.volvo.site.gwt.usercabinet.shared.constants.SharedGwtConsts.GWT_DISPATCHER_RELATIVE_PATH;

public final class AppWorkflow {

    public static final BeanFactory beanFactory = GWT.create(BeanFactory.class);

    public static final CommandDispatcher commandDispatcher;

    public static final EventBus eventBus = new SimpleEventBus();

    public static final ClientFactoryGinjector clientFactory = GWT.create(ClientFactoryGinjector.class);

    public static final CommonMessages commonMessages = GWT.create(CommonMessages.class);

    public static final HtmlTemplates htmlTemplates = GWT.create(HtmlTemplates.class);

    public static final CommonRes commonRes = GWT.create(CommonRes.class);

    public static final CommonCss commonCss = commonRes.commonCss();

    public static final PlaceController placeController = new PlaceController(eventBus);

    public static BeanFactory getBeanFactory() {
        return beanFactory;
    }

    public static CommandDispatcher getCommandDispatcher() {
        return commandDispatcher;
    }

    public static EventBus getEventBus() {
        return eventBus;
    }

    public static ClientFactoryGinjector getClientFactory() {
        return clientFactory;
    }

    public static CommonMessages getCommonMessages() {
        return commonMessages;
    }

    static {
        commonCss.ensureInjected();
        commonRes.keylogsCss().ensureInjected();
        commandDispatcher = new CommandDispatcher(GwtUtil.getFullPath(GWT_DISPATCHER_RELATIVE_PATH));
    }

	private AppWorkflow() {
	}
}
