package com.volvo.site.server.model.entity;


import org.mockito.Mock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

import static com.volvo.platform.java.JavaUtils.invokePrivateMethod;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

public class AgentInstallerTest {

    private final byte[] data = new byte[0];

    @Mock
    private ClientInstaller clientInstaller;

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    @Test
    public void of1Test() {
        AgentInstaller agentInstaller = AgentInstaller.of(data);
        assertCommons(agentInstaller);
        assertFalse(agentInstaller.isActive());
        assertNull(agentInstaller.getClientInstaller());
    }

    @Test
    public void of2Test() {
        AgentInstaller agentInstaller = AgentInstaller.of(data, clientInstaller);
        assertCommons(agentInstaller);
        assertFalse(agentInstaller.isActive());
        assertEquals(agentInstaller.getClientInstaller(), clientInstaller);
    }

    @Test
    public void of3Test() {
        AgentInstaller agentInstaller = AgentInstaller.of(data, clientInstaller, true);
        assertCommons(agentInstaller);
        assertTrue(agentInstaller.isActive());
        assertEquals(agentInstaller.getClientInstaller(), clientInstaller);
    }

    @Test
    public void prePersistTest() throws InvocationTargetException, IllegalAccessException {
        AgentInstaller agentInstaller = AgentInstaller.of(data);
        assertNull(agentInstaller.getCrDate());

        invokePrivateMethod(AgentInstaller.class, agentInstaller, "prePersist");

        assertNotNull(agentInstaller.getCrDate());
    }

    private void assertCommons(AgentInstaller agentInstaller) {
        assertEquals(agentInstaller.getData(), data);
        assertNull(agentInstaller.getId());
        assertNull(agentInstaller.getCrDate());
        assertEquals(agentInstaller.getClientInstallerId(), 0);
    }
}
