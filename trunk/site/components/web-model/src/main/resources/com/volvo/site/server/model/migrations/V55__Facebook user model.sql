CREATE TABLE `fb_users` (
	`id` INT(20) NOT NULL AUTO_INCREMENT,
	`fb_user_id` BIGINT(20) NOT NULL,
	`spiono_user` INT(11) NOT NULL,
	`expires_in` INT(10) NOT NULL,
	`access_token` VARCHAR(100) NOT NULL,
	`cr_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`user_name` VARCHAR(100) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `spiono_user` (`fb_user_id`),
	INDEX `fk_fbuser_spionouser` (`spiono_user`),
	CONSTRAINT `fk_fbuser_spionouser` FOREIGN KEY (`spiono_user`) REFERENCES `users` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
