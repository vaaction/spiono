ALTER TABLE `user_settings`
	CHANGE COLUMN `last_logs_search_phrase` `last_agents_search_phrase` VARCHAR(50) NULL DEFAULT NULL AFTER `user_id`,
	CHANGE COLUMN `last_logs_search_agent_status` `last_agents_search_status` VARCHAR(50) NULL DEFAULT NULL AFTER `last_agents_search_phrase`;