ALTER TABLE `fb_users`
	CHANGE COLUMN `access_token` `access_token` VARCHAR(300) NOT NULL AFTER `expires_in`;

ALTER TABLE `vk_users`
	CHANGE COLUMN `access_token` `access_token` VARCHAR(300) NOT NULL AFTER `expires_in`;