CREATE TABLE `agent` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL DEFAULT 'DefaultName',
	`user` INT(11) NOT NULL,
	`agent_installer` INT(11) NOT NULL DEFAULT '0',
	`client_installer` INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `user_id_fk` (`user`),
	INDEX `agent_installer_fk` (`agent_installer`),
	INDEX `client_installer_fk` (`client_installer`),
	CONSTRAINT `agent_installer` FOREIGN KEY (`agent_installer`) REFERENCES `agent_installer` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `agent_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `client_installer` FOREIGN KEY (`client_installer`) REFERENCES `client_installer` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;