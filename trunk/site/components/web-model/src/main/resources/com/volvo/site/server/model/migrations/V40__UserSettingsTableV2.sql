ALTER TABLE `user_settings`
	CHANGE COLUMN `filter_quick_search` `agentsview_quick_search` VARCHAR(50) NULL DEFAULT NULL AFTER `user_id`,
	CHANGE COLUMN `filter_agent_status` `agentsview_agent_status` VARCHAR(20) NULL DEFAULT NULL AFTER `agentsview_quick_search`;