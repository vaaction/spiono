ALTER TABLE `agent`
	ADD COLUMN `status` VARCHAR(20) NOT NULL AFTER `client_installer`,
	ADD COLUMN `client_key` VARCHAR(25) NOT NULL AFTER `status`,
	ADD INDEX `status` (`status`);
