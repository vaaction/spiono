package com.volvo.site.server.model.entity;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import static com.volvo.platform.spring.BCryptUtil.bcryptHash;

@Entity
@Table(name = "twitter_users")
@NoArgsConstructor()
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class TwitterUser implements Serializable {

    private static final String TWITTER_EMAIL_SUFFIX = "@twitter.com";

    public static TwitterUser of(long userId, int expiresIn, String accessToken){
        TwitterUser twitterUser = new TwitterUser();
        twitterUser.setTwitterUserId(userId);
        twitterUser.setExpiresIn(expiresIn);
        twitterUser.setAccessToken(accessToken);

        User newUser = User.of(String.valueOf(twitterUser.getTwitterUserId()) + TWITTER_EMAIL_SUFFIX, accessToken,
                User.UserType.TWITTER_USER);
        newUser.setActive(true);
        twitterUser.setSpionoUser(newUser);

        return twitterUser;
    }

    public static void update(TwitterUser twitterUser, int expiresIn, String accessToken){
        twitterUser.getSpionoUser().setPassword(bcryptHash(accessToken));
        twitterUser.updateAccessToken(accessToken);
        twitterUser.setExpiresIn(expiresIn);
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column
    @NotNull
    private Long twitterUserId;

    @Column
    @NotNull
    private Integer expiresIn; // in seconds

    @Column(length = 300)
    @NotNull
    private String accessToken;

    @Column
    private String userName;

    @Column(nullable = false)
    @Setter(AccessLevel.NONE)
    private Date crDate;

    @OneToOne(cascade = CascadeType.ALL)
    private User spionoUser;

    public void updateAccessToken(String newAccessToken){
        this.accessToken = newAccessToken;
    }

    @PrePersist
    private void prePersist() {
        crDate = new Date();
    }
}
