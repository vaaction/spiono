package com.volvo.site.server.model.entity;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import static com.volvo.platform.spring.BCryptUtil.bcryptHash;

@Entity
@Table(name = "fb_users")
@NoArgsConstructor()
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class FacebookUser implements Serializable {

    private static final String FB_EMAIL_SUFFIX = "@fb.com";

    public static FacebookUser of(long userId, int expiresIn, String accessToken){
        FacebookUser facebookUser = new FacebookUser();
        facebookUser.setFbUserId(userId);
        facebookUser.setExpiresIn(expiresIn);
        facebookUser.setAccessToken(accessToken);

        User newUser = User.of(String.valueOf(facebookUser.getFbUserId()) + FB_EMAIL_SUFFIX, accessToken, User.UserType.FACEBOOK_USER);
        newUser.setActive(true);
        facebookUser.setSpionoUser(newUser);

        return facebookUser;
    }

    public static void update(FacebookUser facebookUser, int expiresIn, String accessToken){
        facebookUser.getSpionoUser().setPassword(bcryptHash(accessToken));
        facebookUser.updateAccessToken(accessToken);
        facebookUser.setExpiresIn(expiresIn);
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column
    @NotNull
    private Long fbUserId;

    @Column
    @NotNull
    private Integer expiresIn; // in seconds

    @Column(length = 300)
    @NotNull
    private String accessToken;

    @Column
    private String userName;

    @Column(nullable = false)
    @Setter(AccessLevel.NONE)
    private Date crDate;

    @OneToOne(cascade = CascadeType.ALL)
    private User spionoUser;

    public void updateAccessToken(String newAccessToken){
        this.accessToken = newAccessToken;
    }

    @PrePersist
    private void prePersist() {
        crDate = new Date();
    }
}
