package com.volvo.site.server.model;

import lombok.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ModelConstants {

    public static final int clientInstallerKeyLen = 20;

    public static final int agentCertificateLen = 80;

    public static final int bCryptHashSize = 60;

    public static final int emailMaxLength = 50;

    public static final int mysqlMediumBlobMaxSize = 3 + (16777216 - 1);  // L + 3 bytes, where L < 2^24

    public static final int spyItemMaxTextLen = 150;

    public static final int spyItemMaxWindowTextLen = 100;

    public static final int spyItemMaxProcessDescriptionTextLen = 100;
}
