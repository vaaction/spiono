package com.volvo.site.server.model.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = ForgotPassword.TABLE_NAME)
@Getter
@Setter
@ToString(exclude = "user")
@EqualsAndHashCode(exclude = "user")
@NoArgsConstructor
public class ForgotPassword  implements Serializable {

    public static final String TABLE_NAME = "forgot_pwd";

    public static ForgotPassword of(User user) {
        ForgotPassword forgotPassword = new ForgotPassword();
        forgotPassword.setUser(user);
        forgotPassword.setStatus(ForgotPasswordStatus.CREATED);
        forgotPassword.setUniqueKey(UUID.randomUUID().toString());
        user.addForgotPassword(forgotPassword);
        return forgotPassword;
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @Setter(AccessLevel.NONE)
    private Date crDate;

    @Column
    private Date pwdResetDate;

    @Column(length = 45, nullable = false)
    @NotNull
    @Size(min = 10)
    private String uniqueKey;

    @Column(name = "user", insertable = false, updatable = false)
    @Setter(AccessLevel.NONE)
    private long userId;

    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @NotNull
    private User user;

    @Enumerated(EnumType.STRING)
    private ForgotPasswordStatus status;

    public static enum ForgotPasswordStatus {
        CREATED,
        PWD_CHANGED,
        TIMED_OUT
    }

    @PrePersist
    private void prePersist(){
        resetCreationDate();
        status = ForgotPasswordStatus.CREATED;
        uniqueKey = UUID.randomUUID().toString();
    }

    public void resetCreationDate(){
        crDate = new Date();
    }
}
