package com.volvo.site.server.service.social;

import com.volvo.site.server.model.entity.VKUser;
import org.json.JSONObject;

public interface VKService {

    JSONObject getAuthorizationCredentials(String code);

    VKUser getAuthorizedUser(JSONObject credentials);
}
