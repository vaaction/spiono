package com.volvo.site.server.form;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

import static com.volvo.site.gwt.usercabinet.shared.constants.SharedGwtConsts.*;

@ToString
@Getter
@Setter
public class ChangePasswordForm {

    @NotNull
    @Length(min = PWD_LEN_MIN, max = PWD_LEN_MAX)
    private String password;

    @NotNull
    @Length(min = 10, max = 45)
    private String resCode;
}
