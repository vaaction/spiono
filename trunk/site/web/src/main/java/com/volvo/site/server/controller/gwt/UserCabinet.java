package com.volvo.site.server.controller.gwt;

import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.volvo.platform.gwt.shared.command.CommandDTO;
import com.volvo.platform.gwt.shared.remerror.RemoteErrorDTO;
import com.volvo.platform.java.ServletUtils;
import com.volvo.site.gwt.usercabinet.shared.constants.SharedGwtConsts;
import com.volvo.site.gwt.usercabinet.shared.dto.ChangePwdDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.CheckAgentRegisteredDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.CreateAgentDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.CreateAgentResultDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.DeleteAgentDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.DeleteAndCreateNewAgentDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.GetUserNameDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.GetUserNameResultDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.GetAgentStatisticDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.GetAgentStatisticResultDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.GetAgentsDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.GetKeyLogDataDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.KeyLogDataListResultDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.PagedAgentsListDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.SetAgentNameDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.SetAgentOnPauseDTO;
import com.volvo.site.gwt.usercabinet.shared.enums.StatusFilter;
import com.volvo.site.server.component.Authenticator;
import com.volvo.site.server.component.GwtDispatchMethodInvoker;
import com.volvo.site.server.converter.KeyLogDataListResultDTOConverter;
import com.volvo.site.server.converter.PageAgentToGetAgentsResultListDTOConverter;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.service.AgentService;
import com.volvo.site.server.service.LogsService;
import com.volvo.site.server.service.StatisticsService;
import com.volvo.site.server.service.UserService;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.facade.LogsSearchFacade;
import com.volvo.site.server.util.ControllersPaths;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static com.volvo.platform.java.IOUtils.stringToUtf8Bytes;
import static com.volvo.site.server.model.entity.Agent.AgentStatus;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping(value = ControllersPaths.UserCabinet.CONTROLLER_PATH, method = GET)
@Slf4j
public class UserCabinet {

    private GwtDispatchMethodInvoker gwtDispatcher;
    private UserService userService;
	private StatisticsService statisticsService;
    private LogsService logsService;
    private AgentService agentService;
    private LogsSearchFacade logsSearchFacade;

    private UserCabinet() {
    }

    @Autowired
    public UserCabinet(GwtDispatchMethodInvoker gwtDispatcher, UserService userService,
                       StatisticsService statisticsService, LogsService logsService, AgentService agentService,
                       LogsSearchFacade logsSearchFacade) {
        this.gwtDispatcher = gwtDispatcher;
        this.userService = userService;
        this.statisticsService = statisticsService;
        this.logsService = logsService;
        this.agentService = agentService;
        this.logsSearchFacade = logsSearchFacade;
    }

    @RequestMapping(value = "/")
    public String userCabinet() {
        return "user-cabinet";
    }

    @RequestMapping(value = "/agent/{id}/{key}/download")
    public void downloadInstaller(@PathVariable("id") Long id, @PathVariable("key") String key,
                                  HttpServletResponse response) throws IOException {
        try {
            byte[] clientInstaller = agentService.downloadAgent(id, key);
            ServletUtils.writeExeToResponseAndFlush(response, getSignedInstallerBody(clientInstaller, key), "spiono.exe");
        } catch (EntityNotFoundException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @RequestMapping(value = "/" + SharedGwtConsts.GWT_DISPATCHER_RELATIVE_PATH,
            consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE,
            method = POST)
    @ResponseBody
    public Object handleGwtDispatcherCall(@RequestBody @Valid CommandDTO command) throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {
        try {
            gwtDispatcher.allocateThreadLocalBeanFactory();
            return gwtDispatcher.invokeDispatcherMethod(this, command);
        }
        finally {
            gwtDispatcher.releaseCurrentThreadBeanFactory();
        }
    }

    public void reportGWTError(RemoteErrorDTO error) {
        log.error(error.getMessage());
    }

    public boolean changePassword(ChangePwdDTO changePwdDTO) {
        return userService.changePassword(getUserId(), changePwdDTO.getOldPassword(), changePwdDTO.getNewPassword());
    }

    public CreateAgentResultDTO createAgent(CreateAgentDTO createAgentDTO) {
        Agent agent = agentService.createAgent(getUserId(), createAgentDTO.getName());
        return createAgentResultDTO(agent);
    }

    public boolean checkAgentRegistered(CheckAgentRegisteredDTO checkAgentRegisteredDTO) {
        return agentService.isAgentRegistered(checkAgentRegisteredDTO.getAgentId());
    }

    public GetUserNameResultDTO getUserName(GetUserNameDTO command){
        GetUserNameResultDTO result = gwtDispatcher.newAutoBeanAs(GetUserNameResultDTO.class);
        result.setName(userService.getUserName(command.getEmail()));
        return result;
    }

    public void deleteAgent(DeleteAgentDTO command){
        agentService.deleteAgent(command.getAgentId());
    }

    public CreateAgentResultDTO deleteAgentAndCreateANewOne(DeleteAndCreateNewAgentDTO command) {
        Agent agent = agentService.deleteAgentAndCreateNewOne(getUserId(), command.getAgentId());
        return createAgentResultDTO(agent);
    }

    public void setAgentName(SetAgentNameDTO command) {
        agentService.setAgentName(command.getAgentId(), command.getAgentName());
    }

    public PagedAgentsListDTO getUserAgents(GetAgentsDTO command){
        Page<Agent> agentsPage = agentService.getAgents(getUserId(), command.getSearchingString(),
                formStatusList(command.getStatusFilter()), command.getPageNumber(), command.getPageSize());
        return new PageAgentToGetAgentsResultListDTOConverter(getBeanFactory()).apply(agentsPage);
    }

    public void setAgentOnPause(SetAgentOnPauseDTO command){
        agentService.setAgentOnPause(command.getAgentId(), command.getIsOnPause());
    }

	public KeyLogDataListResultDTO getLogsForAgent(GetKeyLogDataDTO command){
		return new KeyLogDataListResultDTOConverter(getBeanFactory()).apply(
                logsSearchFacade.performLogsSearch(getUserId(), command));
	}

	public GetAgentStatisticResultDTO getAgentStatistic(GetAgentStatisticDTO command){
		Map<String, Integer> statistic = statisticsService.listStatisticForAgent(command.getAgentId(), command.getStartDate(), command.getEndDate());
		GetAgentStatisticResultDTO result = gwtDispatcher.newAutoBeanAs(GetAgentStatisticResultDTO.class);
		result.setStatistic(statistic);
		return result;
	}

    private CreateAgentResultDTO createAgentResultDTO(Agent agent) {
        CreateAgentResultDTO result = gwtDispatcher.newAutoBeanAs(CreateAgentResultDTO.class);
        result.setDownloadKey(agent.getClientKey());
        result.setAgentId(agent.getId());
        return result;
    }

    private byte[] getSignedInstallerBody(byte[] clientInstaller, String clientInstallerKey) throws IOException {
        byte[] installerIdAsBytes = stringToUtf8Bytes(clientInstallerKey);
        return addAll(clientInstaller, installerIdAsBytes);
    }

    private static long getUserId() {
        return Authenticator.getCurrentUserId();
    }

    private AutoBeanFactory getBeanFactory() {
        return gwtDispatcher.getThreadLocalBeanFactory();
    }

    private List<Agent.AgentStatus> formStatusList(StatusFilter status){
       switch (status){
            case ALL_AGENTS:
                return null;
            case REGISTERED:
                return newArrayList(AgentStatus.AGENT_REGISTERED);
            case NOT_REGISTERED:
                return newArrayList(AgentStatus.AGENT_CREATED, AgentStatus.AGENT_DOWNLOADED);
            case DELETED:
                return newArrayList(AgentStatus.AGENT_DELETED);
            default:
                throw new IllegalStateException("Status of agent for user:" + getUserId() + " is incorrect:" + status );
        }
    }

    private StatusFilter convertToStatusFilter(String statusAsString){
        for(StatusFilter statusFilter : StatusFilter.values()){
            if (statusFilter.toString().equals(statusAsString)){
                return statusFilter;
            }
        }
        return null;
    }
}
