package com.volvo.site.server.config;

import com.volvo.site.mailing.config.DevMailSenderConfig;
import com.volvo.site.mailing.config.EmailingConfig;
import com.volvo.site.mailing.config.ProdMailSenderConfig;
import com.volvo.site.server.config.constants.AppProperties;
import freemarker.template.TemplateModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

@Configuration
@Import({RootPropertiesPlaceHolderConfig.class, EmailingConfig.class, DevMailSenderConfig.class, ProdMailSenderConfig.class})
public class RootEmailConfig {
    @Value(AppProperties.SiteName)
    private String siteName;

    @Value(AppProperties.HostName)
    private String hostName;

    @Autowired
    private freemarker.template.Configuration freemarkerConfiguration;

    @PostConstruct
    private void postConstruct() throws TemplateModelException {
        freemarkerConfiguration.setSharedVariable("sitename", siteName);
        freemarkerConfiguration.setSharedVariable("hostname", hostName);
    }
}
