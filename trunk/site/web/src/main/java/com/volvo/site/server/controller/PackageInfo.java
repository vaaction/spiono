package com.volvo.site.server.controller;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PackageInfo {
    public static final String PACKAGE_NAME = "com.volvo.site.server.controller";
}
