package com.volvo.site.server.util;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class CookieConsts {

    public static final String EMAIL = "email";
}
