package com.volvo.site.server.service.impl;

import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.FBUserJpaRepository;
import com.volvo.site.server.repository.TwitterUserJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.repository.VKUserJpaRepository;
import com.volvo.site.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.volvo.site.server.service.exception.EntityNotFoundException.throwEntityNotFoundIfNull;


@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
    private final UserJpaRepository userJpaRepository;
    private final VKUserJpaRepository vkUserJpaRepository;
    private final FBUserJpaRepository fbUserJpaRepository;
    private final TwitterUserJpaRepository twitterUserJpaRepository;

    @Autowired
    public UserServiceImpl(UserJpaRepository userJpaRepository, VKUserJpaRepository vkUserJpaRepository,
                           FBUserJpaRepository fbUserJpaRepository, TwitterUserJpaRepository twitterUserJpaRepository) {
        this.userJpaRepository = userJpaRepository;
        this.vkUserJpaRepository = vkUserJpaRepository;
        this.fbUserJpaRepository = fbUserJpaRepository;
        this.twitterUserJpaRepository = twitterUserJpaRepository;
    }

    @Modifying
    @Override
    public boolean changePassword(long userId, String oldPassword, String newPassword) {
        checkNotNull(oldPassword);
        checkNotNull(newPassword);

        User user = getActiveUser(userId);
        if (user.tryUpdatePassword(oldPassword, newPassword)) {
            userJpaRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public String getUserName(String email) {
        User user = userJpaRepository.findUserByEmail(email);
        switch (user.getType()){
            case CORE_SPIONO:
                return email;
            case VK_USER:
                return getVkUserName(user);
            case FACEBOOK_USER:
                return getFBUserName(user);
            case TWITTER_USER:
                return getTwitterUserName(user);
            default:
                return "undefined";
        }
    }

    private String getVkUserName(User user){
        return vkUserJpaRepository.findBySpionoUser(user).getUserName();
    }

    private String getFBUserName(User user){
        return fbUserJpaRepository.findBySpionoUser(user).getUserName();
    }

    private String getTwitterUserName(User user){
        return twitterUserJpaRepository.findBySpionoUser(user).getUserName();
    }

    private User getActiveUser(long userId) {
        return throwEntityNotFoundIfNull(userJpaRepository.findActiveUserById(userId), User.class, userId);
    }
}
