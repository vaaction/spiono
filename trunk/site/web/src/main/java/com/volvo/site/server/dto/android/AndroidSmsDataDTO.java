package com.volvo.site.server.dto.android;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@ToString
@Getter
@Setter
public class AndroidSmsDataDTO {

	@JsonProperty(value = "Date")
	@NotNull
	@Min(value = 1)
	private long date;

	@JsonProperty(value = "Type")
	private SmsType type;

	@JsonProperty(value = "Number")
	private String number;

	@JsonProperty(value = "Name")
	private String name;

	@JsonProperty(value = "Text")
	private String text;

}
