package com.volvo.site.server.service.social.impl;

import com.volvo.site.server.config.RootPropertiesPlaceHolderConfig;
import com.volvo.site.server.config.constants.AppProperties;
import com.volvo.site.server.model.entity.FacebookUser;
import com.volvo.site.server.model.entity.Syslog;
import com.volvo.site.server.repository.FBUserJpaRepository;
import com.volvo.site.server.repository.SyslogRepository;
import com.volvo.site.server.service.social.FacebookService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.volvo.site.server.util.ControllersPaths.IndexController.LOGIN_FB;

@Service
@Transactional
@Import({RootPropertiesPlaceHolderConfig.class})
public class FacebookServiceImpl implements FacebookService{

    private static final String CLIENT_ID_PARAM = "client_id";
    private static final String CLIENT_SECRET_PARAM = "client_secret";
    private static final String REDIRECT_URI_PARAM = "redirect_uri";
    private static final String CODE_PARAM = "code";
    private static final String EXIPRES_PARAM = "expires";
    private static final String ACCESS_TOKEN_PARAM = "access_token";
    private static final String FB_USER_ID_PARAM = "uid";
    private static final String USER_ID_RESPONSE_PARAM = "user_id";
    private static final String EXPIRES_IN_RESPONSE_PARAM = "expires_in";
    private static final String FIRST_NAME_RESPONSE_PARAM = "first_name";
    private static final String LAST_NAME_RESPONSE_PARAM = "last_name";

    @Value(AppProperties.FBAuthorizationClientId)
    private String fbClientId;

    @Value(AppProperties.FBAuthorizationClientSecret)
    private String fbClientSecret;

    @Value(AppProperties.FBAuthorizationRedirectURI)
    private String redirectUri;

    @Value(AppProperties.FBAuthorizationAccessTokenUrl)
    private String accessTokenUrl;

    @Value(AppProperties.FBAuthorizationGetUserInformationURL)
    private String getUserInformationUrl;

    @Autowired
    private SyslogRepository syslogRepository;

    @Autowired
    private FBUserJpaRepository fbUserJpaRepository;

    @Override
    public String getCredentials(String code) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(getTokenRequestUrl(code));
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            return new BufferedReader(new InputStreamReader((conn.getInputStream()))).readLine();
        } catch (IOException e) {
            syslogRepository.save(Syslog.of("IOException: " + e.getMessage()));
            return null;
        } finally {
            if (conn != null){
                conn.disconnect();
            }
        }
    }

    @Override
    public FacebookUser getAuthorizedUser(String credentials) {
        try {
            String accessToken = getAccessToken(credentials);
            String expiresIn = getExpiresIn(credentials);

            JSONObject userInformation = getUserJSONInformation(accessToken);

            long fbUserId = userInformation.getLong("id");
            String userName = userInformation.getString("name");

            FacebookUser fbUser = fbUserJpaRepository.findByFbUserId(fbUserId);
            if (fbUser == null){
                fbUser = FacebookUser.of(fbUserId, Integer.valueOf(expiresIn), accessToken);
            } else {
                FacebookUser.update(fbUser, Integer.valueOf(expiresIn), accessToken);
            }
            fbUser.setUserName(userName);

            return fbUserJpaRepository.save(fbUser);
        }  catch (Exception e) {
            syslogRepository.save(Syslog.of("Cannot get authorized FB user: " + e.getMessage()));
            return null;
        }
    }

    private JSONObject getUserJSONInformation(String accessToken){
        HttpURLConnection conn = null;
        try {
            URL url = new URL(getUserInformationUrl + accessToken);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            return new JSONObject(br.readLine());
        } catch (IOException e) {
            syslogRepository.save(Syslog.of("IOException in getUserJSONInformation: " + e.getMessage()));
            return null;
        } catch (JSONException e) {
            syslogRepository.save(Syslog.of("JSONException in getUserJSONInformation: " + e.getMessage()));
            return null;
        } finally {
            if (conn != null){
                conn.disconnect();
            }
        }
    }

    private String getTokenRequestUrl(String code){
        StringBuilder result = new StringBuilder(accessTokenUrl + "?").append(CLIENT_ID_PARAM).append("=").append(fbClientId);
        addUrlParameter(result, CLIENT_SECRET_PARAM, fbClientSecret);
        addUrlParameter(result, REDIRECT_URI_PARAM, redirectUri + LOGIN_FB);
        addUrlParameter(result, CODE_PARAM, code);
        return result.toString();
    }

    private void addUrlParameter(StringBuilder sb, String paramName, String paramValue){
        sb.append("&").append(paramName).append("=").append(paramValue);
    }

    private String getExpiresIn(String credentials){
        return credentials.substring(credentials.indexOf(EXIPRES_PARAM + "=")
                + (EXIPRES_PARAM + "=").length(), credentials.length());
    }

    private String getAccessToken(String credentials){
        return credentials.substring((ACCESS_TOKEN_PARAM + "=").length(),
                credentials.indexOf(EXIPRES_PARAM + "=") - 1);
    }

}
