package com.volvo.site.server.util;


import org.apache.lucene.search.Query;
import org.hibernate.search.query.dsl.BooleanJunction;
import org.hibernate.search.query.dsl.MustJunction;
import org.hibernate.search.query.dsl.QueryBuilder;

import java.util.Date;

public class LuceneLogsSearchBuilder {

    private final QueryBuilder queryBuilder;
    private final BooleanJunction<BooleanJunction> booleanJunction;
    private MustJunction mustJunction;

    public LuceneLogsSearchBuilder(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
        this.booleanJunction = queryBuilder.bool();
    }

    public Query build() {
        if (mustJunction == null) {
            addMust(queryBuilder.all().createQuery());
        }
        return mustJunction.createQuery();
    }

    public LuceneLogsSearchBuilder withAgent(long agentId) {
        return addMust(queryBuilder.keyword().onField("agentId").matching(agentId).createQuery());
    }

    public LuceneLogsSearchBuilder withSearchString(String searchString) {
        return addMust(queryBuilder.keyword().onFields("text", "windowText", "processName").
                matching(searchString).createQuery());
    }

    public LuceneLogsSearchBuilder withStartDate(Date startDate) {
        return addMust(queryBuilder.range().onField("startDate").above(startDate).createQuery());
    }

    public LuceneLogsSearchBuilder withEndDate(Date endDate) {
        return addMust(queryBuilder.range().onField("endDate").below(endDate).createQuery());
    }

    public LuceneLogsSearchBuilder addMust(Query query) {
        if (mustJunction == null) {
            mustJunction = booleanJunction.must(query);
        }
        else {
            mustJunction = mustJunction.must(query);
        }
        return this;
    }
}
