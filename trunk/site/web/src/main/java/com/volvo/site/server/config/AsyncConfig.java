package com.volvo.site.server.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.PreDestroy;
import java.util.concurrent.Executor;

@Configuration
@EnableAsync
@EnableScheduling
public class AsyncConfig implements AsyncConfigurer {

    private ThreadPoolTaskExecutor executor;

    @PreDestroy
    private void preDestroy() {
        if (executor != null) {
            executor.shutdown();
        }
    }

    @Override
    public Executor getAsyncExecutor() {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        int maxPoolSize = Math.max(availableProcessors - 1, 1);

        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(maxPoolSize);
        taskExecutor.initialize();
        return executor = taskExecutor;
    }
}
