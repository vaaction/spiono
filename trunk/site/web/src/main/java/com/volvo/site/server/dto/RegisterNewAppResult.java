package com.volvo.site.server.dto;

import lombok.AllArgsConstructor;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * User: Vladimir Korobkov
 * Date: 03/05/12
 * Time: 14:56
 */
@ToString
@AllArgsConstructor
public class RegisterNewAppResult {

    @JsonProperty(value = "State")
    private RegistrationStatus state;

    @JsonProperty(value = "Certificate")
    private String certificate;

    @JsonProperty(value = "Agent")
    private String agent;
}
