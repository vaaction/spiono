package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.AgentInstaller;

public interface AgentInstallerRepository{

    boolean hasActiveAgentInstaller(long clientInstallerId);

    AgentInstaller findLastActive(long clientInstallerId);
}
