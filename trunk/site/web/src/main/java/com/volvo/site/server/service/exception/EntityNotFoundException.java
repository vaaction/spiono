package com.volvo.site.server.service.exception;

import lombok.Getter;

public class EntityNotFoundException extends ServiceException {

    public static<T> T throwEntityNotFoundIfNull(T entity, Class<T> entityClass, Object id) {
        if (entity == null) {
            throw new EntityNotFoundException(entityClass, id);
        }
        return entity;
    }

    @Getter
    private final Class<?> entityClass;

    @Getter
    private final Object id;

    public EntityNotFoundException(Class<?> entityClass, Object id) {
        super("The entiry of class [" + entityClass + "] with id[" + id + "] was not found");

        this.entityClass = entityClass;
        this.id = id;
    }
}
