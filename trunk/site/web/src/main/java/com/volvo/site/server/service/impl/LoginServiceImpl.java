package com.volvo.site.server.service.impl;

import com.volvo.platform.java.unique.UniqueKeyChecker;
import com.volvo.platform.spring.qualifiers.AsyncImplementation;
import com.volvo.site.mailing.service.EmailService;
import com.volvo.site.server.config.constants.AppProperties;
import com.volvo.site.server.model.entity.Registration;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.service.LoginService;
import com.volvo.site.server.service.exception.ConfirmRegistrationException;
import com.volvo.site.server.service.exception.DuplicateEntityException;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Locale;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.volvo.platform.java.DateTimeUtils.nowMinusDays;
import static com.volvo.platform.java.unique.UniqueKeyFactory.*;
import static com.volvo.site.server.service.exception.EntityNotFoundException.throwEntityNotFoundIfNull;

@Service
@Transactional(readOnly = true)
@Slf4j
public class LoginServiceImpl extends LoginServiceImplBase implements LoginService {

    @Autowired
    private LoginServiceAsyncIml async;

    @Value(AppProperties.RootUsername)
    private String rootUsername;

    @Autowired
    @AsyncImplementation
    private EmailService mailService;

    @Modifying
    @Override
    public Registration register(String email, String password, Locale locale) throws DuplicateEntityException {
        log.info("> register");

        email = email.toLowerCase();
        checkNotNull(password);

        checkEmailNotRegistered(email);

        Registration registration = registrationRepository.findByEmailAndStatus(email, Registration.RegistrationStatus.PENDING);
        if (registration == null) {
            registration = createUniqueRegistration(email, password);
        } else {
            registration.setPassword(password);
        }
        registrationRepository.save(registration);

        mailService.sendRegistrationEmail(email, registration.getUniqueKey(), locale);

        return registration;
    }

    @Modifying
    @Override
    public Pair<String, User> confirmRegistration(String key) throws EntityNotFoundException {
        Registration registration = getRegistration(key);

        switch (registration.getStatus()) {
            case PENDING:
                String password = registration.getPassword();
                registration.setStatus(Registration.RegistrationStatus.CONFIRMED);
                registration.setPassword("password_died");
                registrationRepository.save(registration);
                return Pair.of(password, userRepository.findUserByEmail(registration.getEmail()));
            case CONFIRMED:
                throw new ConfirmRegistrationException(ConfirmRegistrationException.Reason.ALREADY_REGISTERED,
                        registration);
            case TIMED_OUT:
                throw new ConfirmRegistrationException(ConfirmRegistrationException.Reason.TIMEOUT,
                        registration);
            default:
                throw new IllegalStateException("Unhandled enum value " + registration);
        }
    }

    @Scheduled(cron = "0 0 * * * *"/* Every hour*/)
    @Override
    public void cleanUpOldRegistrations() {
        Date deadLine = nowMinusDays(2);
        log.info("cleanUpOldRegistrations: deadline date is {}", deadLine);

        int cleanedCount = registrationRepository.markTooOldRegistration(deadLine);
        log.info("Cleaned up {} old registrations", cleanedCount);
    }

    private void checkEmailNotRegistered(String email) throws DuplicateEntityException {
        if (isEmailAlreadyRegistered(email)) {
            throw new DuplicateEntityException(User.class, "email", email);
        }
    }

    private boolean isEmailAlreadyRegistered(String email) {
        email = email.toLowerCase();
        return email.equalsIgnoreCase(rootUsername) || userRepository.findUserIdByEmail(email) != null;
    }

    private Registration getRegistration(String uniqueKey) {
        checkNotNull(uniqueKey);
        Registration registration = registrationRepository.findByUniqueKey(uniqueKey);
        throwEntityNotFoundIfNull(registration, Registration.class, uniqueKey);
        return registration;
    }

    private Registration createUniqueRegistration(String email, String password) {
        String uniqueKey = uniqueKeyGenerator(asStringGenerator(uuidKeyGenerator()), new UniqueKeyChecker<String>() {
            @Override
            public boolean isKeyUnique(String key) {
                return registrationRepository.findByUniqueKey(key) == null;
            }
        }).generateNew();
        return Registration.of(email, password, uniqueKey);
    }
}
