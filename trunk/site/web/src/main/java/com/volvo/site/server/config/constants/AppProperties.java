package com.volvo.site.server.config.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AppProperties {
    public static final String InstallerFileUrl = "${winapp.installer.src}";
    public static final String AgentFileUrl = "${winapp.agent.src}";
    public static final String RootUsername="${root.un}";
    public static final String RootPassword="${root.pwd}";
    public static final String RootPasswordUseHash ="${root.pwd.usehash}";
    public static final String SiteName ="${sitename}";
    public static final String HostName ="${hostname}";

    public static final String VkAuthorizationAccessTokenUrl ="${authorization.vk.access_token_url}";
    public static final String VkAuthorizationClientId ="${authorization.vk.client_id}";
    public static final String VkAuthorizationClientSecret ="${authorization.vk.client_secret}";
    public static final String VkAuthorizationRedirectURI ="${authorization.vk.redirect_uri}";
    public static final String VkApiBaseUrl ="${vk.api.base_url}";

    public static final String FBAuthorizationAccessTokenUrl ="${authorization.fb.access_token_url}";
    public static final String FBAuthorizationClientId ="${authorization.fb.client_id}";
    public static final String FBAuthorizationClientSecret ="${authorization.fb.client_secret}";
    public static final String FBAuthorizationRedirectURI ="${authorization.fb.redirect_uri}";
    public static final String FBAuthorizationGetUserInformationURL ="${authorization.fb.get_user_information}";

    public static final String TWAuthorizationConsumerKey ="${authorization.tw.consumer_key}";
    public static final String TWAuthorizationConsumerSecret ="${authorization.tw.consumer_secret}";
}
