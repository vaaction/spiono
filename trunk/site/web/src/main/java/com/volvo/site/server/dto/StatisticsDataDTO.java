package com.volvo.site.server.dto;

import lombok.*;
import org.codehaus.jackson.annotate.*;
import org.hibernate.validator.constraints.*;

import javax.validation.constraints.*;

import static com.volvo.site.server.model.ModelConstants.*;

@ToString
@Getter
@Setter
public class StatisticsDataDTO {

	@JsonProperty(value = "StartDate")
	@NotNull
	@Min(value = 1)
	private long startDate;

	@JsonProperty(value = "EndDate")
	@NotNull
	@Min(value = 1)
	private long endDate;

	@JsonProperty(value = "ProcessName")
	@NotNull
	@Length(max = spyItemMaxWindowTextLen)
	private String processName;

	@JsonProperty(value = "ProcessDescription")
	@Length(max = spyItemMaxProcessDescriptionTextLen)
	private String processDescription;

}
