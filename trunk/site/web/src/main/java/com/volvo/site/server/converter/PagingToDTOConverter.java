package com.volvo.site.server.converter;

import com.google.common.base.Function;
import com.volvo.platform.gwt.shared.dto.PageResultDTO;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;

import javax.annotation.Nullable;


@AllArgsConstructor
public class PagingToDTOConverter implements Function<PageResultDTO, PageResultDTO> {

    private final Page page;

    @Override
    public PageResultDTO apply(@Nullable PageResultDTO dtoPage) {
        dtoPage.setPageSize(page.getSize());
        dtoPage.setPageNumber(page.getNumber());
        dtoPage.setNumberOfElements(page.getNumberOfElements());
        dtoPage.setTotalElements(page.getTotalElements());
        dtoPage.setTotalPages(page.getTotalPages());
        dtoPage.setHasNextPage(page.hasNextPage());
        dtoPage.setHasPreviousPage(page.hasPreviousPage());
        dtoPage.setIsFirstPage(page.isFirstPage());
        dtoPage.setIsLastPage(page.isLastPage());
        return dtoPage;
    }
}
