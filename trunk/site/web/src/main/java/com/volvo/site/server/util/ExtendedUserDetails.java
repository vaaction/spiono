package com.volvo.site.server.util;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Getter
public class ExtendedUserDetails extends User {

    final private long userId;

    public ExtendedUserDetails(com.volvo.site.server.model.entity.User user, Collection<? extends GrantedAuthority> authorities) {
        super(user.getEmail(), user.getPassword(), authorities);
        this.userId = user.getId();
    }
}
