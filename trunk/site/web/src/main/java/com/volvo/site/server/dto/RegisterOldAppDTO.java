package com.volvo.site.server.dto;

import lombok.Getter;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.validation.constraints.NotNull;

/**
 * User: Vladimir Korobkov
 * Date: 03/05/12
 * Time: 15:04
 */
@ToString(callSuper = true)
@Getter
public class RegisterOldAppDTO extends CertificatedDTO {

    @JsonProperty(value = "NewAppDTO")
    @NotNull
    private RegisterNewAppDTO newAppDTO;
}
