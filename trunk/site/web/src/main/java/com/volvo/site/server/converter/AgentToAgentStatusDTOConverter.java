package com.volvo.site.server.converter;

import com.google.common.base.Function;
import com.volvo.site.gwt.usercabinet.shared.enums.AgentStatus;
import com.volvo.site.server.model.entity.Agent;

import javax.annotation.Nullable;


public class AgentToAgentStatusDTOConverter implements Function<Agent, AgentStatus> {
    @Override
    public AgentStatus apply(@Nullable Agent agent) {
        switch (agent.getStatus()){
            case AGENT_CREATED:
            case AGENT_DOWNLOADED:
                return AgentStatus.NOT_REGISTERED;

            case AGENT_REGISTERED:
                if(agent.isOnPause()){
                    return AgentStatus.ON_PAUSE;
                }
                else{
                    return (agent.hasHeartBeatsAtTheLastMinute() == true ? AgentStatus.ONLINE
                            : AgentStatus.OFFLINE);
                }

            case AGENT_DELETED:
                return AgentStatus.DELETED;

            default:
                throw new IllegalStateException("Agent:" + agent.getId() + "" +
                        " has illegal state:" + agent.getStatus());
        }
    }
}
