package com.volvo.site.server.controller.spy;

import com.volvo.site.server.dto.*;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.service.AgentService;
import com.volvo.site.server.service.WinAppInstallerService;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.io.IOException;

import static com.volvo.platform.java.IOUtils.bytesToBase64String;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping(value = "installer", method = POST)
@Slf4j
public class SpyInstaller {

    @Autowired
    private AgentService agentService;

    @Autowired
    private WinAppInstallerService winAppInstallerService;

    @RequestMapping(value = "/reportError", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public void reportError(@RequestBody @Valid ErrorDTO error)  {
        log.error("reportError: {}", error);
    }

    @RequestMapping(value = "/registerNewApp",
            consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public RegisterNewAppResult registerNewApp(@RequestBody @Valid RegisterNewAppDTO data) throws IOException {
        log.info(".registerNewApp. Installer id: {}", data.getInstallerId());

        try {
            Agent agent = agentService.registerAgent(data.getInstallerId());
            String certificate = agent.getCertificate();
            byte[] agentData = agent.getAgentInstaller().getData();
            return new RegisterNewAppResult(RegistrationStatus.OK, certificate, bytesToBase64String(agentData));
        }
        catch (EntityNotFoundException e) {
            return new RegisterNewAppResult(RegistrationStatus.NOT_FOUND, null, null);
        }
    }

    @RequestMapping(value = "/upateAgent",
            consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public UpdateAgentResult updateAgent(@RequestBody @Valid CertificatedDTO request) throws IOException {
        log.info("> updateAgent {}", request);
        return winAppInstallerService.updateAgentLib(request.getCertificate());
    }
}
