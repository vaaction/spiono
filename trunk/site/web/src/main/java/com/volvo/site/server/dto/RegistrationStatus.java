package com.volvo.site.server.dto;

public enum RegistrationStatus {
    OK, ALREADY_REGISTERED, NOT_FOUND
}
