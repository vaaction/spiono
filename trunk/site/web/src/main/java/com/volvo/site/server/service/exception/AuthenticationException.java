package com.volvo.site.server.service.exception;

public class AuthenticationException extends org.springframework.security.core.AuthenticationException {

    public final boolean haveToConfirmRegistration;

    public final Long errorLoginsCount;

    public AuthenticationException(Throwable t, boolean haveToConfirmRegistration, Long errorLoginsCount) {
        super(t.getMessage(), t);
        this.haveToConfirmRegistration = haveToConfirmRegistration;
        this.errorLoginsCount = errorLoginsCount;
    }
}
