package com.volvo.site.server.service.social;

import com.volvo.site.server.model.entity.TwitterUser;
import twitter4j.Twitter;
import twitter4j.auth.RequestToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface TwitterService {

    void redirectToAuthorizationPage(HttpServletRequest request, HttpServletResponse response);

    TwitterUser getAuthorizedUser(Twitter twitter, RequestToken requestToken, String verifier);

}
