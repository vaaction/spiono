package com.volvo.site.server.config.db;

import com.volvo.platform.spring.conf.ProfileDev;
import com.volvo.site.server.config.CommonConfig;
import com.volvo.site.server.config.constants.ConfigFilePaths;
import com.volvo.site.server.config.placeholder.PropertiesPlaceHolderDevConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ProfileDev
@PropertySource(ConfigFilePaths.DB_DEV_CONFIG_CLASSPATH)
@Import({ CommonConfig.class, PropertiesPlaceHolderDevConfig.class })

@EnableTransactionManagement
@EnableJpaRepositories(com.volvo.site.server.repository.PackageInfo.PACKAGE_NAME)
@ComponentScan(basePackages = {
        com.volvo.site.server.repository.PackageInfo.PACKAGE_NAME,
        com.volvo.site.server.service.PackageInfo.PACKAGE_NAME,
})
public class DevelopmentDbConfig extends AbstractDbConfig {
}
