package com.volvo.site.server.controller.pages;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping(value = "admin", method = GET)
@Slf4j
public class AdminController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        AdminController.log.info("index()");

        return "index";
    }
}
