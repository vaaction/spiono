package com.volvo.site.server.service.social.impl;

import com.volvo.site.server.config.RootPropertiesPlaceHolderConfig;
import com.volvo.site.server.config.constants.AppProperties;
import com.volvo.site.server.model.entity.Syslog;
import com.volvo.site.server.model.entity.VKUser;
import com.volvo.site.server.repository.SyslogRepository;
import com.volvo.site.server.repository.VKUserJpaRepository;
import com.volvo.site.server.service.social.VKService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.volvo.site.server.util.ControllersPaths.IndexController.LOGIN_VK;

@Service
@Transactional
@Import({RootPropertiesPlaceHolderConfig.class})
public class VKServiceImpl implements VKService {

    private static final String CLIENT_ID_PARAM = "client_id";
    private static final String CLIENT_SECRET_PARAM = "client_secret";
    private static final String REDIRECT_URI_PARAM = "redirect_uri";
    private static final String CODE_PARAM = "code";
    private static final String VK_USER_ID_PARAM = "uid";
    private static final String ACCESS_TOKEN_PARAM = "access_token";
    private static final String USER_ID_RESPONSE_PARAM = "user_id";
    private static final String EXPIRES_IN_RESPONSE_PARAM = "expires_in";
    private static final String FIRST_NAME_RESPONSE_PARAM = "first_name";
    private static final String LAST_NAME_RESPONSE_PARAM = "last_name";

    @Autowired
    private SyslogRepository syslogRepository;

    @Autowired
    private VKUserJpaRepository vkUserJpaRepository;

    @Value(AppProperties.VkAuthorizationClientId)
    private String vkClientId;

    @Value(AppProperties.VkAuthorizationClientSecret)
    private String vkClientSecret;

    @Value(AppProperties.VkAuthorizationRedirectURI)
    private String redirectUri;

    @Value(AppProperties.VkAuthorizationAccessTokenUrl)
    private String accessTokenUrl;

    @Value(AppProperties.VkApiBaseUrl)
    private String baseVkAPIUrl;

    @Override
    public JSONObject getAuthorizationCredentials(String code) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(getTokenRequestUrl(code));
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            JSONObject response = new JSONObject(br.readLine());
            syslogRepository.save(Syslog.of("Response from VK server: " + conn.getResponseCode() + ":" + conn.getResponseMessage()));
            return response;
        } catch (IOException e) {
            syslogRepository.save(Syslog.of("IOException: " + e.getMessage()));
            return null;
        } catch (JSONException e) {
            syslogRepository.save(Syslog.of("JSONException: " + e.getMessage()));
            return null;
        } finally {
            if (conn != null){
                conn.disconnect();
            }
        }
    }

    @Override
    public VKUser getAuthorizedUser(JSONObject credentials) {
        try {
            long userId = credentials.getLong(USER_ID_RESPONSE_PARAM);
            int expiresIn = credentials.getInt(EXPIRES_IN_RESPONSE_PARAM);
            String accessToken = credentials.getString(ACCESS_TOKEN_PARAM);

            VKUser vkUser = vkUserJpaRepository.findByVkUserId(userId);
            if (vkUser == null){
                vkUser = VKUser.of(userId, expiresIn, accessToken);
            } else {
                VKUser.update(vkUser, expiresIn, accessToken);
            }
            vkUser.setUserName(getVkUserName(userId, accessToken));
            return vkUserJpaRepository.save(vkUser);

        }  catch (Exception e) {
            syslogRepository.save(Syslog.of("Cannot get authorized VK user: " + e.getMessage()));
            return null;
        }
    }

    private String getTokenRequestUrl(String code){
        StringBuilder result = new StringBuilder(accessTokenUrl + "?").append(CLIENT_ID_PARAM).append("=").append(vkClientId);
        addUrlParameter(result, CLIENT_SECRET_PARAM, vkClientSecret);
        addUrlParameter(result, REDIRECT_URI_PARAM, redirectUri + LOGIN_VK);
        addUrlParameter(result, CODE_PARAM, code);
        return result.toString();
    }

    private void addUrlParameter(StringBuilder sb, String paramName, String paramValue){
        sb.append("&").append(paramName).append("=").append(paramValue);
    }

    private String getVkUserName(long uid, String accessToken){
        HttpURLConnection conn = null;
        try {
            URL url = new URL(getUserNameRequestUrl(uid, accessToken));
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");

            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            JSONObject response = new JSONObject(br.readLine());
            syslogRepository.save(Syslog.of("Response from VK server on name request: " + conn.getResponseCode() + ":" + conn.getResponseMessage()));

            JSONObject responseParams = ((JSONArray)response.get("response")).getJSONObject(0);

            String userFirstName = responseParams.getString(FIRST_NAME_RESPONSE_PARAM);
            String userLastName = responseParams.getString(LAST_NAME_RESPONSE_PARAM);
            return userFirstName + " " + userLastName;
        } catch (IOException e) {
            syslogRepository.save(Syslog.of("IOException during process of getting user name: " + e.getMessage()));
            return null;
        } catch (JSONException e) {
            syslogRepository.save(Syslog.of("JSONException during process of getting user name: " + e.getMessage()));
            return null;
        } finally {
            if (conn != null){
                conn.disconnect();
            }
        }
    }

    private String getUserNameRequestUrl(long uid, String accessToken){
        StringBuilder result = new StringBuilder(baseVkAPIUrl + "users.get" + "?").append("v=5.0");
        addUrlParameter(result, VK_USER_ID_PARAM, String.valueOf(uid));
        addUrlParameter(result, ACCESS_TOKEN_PARAM, accessToken);
        return result.toString();
    }
}
