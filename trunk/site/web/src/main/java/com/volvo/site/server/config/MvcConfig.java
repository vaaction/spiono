package com.volvo.site.server.config;

import com.volvo.site.server.config.placeholder.PropertiesPlaceHolderDevConfig;
import com.volvo.site.server.util.GwtJsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.tiles2.SpringBeanPreparerFactory;
import org.springframework.web.servlet.view.tiles2.TilesConfigurer;
import org.springframework.web.servlet.view.tiles2.TilesView;
import org.springframework.web.servlet.view.tiles2.TilesViewResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.volvo.platform.java.IOUtils.DIR_SEPARATOR_UNIX;

@Import({ CommonConfig.class, PropertiesPlaceHolderDevConfig.class })
@Configuration
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter
{
	@Bean
	public TilesViewResolver configureInternalResourceViewResolver() {
		TilesViewResolver resolver = new TilesViewResolver();
		resolver.setViewClass(TilesView.class);
		return resolver;
	}

	@Bean
	public TilesConfigurer tilesConfigurer() {
		TilesConfigurer configurer = new TilesConfigurer();
		configurer
				.setDefinitions(new String[] { "/WEB-INF/tiles/tiles-templates.xml" });
		configurer.setPreparerFactoryClass(SpringBeanPreparerFactory.class);
		return configurer;
	}

    @Bean
    public RequestMappingHandlerMapping defaultAnnotationHandlerMapping() {
        return new RequestMappingHandlerMapping();
    }

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations(
				"/static/");
        registry.addResourceHandler("/UserCabinet/**").addResourceLocations(
                "/UserCabinet/");
		registry.addResourceHandler("/com.volvo.site.GwtClient/**")
				.addResourceLocations("/com.volvo.site.GwtClient/");
		registry.addResourceHandler("*.ico").addResourceLocations(DIR_SEPARATOR_UNIX);
	}

	@Override
	public void configureMessageConverters(
			List<HttpMessageConverter<?>> converters) {
		converters.add(new GwtJsonMessageConverter());
        converters.add(new BufferedImageHttpMessageConverter());
	}

	@Override
	public Validator getValidator() {
		return new LocalValidatorFactoryBean();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LocaleChangeInterceptor());
		registry.addInterceptor(new PassLocaleInterceptor());
	}

	private static class PassLocaleInterceptor extends HandlerInterceptorAdapter {

		@Override
		public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
			super.postHandle(request, response, handler, modelAndView);
            if(modelAndView != null){
                modelAndView.addObject("locale", RequestContextUtils.getLocaleResolver(request).resolveLocale(request)
                        .getLanguage());
            }
		}
	}
}