package com.volvo.site.server.controller.spy;

import com.volvo.site.server.dto.CertificatedDTO;
import com.volvo.site.server.dto.HeartbeatResult;
import com.volvo.site.server.dto.KeyLogDTO;
import com.volvo.site.server.dto.StatisticsLogDTO;
import com.volvo.site.server.service.AgentService;
import com.volvo.site.server.service.LogsService;
import com.volvo.site.server.service.StatisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping(value = "winapp/spy", method = POST,
        consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
@Slf4j
public class SpyAgent {

    @Autowired
    private AgentService agentService;

	@Autowired
	private StatisticsService statisticsService;

    @Autowired
    private LogsService logsService;

    @RequestMapping(value = "/heartbeat")
    @ResponseBody
    public HeartbeatResult heartBeat(@RequestBody @Valid CertificatedDTO heartBeatDTO) {
        return agentService.makeHeartBeat(heartBeatDTO.getCertificate());
    }

    @RequestMapping(value = "/spylog")
    @ResponseBody
    public void spyLog(@RequestBody @Valid KeyLogDTO spyLog) {
        SpyAgent.log.info("> spyLog: {}", spyLog.getKeyLogs());
        logsService.writeLogs(spyLog);
    }

	@RequestMapping(value = "/statistics")
	@ResponseBody
	public void logStatistics(@RequestBody @Valid StatisticsLogDTO spyLog) {
		SpyAgent.log.info("> spyStatisticsLog: {}", spyLog.getSpyData());
		statisticsService.logAgentData(spyLog);
	}
}
