package com.volvo.site.server.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

public final class ControllersPaths {

    public static String appendSlash(String s) {
        return s + "/";
    }

	protected ControllersPaths() {
	}

    @Deprecated
	public final static class UserService {

		protected UserService() {
		}

		public final static String CONTROLLER_PATH = "userService";

		public static final String JOIN = "/join";

		public static final String JOIN_FULL = CONTROLLER_PATH + JOIN;

        public static final String REMOTE_LOG = "/log";

        public static final String REMOTE_LOG_FULL = CONTROLLER_PATH + REMOTE_LOG;
	}

    @NoArgsConstructor(access = AccessLevel.NONE)
    public final static class UserCabinet {
        public static final String CONTROLLER_PATH = "user-cabinet";
    }

    public static final class IndexController {
        public static final String VIEW_INDEX = "index";

        public static final String VIEW_LOGIN = "login";


	    public static final String LOGIN = "/login";
        public static final String LOGIN_VK = LOGIN + "/WithVK";
        public static final String LOGIN_FB = LOGIN + "/WithFB";
        public static final String LOGIN_TWITTER = LOGIN + "/WithTwitter";
        public static final String TWITTER_CALLBACK =  "/twitter/callback";
        public static final String LOGIN_TWITTER_CALLBACK =  LOGIN + TWITTER_CALLBACK;

        public static final String VIEW_RESTORE = "restore";
        public static final String VIEW_CHANGE_PASSWORD = "change-password";
        public static final String VIEW_JUST_RESTORED = "just-restored";

        public static final String RESTORE = "/restore";
        public static final String CHANGE_PASSWORD = "/restore/change-password";

        public static final String VIEW_REGISTER = "register";
        public static final String VIEW_JUST_REGISTERED = "just-registered";

        public static final String REGISTER = "/register";

        public static final String USERCABINET = "/user-cabinet";
        public static final String VIEW_USERCABINET = "user-cabinet";
        public static final String VIEW_ADMIN = "admin";
    }
}
