package com.volvo.site.server.repository;


import com.volvo.site.server.model.entity.ClientInstaller;


public interface ClientInstallerRepository {

    ClientInstaller findLastActive();
}
