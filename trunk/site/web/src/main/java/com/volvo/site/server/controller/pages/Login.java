package com.volvo.site.server.controller.pages;

import com.volvo.site.server.component.Authenticator;
import com.volvo.site.server.config.RootPropertiesPlaceHolderConfig;
import com.volvo.site.server.form.LoginForm;
import com.volvo.site.server.model.entity.FacebookUser;
import com.volvo.site.server.model.entity.TwitterUser;
import com.volvo.site.server.model.entity.VKUser;
import com.volvo.site.server.service.RestorePasswordService;
import com.volvo.site.server.service.social.FacebookService;
import com.volvo.site.server.service.social.TwitterService;
import com.volvo.site.server.service.social.VKService;
import com.volvo.site.server.util.ControllersPaths;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import twitter4j.Twitter;
import twitter4j.auth.RequestToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URISyntaxException;

import static com.volvo.platform.spring.MvcUtil.modelAndView;
import static com.volvo.platform.spring.MvcUtil.modelAndViewWithForm;
import static com.volvo.platform.spring.MvcUtil.redirectToView;
import static com.volvo.site.server.util.ControllersPaths.IndexController.*;

@Controller
@Slf4j
@Import({RootPropertiesPlaceHolderConfig.class})
public class Login {

    @Autowired
    private Authenticator authenticator;

    @Autowired
    private RestorePasswordService restorePasswordService;

    @Autowired
    private VKService vkService;

    @Autowired
    private FacebookService fbService;

    @Autowired
    private TwitterService twitterService;

    @RequestMapping(value = ControllersPaths.IndexController.LOGIN, method = RequestMethod.GET)
    public ModelAndView login() {
        return modelAndViewWithForm(VIEW_INDEX, LoginForm.class);
    }

    @RequestMapping(value = ControllersPaths.IndexController.LOGIN, method = RequestMethod.POST)
    public ModelAndView login(@Valid LoginForm loginForm, BindingResult bindingResult,
                              HttpServletRequest request, HttpServletResponse response) throws URISyntaxException, IOException {
        if (bindingResult.hasErrors()) {
            return modelAndView(VIEW_INDEX, bindingResult);
        }

        return loginAndRedirect(loginForm, bindingResult, request, response);
    }

    @RequestMapping(value = LOGIN_VK, method = RequestMethod.GET)
    public ModelAndView vkAuthorization(@RequestParam(value = "code") String code, HttpServletRequest request,
                                        HttpServletResponse response){

        JSONObject credentials = vkService.getAuthorizationCredentials(code);
        VKUser authorizedUser = vkService.getAuthorizedUser(credentials);
        if (authorizedUser != null){
            return authenticator.authenticateAndRedirect(authorizedUser.getSpionoUser().getEmail(),
                    authorizedUser.getAccessToken(), request, response);
        } else {
            return redirectToView(VIEW_INDEX).addObject("loginError", Boolean.TRUE);
        }
    }

    @RequestMapping(value = LOGIN_FB, method = RequestMethod.GET)
    public ModelAndView fbAuthorization(@RequestParam(value = "code") String code, HttpServletRequest request,
                                        HttpServletResponse response){

        String credentials = fbService.getCredentials(code);
        FacebookUser authorizedUser = fbService.getAuthorizedUser(credentials);
        if (authorizedUser != null){
            return authenticator.authenticateAndRedirect(authorizedUser.getSpionoUser().getEmail(),
                    authorizedUser.getAccessToken(), request, response);
        } else {
            return redirectToView(VIEW_INDEX).addObject("loginError", Boolean.TRUE);
        }
    }

    @RequestMapping(value = LOGIN_TWITTER, method = RequestMethod.GET)
    public void twitterAuthorization(HttpServletRequest request, HttpServletResponse response){
        twitterService.redirectToAuthorizationPage(request, response);
    }

    @RequestMapping(value = LOGIN_TWITTER_CALLBACK, method = RequestMethod.GET)
    public ModelAndView twitterCallback(HttpServletRequest request, HttpServletResponse response){
        Twitter twitter = (Twitter) request.getSession().getAttribute("twitter");
        RequestToken requestToken = (RequestToken) request.getSession().getAttribute("requestToken");
        String verifier = request.getParameter("oauth_verifier");

        TwitterUser authorizedUser = twitterService.getAuthorizedUser(twitter, requestToken, verifier);
        if (authorizedUser != null){
            return authenticator.authenticateAndRedirect(authorizedUser.getSpionoUser().getEmail(),
                    authorizedUser.getAccessToken(), request, response);
        } else {
            return redirectToView(VIEW_INDEX).addObject("loginError", Boolean.TRUE);
        }
    }

    private ModelAndView loginAndRedirect(LoginForm loginForm, BindingResult bindingResult,
                                          HttpServletRequest request, HttpServletResponse response) {
        String email = loginForm.getEmail();
        String password = loginForm.getPassword();
        try {
            return authenticator.authenticateAndRedirect(email, password, request, response);
        }
        catch (AuthenticationException exception) {
            return handleAuthenticationException(bindingResult, exception, email, request);
        }
    }

    private ModelAndView handleAuthenticationException(BindingResult bindingResult, AuthenticationException exception,
                                                       String email, HttpServletRequest request) {
        if (exception instanceof com.volvo.site.server.service.exception.AuthenticationException) {
            com.volvo.site.server.service.exception.AuthenticationException e =
                    (com.volvo.site.server.service.exception.AuthenticationException)exception;
            if (Long.valueOf(2).equals(e.errorLoginsCount)) {
                restorePasswordService.requestPasswordReset(email, RequestContextUtils.getLocaleResolver(request).resolveLocale(request));
                return returnLoginWithError(bindingResult, "restorePasswordMessageSent");
            }
            if (e.haveToConfirmRegistration) {
                return returnLoginWithError(bindingResult, "haveToConfirm");
            }
        }
        return returnLoginWithError(bindingResult, "loginError");
    }

    private ModelAndView returnLoginWithError(BindingResult bindingResult, String errKey) {
        return modelAndView(VIEW_INDEX, bindingResult).addObject(errKey, Boolean.TRUE);
    }
}
