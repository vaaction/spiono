package com.volvo.site.server.controller.pages;

import com.volvo.site.server.component.Authenticator;
import com.volvo.site.server.form.ChangePasswordForm;
import com.volvo.site.server.form.RestoreForm;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.service.RestorePasswordService;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.exception.UniqueKeyIsAlreadyUsedException;
import com.volvo.site.server.service.exception.UniqueKeyTimedOutException;
import com.volvo.site.server.service.exception.UserInactiveException;
import com.volvo.site.server.util.ControllersPaths;
import com.volvo.site.server.util.MessagesConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Locale;

import static com.volvo.platform.java.EmailUtils.getEmailSitename;
import static com.volvo.platform.spring.MvcUtil.modelAndView;
import static com.volvo.platform.spring.MvcUtil.modelAndViewWithForm;
import static com.volvo.site.server.util.ControllersPaths.IndexController.RESTORE;
import static com.volvo.site.server.util.ControllersPaths.IndexController.VIEW_RESTORE;

@Controller
public class Restore {

    @Autowired
    RestorePasswordService restoreService;

    @Autowired
    private Authenticator authenticator;

    @RequestMapping(value = RESTORE, method = RequestMethod.GET)
    public ModelAndView restore() {
        return modelAndViewWithForm(VIEW_RESTORE, RestoreForm.class);
    }

    @RequestMapping(value = "/restore/restoreAjax", method = RequestMethod.POST)
    @ResponseBody
    public String restoreAjax(@Valid RestoreForm restoreForm) throws InterruptedException {
        try {
            restoreService.requestPasswordReset(restoreForm.getEmail(), Locale.ENGLISH);
        }
        catch (EntityNotFoundException exception){
            return "user_not_found";
        }
        catch (UserInactiveException exception){
            return "user_inactive";
        }

        return "ok";
    }

    @RequestMapping(value = RESTORE, method = RequestMethod.POST)
    public ModelAndView restore(@Valid RestoreForm form, BindingResult bindingResult,
                                HttpServletRequest request)
            throws URISyntaxException, IOException {
        try {
            restoreService.requestPasswordReset(form.getEmail(), RequestContextUtils.getLocaleResolver(request)
		            .resolveLocale(request));
        }
        catch (EntityNotFoundException exception){
            bindingResult.rejectValue("email", MessagesConsts.ERR_USER_NOT_FOUND);
        }
        catch (UserInactiveException exception){
            bindingResult.rejectValue("email", MessagesConsts.ERR_USER_INACTIVE);
        }
        if (bindingResult.hasErrors()) {
            return modelAndView(VIEW_RESTORE, bindingResult);
        }

        return justRestored(form.getEmail());
    }

    @RequestMapping(value = "/restore/just-restored/{email}", method = RequestMethod.GET)
    public ModelAndView justRestored(@PathVariable("email") String email) {
        return new ModelAndView(ControllersPaths.IndexController.VIEW_JUST_RESTORED, "email", getEmailSitename(email));
    }

    @RequestMapping(value = "/restore/change-password/{resCode}", method = RequestMethod.GET)
    public ModelAndView changePassword(@PathVariable("resCode") String resCode) {
        try {
            restoreService.checkPasswordResetKey(resCode);
        }
        catch (UniqueKeyIsAlreadyUsedException exception) {
            return modelAndViewWithForm(ControllersPaths.IndexController.VIEW_RESTORE, new RestoreForm(),
                    new ModelMap("uniqueKeyIsNotValidException", Boolean.TRUE));
        }
        catch (UniqueKeyTimedOutException exception) {
            return modelAndViewWithForm(ControllersPaths.IndexController.VIEW_RESTORE, new RestoreForm(),
                    new ModelMap("uniqueKeyTimedOutException", Boolean.TRUE));
        }
        catch (EntityNotFoundException exception) {
            return modelAndView(ControllersPaths.IndexController.VIEW_CHANGE_PASSWORD,
                                new ModelMap("entityNotFoundException", Boolean.TRUE));
        }

        ChangePasswordForm form = new ChangePasswordForm();
        form.setResCode(resCode);
        return modelAndViewWithForm(ControllersPaths.IndexController.VIEW_CHANGE_PASSWORD, form,
                                    new ModelMap("passwordForm", Boolean.TRUE));
    }

    @RequestMapping(value = ControllersPaths.IndexController.CHANGE_PASSWORD, method = RequestMethod.POST)
    public ModelAndView changePassword(@Valid ChangePasswordForm form, BindingResult bindingResult,
                                       HttpServletRequest request, HttpServletResponse response) {
        if (bindingResult.hasErrors()) {
            ModelAndView mv = modelAndView(ControllersPaths.IndexController.VIEW_CHANGE_PASSWORD, bindingResult);
            mv.getModel().put("passwordForm", Boolean.TRUE);
            mv.getModel().put("resCode", form.getResCode());
            return mv;
        }

        try {
            User user = restoreService.changePassword(form.getResCode(), form.getPassword());
            return authenticator.authenticateAndRedirect(user.getEmail(), form.getPassword(), request, response);
        }
        catch (UniqueKeyIsAlreadyUsedException exception){
            return modelAndViewWithForm(ControllersPaths.IndexController.VIEW_RESTORE, new RestoreForm(),
                    new ModelMap("uniqueKeyIsNotValidException", Boolean.TRUE));
        }
        catch (UniqueKeyTimedOutException exception){
            return modelAndViewWithForm(ControllersPaths.IndexController.VIEW_RESTORE, new RestoreForm(),
                    new ModelMap("uniqueKeyTimedOutException", Boolean.TRUE));
        }
        catch (EntityNotFoundException exception){
            return modelAndView(ControllersPaths.IndexController.VIEW_CHANGE_PASSWORD,
                                new ModelMap("entityNotFoundException", Boolean.TRUE));
        }
    }
}