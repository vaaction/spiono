package com.volvo.site.server.converter;


import com.google.common.base.Function;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.KeyLogDataDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.KeyLogDataListResultDTO;
import com.volvo.site.server.service.facade.LogsSearchFacade;
import lombok.AllArgsConstructor;

import javax.annotation.Nullable;
import java.util.List;

import static com.volvo.platform.java.CollectionsTransformer.transformToList;


@AllArgsConstructor
public class KeyLogDataListResultDTOConverter implements Function<LogsSearchFacade.PerformLogsSearchResult,
        KeyLogDataListResultDTO> {

    private final AutoBeanFactory beanFactory;

    @Override
    public KeyLogDataListResultDTO apply(@Nullable LogsSearchFacade.PerformLogsSearchResult performLogsSearchResult) {
        // Converting keylogs
        List<KeyLogDataDTO> keyLogDataDTOList = new KeyLogListToKeyLogDataDTOListConverter(beanFactory).apply(
                performLogsSearchResult.keylogs);

        // Converting agents
        List<AgentDTO> agents = transformToList(performLogsSearchResult.agents,
                new AgentToAgentsResultItemDTOConverter(beanFactory));

        KeyLogDataListResultDTO resultDto = beanFactory.create(KeyLogDataListResultDTO.class).as();
        resultDto.setKeyLogData(keyLogDataDTOList);
        resultDto.setAvailableAgents(agents);

        return resultDto;
    }
}
