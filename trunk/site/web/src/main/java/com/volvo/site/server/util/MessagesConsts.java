package com.volvo.site.server.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class MessagesConsts {
    public static final String ERR_USER_NOT_FOUND = "error.wrong.user_not_found";
    public static final String ERR_USER_INACTIVE = "error.wrong.user_inactive";
}
