package com.volvo.site.server.service.impl;


import com.volvo.site.server.dto.android.AndroidLogDTO;
import com.volvo.site.server.service.AndroidService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@Transactional(readOnly = true)
public class AndroidServiceImpl implements AndroidService {

	@Override
	public void logAgentData(AndroidLogDTO androidLog) {
		AndroidServiceImpl.log.info("Service > androidCallLogs: {}", androidLog.getCallLogs());
		AndroidServiceImpl.log.info("Service > androidSmsLogs: {}", androidLog.getSmsLogs());
	}
}
