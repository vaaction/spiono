package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.TwitterUser;
import com.volvo.site.server.model.entity.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface TwitterUserJpaRepository extends JpaRepositoryWithLongId<TwitterUser> {

    TwitterUser findByTwitterUserId(Long id);

    TwitterUser findBySpionoUser(User user);

}
