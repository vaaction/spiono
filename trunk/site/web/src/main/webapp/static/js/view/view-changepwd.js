var app = app || {};

$(function($) {
    'use strict';

    app.ChangePwdView = Backbone.View.extend({
        el: '#changePasswordForm',

        initialize: function() {
            $(this.el).validate({
                rules: {
                    password: {
                        minlength: 5
                    }
                },

                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.insertAfter(element)
                    error.addClass('help-block');
                }
            });
        }
    });
});