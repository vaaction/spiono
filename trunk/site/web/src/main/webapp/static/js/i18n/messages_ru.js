
$(function($) {
    'use strict';

    app.Messages = {
        err_passwords_did_not_match: 'Пожалуйста, введите точно такой же пароль',
        err_not_valid_email: 'Введите корректный адрес электронной почты'
    }
});