<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<footer class="text-center">
    <a href="http://vk.com/spiono">
        <img src="${pageContext.request.contextPath}/static/img/vk.png" alt="vk" />
    </a>
    <a href="https://twitter.com/_Spiono_">
        <img src="${pageContext.request.contextPath}/static/img/twitter.png" alt="twitter" />
    </a>
    <a href="http://www.facebook.com/pages/Spiono/333860673400632">
        <img src="${pageContext.request.contextPath}/static/img/facebook.png" alt="facebook" />
    </a>
    </br>
    <small>
        © 2013 <a href="${pageContext.request.contextPath}/">Spiono</a>
    </small>
</footer>