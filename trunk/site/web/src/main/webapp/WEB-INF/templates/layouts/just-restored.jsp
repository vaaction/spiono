<%@ page import="static com.volvo.site.server.util.ControllersPaths.appendSlash" %>
<%@ page import="com.volvo.site.server.util.ControllersPaths" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="text-center">
<h1>
    <spring:message code="restore.header" text="Restore password"/>
    <small>
        <a href="${pageContext.request.contextPath}<%= appendSlash(ControllersPaths.IndexController.LOGIN) %>"><spring:message code="label.signin" text="Sign in"/></a>
        <spring:message code="justrestored.or" text="or"/>
        <a href="${pageContext.request.contextPath}<%= appendSlash(ControllersPaths.IndexController.REGISTER) %>"><spring:message code="label.signup" text="Sign up"/></a>
    </small>
</h1>

<div class="alert alert-info">
    <strong><spring:message code="justrestored.text1" text="You recently asked to reset your password."/></strong></br>
    <spring:message code="justrestored.text2" text="Please, "/> <a href="${email}" target="_blank">
    <spring:message code="justrestored.text3" text="check your email at "/> ${email}</a> <spring:message code="justrestored.text4" text="to restore your password. "/>
</div>
</div>