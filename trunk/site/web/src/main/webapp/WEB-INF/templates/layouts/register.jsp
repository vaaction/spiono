<%@ page import="com.volvo.site.server.form.RegisterForm" %>
<%@ page import="static com.volvo.site.server.util.ControllersPaths.appendSlash" %>
<%@ page import="com.volvo.site.server.util.ControllersPaths" %>
<%@ page import="static com.volvo.platform.spring.MvcUtil.getUncapitalizedSimpleClassName" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<jsp:include page="../includes/localized-messages.jsp"/>
<jsp:include page="../includes/localized-jquery-validate.jsp"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/lib/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/view/view-register.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/app/app-register.js"></script>


<h1 class="text-center">
    <spring:message code="register.header1" text="Register"/>
</h1>

<form:form cssClass="well well-small text-center"
           cssStyle="margin-left: 33%; margin-right: 33%"
        method="POST"
        action="<%= appendSlash(ControllersPaths.IndexController.REGISTER)  %>"
        commandName="<%= getUncapitalizedSimpleClassName(RegisterForm.class) %>">

    <c:set var="email_already_registered">
        <spring:message code="emailAlreadyRegistered"
                        text="This email already registered. Please, type another one."/>
    </c:set>
    <c:if test="${not empty duplicateEmail}">
        <jsp:include page="/WEB-INF/templates/includes/form-error.jsp">
            <jsp:param name="form.error.text" value="${email_already_registered}" />
        </jsp:include>
    </c:if>

    <c:set var="confirmation_expired">
        <spring:message code="confirmationExpired"
                        text="Your confirmation is expired. Please re-register"/>
    </c:set>
    <c:if test="${not empty confirmRegistration}">
        <jsp:include page="/WEB-INF/templates/includes/form-error.jsp">
            <jsp:param name="form.error.text" value="${confirmation_expired}" />
        </jsp:include>
    </c:if>

    <fieldset>
        <jsp:include page="/WEB-INF/templates/includes/form-field-email.jsp" />
        <jsp:include page="/WEB-INF/templates/includes/form-field-password.jsp" />
    </fieldset>

    <button type="submit" class="btn btn-primary"><spring:message code="label.signup_short" text="Sign up"/></button>
</form:form>