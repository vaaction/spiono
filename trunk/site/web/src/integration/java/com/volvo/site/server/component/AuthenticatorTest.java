package com.volvo.site.server.component;

import com.volvo.site.server.config.constants.AppProperties;
import com.volvo.site.server.config.constants.SecurityRoles;
import com.volvo.site.server.model.entity.Registration;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.RegistrationJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.LoginService;
import com.volvo.site.server.service.exception.AuthenticationException;
import com.volvo.site.server.service.exception.NotAuthenticatedAsUserException;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.util.ControllersPaths;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.SaveContextOnUpdateOrErrorResponseWrapper;
import org.springframework.web.servlet.ModelAndView;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import static com.volvo.platform.java.testing.TestUtils.randomEmail;
import static com.volvo.platform.java.testing.TestUtils.randomPwd;
import static com.volvo.platform.spring.MvcUtil.redirectTo;
import static com.volvo.site.server.util.ServerUtil.isAuthorityGranted;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

public class AuthenticatorTest extends AbstractFullSpringTest {

    @Autowired
    Authenticator authenticator;

    @Autowired
    LoginService loginService;

    @Autowired
    RegistrationJpaRepository registrationRepository;

    @Autowired
    UserJpaRepository userJpaRepository;

    @Value(AppProperties.RootUsername)
    private String rootUserName;

    @Value(AppProperties.RootPassword)
    private String rootPassword;

    @Mock
    private HttpServletRequest requestMock;

    @Mock
    private HttpServletResponse responseMock;

    @Mock
    private HttpSession httpSession;

    @Mock
    private SaveContextOnUpdateOrErrorResponseWrapper responseWrapper;

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void emailNullTest() {
        authenticator.authenticateWithServletApi(null, randomPwd(), requestMock, responseMock);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void passwordNullTest() {
        authenticator.authenticateWithServletApi(randomEmail(), null, requestMock, responseMock);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void requestNullTest() {
        authenticator.authenticateWithServletApi(randomEmail(), randomPwd(), null, responseMock);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void responseNullTest() {
        authenticator.authenticateWithServletApi(randomEmail(), randomPwd(), requestMock, null);
    }

    @Test(expectedExceptions = AuthenticationException.class)
    public void tryAuthenticateNotExistingUserTest() {
        authenticator.authenticateWithServletApi("not-existing-user" + randomEmail(), randomPwd(), requestMock, responseMock);
    }

    @Test
    public void authenticateAsAdminTest() {
        Authentication authentication = authenticator.authenticate(rootUserName, rootPassword);
        checkAuthority(authentication, SecurityRoles.ROLE_ADMIN);
    }

    /*
    @Test
    public void getCurrentUserIdPositiveTest() {
        String password = randomPwd();
        User user = loginService.confirmRegistration(loginService.register(randomEmail(), password, Locale.ENGLISH).getUniqueKey()).getRight();
        authenticator.authenticate(user.getEmail(), password);
        assertEquals(Authenticator.getCurrentUserId(), (long)user.getId());
    }
    */

    @Test(expectedExceptions = NotAuthenticatedAsUserException.class)
    public void getCurrentUserIdWhenUserNotLoggedInTest() {
        SecurityContextHolder.clearContext();
        Authenticator.getCurrentUserId();
    }

    @Test(expectedExceptions = NotAuthenticatedAsUserException.class)
    public void getCurrentUserIdWhenUserLoggedInAsRoot() {
        authenticator.authenticate(rootUserName, rootPassword);
        Authenticator.getCurrentUserId();
    }

    @Test(expectedExceptions = AuthenticationException.class)
    public void tryAuthenticateWithoutRegConfirmation() {
        String email = randomEmail();
        String pwd = randomPwd();

        loginService.register(email, pwd, Locale.ENGLISH);
        authenticator.authenticate(email, pwd);
    }

    /*
    @Test
    public void authenticateUserTest() {
        String email = randomEmail();
        String pwd = randomPwd();

        loginService.confirmRegistration(loginService.register(email, pwd, Locale.ENGLISH).getUniqueKey());
        Authentication authentication = authenticator.authenticate(email, pwd);
        checkAuthority(authentication, SecurityRoles.ROLE_USER);
    }
    */

    @Test
    public void tryAuthenticateUserWithNotConfirmedRegistration() {
        try {
            String email = randomEmail();
            String pwd = randomPwd();

            loginService.register(email, pwd, Locale.ENGLISH);
            authenticator.authenticate(email, pwd);
        }
        catch(AuthenticationException e) {
            assertTrue(e.haveToConfirmRegistration);
            return;
        }
        fail();
    }

    /*
    @Test(expectedExceptions = AuthenticationException.class)
    public void tryAuthenticateUserTestWithPwdHash() throws ExecutionException, InterruptedException {
        String email = randomEmail();
        String pwd = randomPwd();

        User user = loginService.confirmRegistration(loginService.register(email, pwd, Locale.ENGLISH).getUniqueKey()).getRight();
        authenticator.authenticate(user.getEmail(), user.getPassword());
    }

    @Test
    public void authenticateAfterFullRegistrationTest() {
        String email = randomEmail();
        String pwd = randomPwd();

        Registration registration = loginService.register(email, pwd, Locale.ENGLISH);
        loginService.confirmRegistration(registration.getUniqueKey());

        Authentication authentication = authenticator.authenticate(email, pwd);
        checkAuthority(authentication, SecurityRoles.ROLE_USER);
    }
    */

    /*
    @Test
    public void tryAuthenticateAndCountLoginErrors() {
        // Given
        String email = randomEmail();
        String pwd = randomPwd();
        loginService.confirmRegistration(loginService.register(email, pwd, Locale.ENGLISH).getUniqueKey());

        // When
        try {
            authenticator.authenticate(email, "1" + pwd);
        }
        catch (AuthenticationException exception) {
            assertEquals(exception.errorLoginsCount, Long.valueOf(1));
            return;
        }
        fail();
    }
    */

    @Test
    public void tryAuthenticateAndCountLoginErrorsForNonExistingAccount() {
        try {
            authenticator.authenticate(randomEmail(), randomPwd());
        }
        catch (AuthenticationException exception) {
            assertNull(exception.errorLoginsCount);
            return;
        }
        fail();
    }

    /*
    @Test
    public void tryWrongAuthenticateAndThenNormalAndWrongAgain() {
        // Given
        String email = randomEmail();
        String pwd = randomPwd();
        loginService.confirmRegistration(loginService.register(email, pwd, Locale.ENGLISH).getUniqueKey());

        // Wrong
        try {
            authenticator.authenticate(email, "1" + pwd);
        }
        catch (AuthenticationException exception) {
            assertEquals(exception.errorLoginsCount, Long.valueOf(1));
        }

        // Normal - reset errorLoginsCount
        authenticator.authenticate(email, pwd);

        // Wrong again
        try {
            authenticator.authenticate(email, "1" + pwd);
        }
        catch (AuthenticationException exception) {
            assertEquals(exception.errorLoginsCount, Long.valueOf(1));
            return;
        }
        fail();
    }

    @Test
    public void authenticateAndRedirectToUserCabinetTest() {
        // Given
        String email = randomEmail();
        String pwd = randomPwd();
        loginService.confirmRegistration(loginService.register(email, pwd, Locale.ENGLISH).getUniqueKey());

        // When
        ModelAndView modelAndView = authenticator.authenticateAndRedirect(email, pwd, requestMock, responseWrapper);

        // Then
        assertEquals(modelAndView.getViewName(), redirectTo(ControllersPaths.IndexController.VIEW_USERCABINET));
    }
    */
    @Test
    public void authenticateAndRedirectToAdminCabinetTest() {
        ModelAndView modelAndView = authenticator.authenticateAndRedirect(rootUserName, rootPassword, requestMock, responseWrapper);
        assertEquals(modelAndView.getViewName(), redirectTo(ControllersPaths.IndexController.VIEW_ADMIN));
    }

    private static void checkAuthority(Authentication authentication, String expectedAuthority) {
        assertTrue(isAuthorityGranted(authentication, expectedAuthority));
    }
}
