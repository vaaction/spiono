package com.volvo.site.server.service;


import com.volvo.site.server.model.entity.Registration;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.RegistrationJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.exception.ConfirmRegistrationException;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.Locale;

import static com.volvo.platform.java.testing.TestUtils.*;
import static java.util.UUID.randomUUID;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class ConfirmRegistrationTest extends AbstractLoginService {
    @Autowired
    UserJpaRepository userRepository;

    @Autowired
    RegistrationJpaRepository registrationRepository;

    /*
    @Test
    public void testConfirmRegistrationSuccessfully() {
        // Given
        String email = randomEmail();
        String pwd = randomPwd();
        Registration registration = loginService.register(email, pwd, Locale.ENGLISH);

        // When
        Pair<String, User> regResult = loginService.confirmRegistration(registration.getUniqueKey());

        // Then
        User user = userRepository.findUserByEmail(email);
        assertNotNull(user);

        registration = registrationRepository.findByUniqueKey(registration.getUniqueKey());
        assertEquals(registration.getStatus(), Registration.RegistrationStatus.CONFIRMED);

        assertEquals(regResult.getLeft(), pwd);
        assertEquals(registration.getPassword(), "password_died");
    }

    @Test(expectedExceptions = ConfirmRegistrationException.class)
    public void testConfirmRegistrationWithExistedEmail() {
        // Given
        String email = randomEmail();
        String passwd = randomPwdHash();
        Registration registration = loginService.register(email,passwd, Locale.ENGLISH);
        userRepository.save(User.of(email,passwd));

        // When
        loginService.confirmRegistration(registration.getUniqueKey());
    }
    */

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void testUniqueKeyNotFoundInRegistrationRepository(){
        final String uniqueKey = String.valueOf(randomUUID());
        loginService.confirmRegistration(uniqueKey);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testNullUniqueKey(){
        loginService.confirmRegistration(null);
    }

    @Test(expectedExceptions = ConfirmRegistrationException.class)
    public void testConfirmAlreadyConfirmed() {
        confirmRegistrationWithStatus(Registration.RegistrationStatus.CONFIRMED);
    }

    @Test(expectedExceptions = ConfirmRegistrationException.class)
    public void testConfirmTimedOut() {
        confirmRegistrationWithStatus(Registration.RegistrationStatus.TIMED_OUT);
    }

    @Test
    public void testConfirmPending() {
        confirmRegistrationWithStatus(Registration.RegistrationStatus.PENDING);
    }

    private void confirmRegistrationWithStatus(Registration.RegistrationStatus status) {
        Registration registration = registrationRepository.save(Registration.of(randomEmail(), randomPwdHash()));
        registration.setStatus(status);
        registrationRepository.save(registration);

        loginService.confirmRegistration(registration.getUniqueKey());
    }
}
