package com.volvo.site.server.service;

import com.volvo.site.server.dto.StatisticsDataDTO;
import com.volvo.site.server.dto.StatisticsLogDTO;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.model.entity.StatisticsLog;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.StatisticsJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.testutil.RepositoryTestUtils;
import com.volvo.site.server.util.ProcessBlackList;
import com.volvo.site.server.util.SpyStatisticsDataDTOMocker;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;

import static com.volvo.site.server.testutil.RepositoryTestUtils.createRandomActiveUser;
import static com.volvo.site.server.testutil.RepositoryTestUtils.random255Bytes;
import static org.testng.Assert.assertEquals;

public class LogStatisticDataTest extends AbstractFullSpringTest {

	@Autowired
	private StatisticsService statisticsService;

	@Autowired
	private AgentService agentService;

	@Autowired
	private WinAppInstallerService winAppInstallerService;

	@Autowired
	private AgentJpaRepository agentJpaRepository;

	@Autowired
	private UserJpaRepository userJpaRepository;

	@Autowired
	private StatisticsJpaRepository statisticsJpaRepository;

	private static final int NUMBER_OF_GENERATED_LOGS = 100;
	private static final String PROCESS_NAME = "process1";
	private static final String PROCESS_DESCRIPTION = "description1";
	private static final long START_DATE = new DateTime(new Date()).toDate().getTime();
	private static final long END_DATE = START_DATE + 10000;
	private Agent agent;

	@Test
	public void logAgentDataTest(){
		User user = userJpaRepository.save(createRandomActiveUser());
		agent = agentJpaRepository.save(createAgent(user));
		agent.setStatus(Agent.AgentStatus.AGENT_REGISTERED);
		agentJpaRepository.save(agent);

		for(int i = 0; i < NUMBER_OF_GENERATED_LOGS; i++){
			StatisticsLogDTO statisticsLogDTO = generateStatisticsLogDTO(generateStatisticDataDTO(
					PROCESS_NAME, PROCESS_DESCRIPTION, START_DATE, END_DATE), agent.getCertificate());
			statisticsService.logAgentData(statisticsLogDTO);
		}

		List<StatisticsLog> result = statisticsJpaRepository.findByAgentId(agent.getId(), false, null, null, ProcessBlackList.getList());
		assertEquals(result.size(), NUMBER_OF_GENERATED_LOGS);
		for(int i = 0; i < NUMBER_OF_GENERATED_LOGS; i++){
			assertEquals(result.get(i).getProcessName(), PROCESS_NAME);
			assertEquals(result.get(i).getProcessDescription(), PROCESS_DESCRIPTION);
			assertEquals(result.get(i).getStartDate().getTime(), START_DATE);
			assertEquals(result.get(i).getEndDate().getTime(), END_DATE);
			assertEquals(result.get(i).getTimeOfUse(), END_DATE - START_DATE);
		}
	}

	private StatisticsLogDTO generateStatisticsLogDTO(StatisticsDataDTO dto, String certificate){
		return SpyStatisticsDataDTOMocker.spyStatisticsDTO(dto, certificate);
	}

	private StatisticsDataDTO generateStatisticDataDTO(String processName, String processDescription, long startDate, long endDate){
		SpyStatisticsDataDTOMocker dtoMocker = new SpyStatisticsDataDTOMocker(processName, processDescription, startDate, endDate);
		return dtoMocker.dto;
	}

	private Agent createAgent(User user){
		ClientInstaller clientInstaller = winAppInstallerService.addClientInstaller(RepositoryTestUtils.random255Bytes);
		winAppInstallerService.addAgentInstaller(clientInstaller.getId(), random255Bytes, true);
		winAppInstallerService.activateClientInstaller(clientInstaller.getId());
		Agent agent = agentService.createAgent(user.getId(), "testAgent");
		agent.generateCertificate();
		return agent;
	}
}
