package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static com.volvo.platform.java.testing.TestUtils.randomEmail;
import static com.volvo.platform.java.testing.TestUtils.randomPwdHash;
import static com.volvo.site.server.testutil.RepositoryTestUtils.createRandomActiveUser;
import static org.testng.Assert.*;

public class UserRepositoryJpaTest  extends AbstractFullSpringTest {

    @Autowired
    UserJpaRepository repository;

    @Test
    public void findIdByEmailTest() {
        assertNull(repository.findUserIdByEmail("xyz"));

        final String email =  randomEmail();
        Long id = repository.save(User.of(email, randomPwdHash())).getId();

        assertEquals(repository.findUserIdByEmail(email), id);
    }

    @Test
    public void findErrLoginsByEmailTest() {
        assertNull(repository.findErrLoginsByEmail("xyz"));

        final String email =  randomEmail();
        User user = repository.save(User.of(email, randomPwdHash()));
        assertEquals(repository.findErrLoginsByEmail(email), Long.valueOf(0));

        user.incrementErrLogins();
        user = repository.save(user);
        assertEquals(repository.findErrLoginsByEmail(email), Long.valueOf(1));

        user.resetErrLogins();
        user = repository.save(user);
        assertEquals(repository.findErrLoginsByEmail(email), Long.valueOf(0));
    }

    @Test
    public void setLoginErrorsCountTest() {
        final String email =  randomEmail();
        User user = repository.save(User.of(email, randomPwdHash()));
        assertEquals(repository.findErrLoginsByEmail(email), Long.valueOf(0));

        repository.setLoginErrorsCount(email, 5);
        assertEquals(repository.findErrLoginsByEmail(email), Long.valueOf(5));
    }

    @Test
    public void resetLoginErrorsCountTest() {
        final String email =  randomEmail();
        User user = repository.save(User.of(email, randomPwdHash()));
        assertEquals(repository.findErrLoginsByEmail(email), Long.valueOf(0));

        repository.setLoginErrorsCount(email, 5);
        assertEquals(repository.findErrLoginsByEmail(email), Long.valueOf(5));

        repository.resetLoginErrorsCount(email);
        assertEquals(repository.findErrLoginsByEmail(email), Long.valueOf(0));
    }

    @Test
    public void finaActiveUserByIdTest() {
        // Given
        User inactiveUser = repository.save(User.of(randomEmail(), randomPwdHash()));
        User activeUser = repository.save(createRandomActiveUser());

        // Then
        assertNull(repository.findActiveUserById(inactiveUser.getId()));
        assertNotNull(repository.findActiveUserById(activeUser.getId()));
    }
}
