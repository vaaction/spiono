package com.volvo.site.server.service;

import com.volvo.site.server.repository.ClientInstallerJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class SetClientInstallerDownloadedTest extends AbstractWinappServiceTest {

    @Autowired
    ClientInstallerJpaRepository clientJpaRepository;

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void clientInstallerNotExistsTest() {
        service.setClientInstallerDownloaded(0L);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void clientInstallerInactiveTest() {
        service.setClientInstallerDownloaded(newClientInstaller().getId());
    }

    @Test
    public void setClientInstallerDownloadedTest1() {
        // Given
        long id = newActiveClientInstaller().getId();

        // When
        assertFalse(isDownloaded(id));
        service.setClientInstallerDownloaded(id);

        // Then
        assertTrue(isDownloaded(id));
        service.setClientInstallerDownloaded(id);
        assertTrue(isDownloaded(id));
    }

    private boolean isDownloaded(long clientInstallerId) {
         return clientJpaRepository.findOne(clientInstallerId).isDownloaded();
    }
}
