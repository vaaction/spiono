package com.volvo.site.server.component;

import com.volvo.site.server.model.entity.Registration;
import com.volvo.site.server.service.LoginService;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Locale;

import static com.volvo.platform.java.testing.TestUtils.*;
import static org.testng.Assert.*;

public class PwdAndEmailUserDetailsServiceTest extends AbstractFullSpringTest {

    @Autowired
    PwdAndEmailUserDetailsService userDetailsServicePwdAndEmail;

    @Autowired
    LoginService loginService;

    @Test(expectedExceptions = NullPointerException.class)
    public void npeTest() {
        userDetailsServicePwdAndEmail.loadUserByUsername(null);
    }

    @Test(dataProvider = "invalidEmails", expectedExceptions = IllegalArgumentException.class)
    public void invalidEmailTest(String email) {
        userDetailsServicePwdAndEmail.loadUserByUsername(email);
    }

    @Test(expectedExceptions = UsernameNotFoundException.class)
    public void tryFindNotExistingUser() {
        assertNull(userDetailsServicePwdAndEmail.loadUserByUsername(randomEmail()));
    }

    /*
    @Test
    public void tryFindNormalUser() {
        final String email = randomEmail();
        final String pwd = randomPwd();

        Registration registration = loginService.register(email, pwd, Locale.ENGLISH);
        loginService.confirmRegistration(registration.getUniqueKey());

        UserDetails userDetails = userDetailsServicePwdAndEmail.loadUserByUsername(email);
        assertNotNull(userDetails);
        assertEquals(userDetails.getUsername(), email);
        assertTrue(BCrypt.checkpw(pwd, userDetails.getPassword()));
        assertTrue(userDetails.isAccountNonExpired());
        assertTrue(userDetails.isAccountNonLocked());
        assertTrue(userDetails.isCredentialsNonExpired());
        assertTrue(userDetails.isEnabled());
    }
    */

    @DataProvider(name="invalidEmails")
    public Object[][] invalidEmails() {
        return invalidEmailsDataProvider();
    }
}
