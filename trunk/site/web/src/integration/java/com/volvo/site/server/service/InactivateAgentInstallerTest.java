package com.volvo.site.server.service;


import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.repository.AgentInstallerJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.exception.ServiceException;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class InactivateAgentInstallerTest extends AbstractWinappServiceTest {

    @Autowired
    AgentInstallerJpaRepository agentJpaRepository;

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void tryWithNotExistingClientInstaller() {
        inactivate(0);
    }

    @Test
    public void inactivateInactivatedAgent() {
        // Given
        long id = newInactiveAgentInstaller().getId();
        assertFalse(isActive(id));

        // When
        inactivate(id);

        // Then
        assertFalse(isActive(id));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void inactivateWithActiveClientInstallerAndOneAgent() {
        Pair<ClientInstaller, AgentInstaller> pair = newActiveClientAndAgentInstallers();
        inactivate(pair.getRight().getId());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void inactivateWithDownloadedClientInstallerAndOneAgent() {
        Pair<ClientInstaller, AgentInstaller> pair = newActiveClientAndAgentInstallers();
        service.setClientInstallerDownloaded(pair.getLeft().getId());
        inactivate(pair.getRight().getId());
    }

    @Test
    public void inactivateWithActiveClientInstallerAndTwoAgents() {
        // Given
        Pair<ClientInstaller, AgentInstaller> pair = newActiveClientAndAgentInstallers();
        assertTrue(isActive(pair.getRight().getId()));

        // When
        newActiveAgentInstaller(pair.getLeft().getId());
        inactivate(pair.getRight().getId());

        // Then
        assertFalse(isActive(pair.getRight().getId()));
    }

    @Test
    public void inactivateWithDownloadedClientInstallerAndTwoAgents() {
        // Given
        Pair<ClientInstaller, AgentInstaller> pair = newActiveClientAndAgentInstallers();
        service.setClientInstallerDownloaded(pair.getLeft().getId());
        assertTrue(isActive(pair.getRight().getId()));

        // When
        newActiveAgentInstaller(pair.getLeft().getId());
        inactivate(pair.getRight().getId());

        // Then
        assertFalse(isActive(pair.getRight().getId()));
    }

    @Test
    public void standardUseCaseTest() {
        // Given
        long clientInstallerId = newClientInstaller().getId();
        long agentInstallerId = newActiveAgentInstaller(clientInstallerId).getId();
        assertTrue(isActive(agentInstallerId));

        // When
        inactivate(agentInstallerId);

        // Then
        assertFalse(isActive(agentInstallerId));
    }

    private void inactivate(long id) {
        service.inactivateAgentInstaller(id);
    }

    private boolean isActive(long id) {
        return agentJpaRepository.findOne(id).isActive();
    }
}
