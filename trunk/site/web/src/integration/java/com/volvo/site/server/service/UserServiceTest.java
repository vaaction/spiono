package com.volvo.site.server.service;

import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.testutil.components.AgentTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.testng.annotations.Test;

import static com.volvo.platform.java.testing.TestUtils.randomPwd;
import static com.volvo.site.server.testutil.RepositoryTestUtils.createActiveUserWithPassword;
import static com.volvo.site.server.testutil.RepositoryTestUtils.createInactiveRandomUser;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

@Test(groups = "SingleClientInstaller")
public class UserServiceTest extends AbstractFullSpringTest {

    @Autowired
    AgentTestUtils agentTestUtils;

    @Autowired
    UserService userService;

    @Autowired
    UserJpaRepository userJpaRepository;

    @Test
    public void changePasswordPositiveTest() {
        // Given
        String oldPassword = randomPwd();
        User user = userJpaRepository.save(createActiveUserWithPassword(oldPassword));

        // When
        String newPassword = randomPwd();
        boolean isChanged = userService.changePassword(user.getId(), oldPassword, newPassword);

        // Then
        assertTrue(isChanged);
        assertTrue(BCrypt.checkpw(newPassword, user.getPassword()));
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void changePasswordOfNonExistingUserTest() {
        userService.changePassword(-1L, randomPwd(), randomPwd());
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void changePasswordForInactiveUserTest() {
        User user = userJpaRepository.save(createInactiveRandomUser());
        userService.changePassword(user.getId(), randomPwd(), randomPwd());
    }

    @Test
    public void changePasswordWithWrongOldPasswordTest() {
        // Given
        String oldPassword = randomPwd();
        User user = userJpaRepository.save(createActiveUserWithPassword(oldPassword));

        // When
        String newPassword = randomPwd();
        boolean isChanged = userService.changePassword(user.getId(), "invalid" + oldPassword, newPassword);

        // Then
        assertFalse(isChanged);
        assertTrue(BCrypt.checkpw(oldPassword, user.getPassword()));
    }
}
