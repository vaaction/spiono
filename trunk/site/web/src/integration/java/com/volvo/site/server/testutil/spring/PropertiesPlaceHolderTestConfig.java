package com.volvo.site.server.testutil.spring;

import com.volvo.site.server.config.placeholder.AbstractPropertiesPlaceHolderConfig;
import org.springframework.context.annotation.Configuration;

import static com.volvo.site.server.config.constants.ConfigFilePaths.APP_TEST_CONFIG_PATH;

@Configuration
public class PropertiesPlaceHolderTestConfig extends AbstractPropertiesPlaceHolderConfig {
    @Override
    protected String getPropertiesFilePath() {
        return APP_TEST_CONFIG_PATH;
    }
}
