package com.volvo.site.server.testutil.spring;


import lombok.Getter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.support.AbstractApplicationContext;

import java.util.ArrayList;

import static com.google.common.collect.Lists.newArrayList;

public class ApplicationContextListener<TEvent extends ApplicationEvent> {

    private final AbstractApplicationContext context;

    private final ArrayList<TEvent> events = newArrayList();

    @Getter
    private boolean isListening;

    public static <TEvent extends ApplicationEvent> ApplicationContextListener<TEvent>
            createContextListener(ApplicationContext context) {
        ApplicationContextListener<TEvent> listener = new ApplicationContextListener<TEvent>(context);
        listener.startListening();
        return listener;
    }

    private final ApplicationListener<TEvent> listener = new ApplicationListener<TEvent>() {
        @Override
        public void onApplicationEvent(TEvent event) {
            events.add(event);
        }
    };

    public ApplicationContextListener(ApplicationContext context) {
        this.context = (AbstractApplicationContext) context;
    }

    public void startListening() {
        if (!isListening) {
            isListening = true;
            context.addApplicationListener(listener);
        }
    }

    public void stopListening() {
        if (isListening) {
            isListening = false;
            context.getApplicationListeners().remove(listener);
        }
    }

    public TEvent findEvent(TEvent event) {
        int index = events.indexOf(event);
        return index == -1 ? null : events.get(index);
    }

    @Override
    protected void finalize() throws Throwable {
        stopListening();
    }
}
