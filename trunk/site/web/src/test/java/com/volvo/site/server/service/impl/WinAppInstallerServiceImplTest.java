package com.volvo.site.server.service.impl;

import com.volvo.site.server.dto.UpdateAgentResult;
import com.volvo.site.server.model.ModelConstants;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.repository.*;
import com.volvo.site.server.service.impl.util.AgentUtilService;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class WinAppInstallerServiceImplTest {

    private static final String validCertificate = randomAlphanumeric(ModelConstants.agentCertificateLen);

    @Mock
    private ClientInstallerJpaRepository clientJpaRepository;
    @Mock
    private AgentInstallerJpaRepository agentInstallerJpaRepository;
    @Mock
    private AgentInstallerRepository agentInstallerRepository;
    @Mock
    private ClientInstallerRepository clientRepository;
    @Mock
    private AgentUtilService agentUtilService;
    @Mock
    private AgentJpaRepository agentJpaRepository;
    @Mock
    private Agent agent;
    @Mock
    private AgentInstaller agentInstaller;
    @Mock
    private ClientInstaller clientInstaller;

    private WinAppInstallerServiceImpl winAppInstallerService;

    @BeforeMethod
    public void beforeMethodSetup() {
        initMocks(this);

        winAppInstallerService = new WinAppInstallerServiceImpl(clientJpaRepository, agentInstallerJpaRepository,
                agentInstallerRepository, clientRepository, agentUtilService, agentJpaRepository);
    }

    @Test
    public void shouldUpdateAgentLibCheckCertificate() {
        long clientInstallerId = 1L;
        long agentInstallerId = 2L;
        winAppInstallerService = spy(winAppInstallerService);

        when(clientInstaller.getId()).thenReturn(clientInstallerId);
        when(agentInstaller.getId()).thenReturn(agentInstallerId);
        when(agent.getClientInstaller()).thenReturn(clientInstaller);
        when(agent.getAgentInstaller()).thenReturn(agentInstaller);
        when(agentUtilService.findByCertificate(validCertificate)).thenReturn(agent);
        doReturn(agentInstaller).when(winAppInstallerService).findLastActiveAgentInstaller(clientInstallerId);

        winAppInstallerService.updateAgentLib(validCertificate);

        verify(agentUtilService).checkAgentCertificate(validCertificate);
    }

    @Test
    public void updateAgentLibPositiveTest() {
        long clientInstallerId = 1L;
        long oldAgentInstallerId = 2L;
        long newAgentInstallerId = 3L;
        AgentInstaller newAgentInstaller = mock(AgentInstaller.class);
        winAppInstallerService = spy(winAppInstallerService);

        when(clientInstaller.getId()).thenReturn(clientInstallerId);
        when(agentInstaller.getId()).thenReturn(oldAgentInstallerId);
        when(newAgentInstaller.getId()).thenReturn(newAgentInstallerId);
        when(newAgentInstaller.getData()).thenReturn(new byte[]{1, 2, 3});
        when(agent.getClientInstaller()).thenReturn(clientInstaller);
        when(agent.getAgentInstaller()).thenReturn(agentInstaller);
        when(agentUtilService.findByCertificate(validCertificate)).thenReturn(agent);
        doReturn(newAgentInstaller).when(winAppInstallerService).findLastActiveAgentInstaller(clientInstallerId);

        UpdateAgentResult result = winAppInstallerService.updateAgentLib(validCertificate);

        verify(agentUtilService).checkAgentCertificate(validCertificate);
        verify(agent).setAgentInstaller(newAgentInstaller);
        verify(agentJpaRepository).save(agent);
        assertTrue(result.isHasUpdate());
        assertNotNull(result.getAgent());
    }
}
