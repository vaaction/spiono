package com.volvo.site.server.controller;

import org.testng.annotations.Test;

import static org.springframework.util.ClassUtils.getPackageName;
import static org.testng.Assert.assertEquals;

public class PackageInfoTest {

    @Test
    public void testPackageName() {
        assertEquals(PackageInfo.PACKAGE_NAME, getPackageName(PackageInfo.class));
    }
}
