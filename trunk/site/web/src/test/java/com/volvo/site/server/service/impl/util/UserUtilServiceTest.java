package com.volvo.site.server.service.impl.util;

import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertSame;

public class UserUtilServiceTest {

    @Mock
    private UserJpaRepository userJpaRepository;

    @Mock
    private User user;

    private UserUtilService userUtilService;

    @BeforeMethod
    public void beforeTest() {
        initMocks(this);
        userUtilService = new UserUtilService(userJpaRepository);
    }

    @Test
    public void shouldReturnUserFromRepository() {
        // Given
        long userId = 5;
        when(userJpaRepository.findActiveUserById(userId)).thenReturn(user);

        // When
        User user = userUtilService.getActiveUser(userId);

        // Then
        assertSame(user, this.user);
        verify(userJpaRepository).findActiveUserById(userId);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldThrowEntiryNotFound() {
        // Given
        long userId = 5;
        when(userJpaRepository.findActiveUserById(userId)).thenReturn(null);

        // When
        userUtilService.getActiveUser(userId);
    }
}
