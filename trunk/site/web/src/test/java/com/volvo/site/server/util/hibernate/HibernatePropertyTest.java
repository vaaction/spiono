package com.volvo.site.server.util.hibernate;


import com.volvo.platform.spring.conf.hibernate.HibernateProperty;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.volvo.platform.java.testing.TestUtils.isEnumValuesAsStringAreUnique;

public class HibernatePropertyTest {

    @Test
    public void testPropertiesAreUnique() {
        Assert.assertTrue(isEnumValuesAsStringAreUnique(HibernateProperty.values()));
    }
}
