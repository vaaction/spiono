package com.volvo.site.server.converter;

import com.volvo.site.server.model.entity.KeyLog;
import com.volvo.site.server.util.SpyKeyLogDataDTOMocker;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class KeyLogDataDTOToKeyLogConverterTest {

    private final SpyKeyLogDataDTOMocker mocker = new SpyKeyLogDataDTOMocker();

    @Test(expectedExceptions = NullPointerException.class)
    public void testNPE() {
        new KeyLogDataDTOToKeyLogConverter(mocker.agentId).apply(null);
    }

    @Test
    public void testApply() {
        KeyLog keyLog = new KeyLogDataDTOToKeyLogConverter(mocker.agentId).apply(mocker.dto);
        assertTrue(mocker.assertDTOEqualsToModel(keyLog));
    }
}
