package com.volvo.site.server.converter;

import com.google.web.bindery.autobean.shared.AutoBean;
import com.volvo.platform.java.CollectionsTransformer;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.BeanFactory;
import com.volvo.site.gwt.usercabinet.shared.dto.KeyLogDataDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.KeyLogDataListResultDTO;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.KeyLog;
import com.volvo.site.server.service.facade.LogsSearchFacade;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

@PrepareForTest({
        KeyLogDataListResultDTOConverter.class,
        KeyLogListToKeyLogDataDTOListConverter.class,
        CollectionsTransformer.class,
        AgentToAgentsResultItemDTOConverter.class})
public class KeyLogDataListResultDTOConverterTest extends PowerMockTestCase  {

    KeyLogDataListResultDTOConverter converter;
    LogsSearchFacade.PerformLogsSearchResult performLogsSearchResult;

    @Mock
    BeanFactory beanFactory;

    @Mock
    List<KeyLog> keyLogs;

    @Mock
    List<Agent> agents;

    @BeforeMethod
    public void beforeTest() {
        initMocks(this);

        converter = PowerMockito.spy(new KeyLogDataListResultDTOConverter(beanFactory));
        performLogsSearchResult = new LogsSearchFacade.PerformLogsSearchResult(keyLogs, agents);
    }

    @Test
    public void applyTest() throws Exception {
        // Given
        KeyLogListToKeyLogDataDTOListConverter keyLogListConverter = mock(KeyLogListToKeyLogDataDTOListConverter.class);
        List<KeyLogDataDTO> keyLogDataDTOList = mock(List.class);
        AgentToAgentsResultItemDTOConverter agentsConverter = mock(AgentToAgentsResultItemDTOConverter.class);
        PowerMockito.mockStatic(CollectionsTransformer.class);
        List<AgentDTO> agentsDTO = mock(List.class);
        AutoBean<KeyLogDataListResultDTO> resultAutoBean = mock(AutoBean.class);
        KeyLogDataListResultDTO result = mock(KeyLogDataListResultDTO.class);


        // When
        PowerMockito.whenNew(KeyLogListToKeyLogDataDTOListConverter.class).withArguments(beanFactory)
                .thenReturn(keyLogListConverter);
        when(keyLogListConverter.apply(keyLogs)).thenReturn(keyLogDataDTOList);
        PowerMockito.whenNew(AgentToAgentsResultItemDTOConverter.class).withArguments(beanFactory)
                .thenReturn(agentsConverter);
        PowerMockito.when(CollectionsTransformer.transformToList(agents, agentsConverter))
                .thenReturn(agentsDTO);
        when(beanFactory.create(KeyLogDataListResultDTO.class)).thenReturn(resultAutoBean);
        when(resultAutoBean.as()).thenReturn(result);

        // Then
        KeyLogDataListResultDTO actualResult = converter.apply(performLogsSearchResult);
        assertEquals(actualResult, result);
        verify(result).setKeyLogData(keyLogDataDTOList);
        verify(result).setAvailableAgents(agentsDTO);
    }
}
